﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using CodeDebuggingNamespace;
using System.Diagnostics;

namespace JacksonHealthConvertToDV2000
{
    public partial class Form1 : Form
    {
        private enum InputfileActiveorRetiree { Active, Retiree };

        private string _fulllogfile = "";

        private InputfileActiveorRetiree _etype; 

        // ========================================================================================

        private string _pathToInputFiles = "..\\..\\..\\zSource\\01Input\\";
        private string _pathToArchiveFiles = "..\\..\\..\\zSource\\02Archive\\";
        private string _pathToOutputFiles = "..\\..\\..\\zSource\\03Output\\";
        private string _fullpathfilename = "";
        private bool _ranonce;

        // ========================================================================================

        private string _inputarchivepath = "";

        private string _outputarchivepath = "";
        private string _logarchivepath = "";


        // ========================================================================================

        public Form1()
        {
            InitializeComponent();
        }
        // ========================================================================================
        //

        private void OutputToLog(string p)
        {
            try
            {
                using (var writer = File.AppendText(_fulllogfile))
                {
                    writer.WriteLine(p);
                }
            }
            catch (Exception)
            {
                //
            }
        }

        // ========================================================================================

        private void Output(string s1)
        {
            textBox1.AppendText(s1 + Environment.NewLine);

            Outputtodebug(s1);

            OutputToLog(s1);

        }

        // =============================================================================================

        private void Outputtodebug(string s1)
        {
            UDP_Debugging.LogFlat("Conversion \\Form1.cs " + s1);
        }

        // ========================================================================================

        private string Dash(int p)
        {
            var s = "";
            for (var i = 0; i < p; i++) { s = s + "-"; }
            return s;
        }

        // ========================================================================================

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.A))
            {
                textBox1.SelectAll();
                e.Handled = true;
            }
        }

        // ========================================================================================

        private string SetInitialFile(string path)
        {
            var files = Directory.GetFiles(path);

            if (files.Length > 0)
            {
                var list1 = new List<string>();

                foreach (var line in files)
                {
                    var s1 = Path.GetExtension(line);

                    if (s1.ToUpper() != ".EXE")
                    {
                        //if (s1.ToUpper() != ".TXT")
                        {
                            //if (s1.ToUpper() != ".INPUT")
                            {
                                list1.Add(line);
                            }
                        }
                    }
                }

                if (list1.Count > 0)
                {
                    return list1[0];
                }

                return "";
            }
            else
            {
                return "";
            }
        }

        // ========================================================================================

        private void SetTitle()
        {
            const string baseFormTitle = "Jackson Health Convert To DV2000 V";
            var major = Assembly.GetExecutingAssembly().GetName().Version.Major.ToString();
            var minor = Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString();
            var build = Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();
            Text = baseFormTitle + major + @"." + minor + @" (Build " + build + @")";
        }

        // ====================================================================================

        private static string Commafystr(string ss)
        {
            var s1 = ss.Trim();

            if (s1.Contains(",")) return ss;

            var s2 = "";

            for (var i = (s1.Length - 1); i > -1; i--)
            {
                s2 = s1[i] + s2;

                if (i == 0) continue;

                // get inverse count
                var j = s1.Length - i;

                if ((j % 3) == 0) { s2 = "," + s2; }
            }

            return s2;
        }

        // ========================================================================================

        private static string Get_FieldOneBased(string singleLine, int startingPos, int endingPos)
        {
            var s1 = "";
            if (singleLine.Length >= endingPos)
            {
                if (startingPos <= endingPos)
                {
                    if (singleLine != "")
                    {
                        s1 = singleLine.Substring(startingPos - 1, endingPos - startingPos + 1);
                    }
                }
            }
            return s1;
        }

        // ========================================================================================

        private bool Validate_Lines_All(int p, List<string> list1)
        {
            var answer = true;

            for (var i = 0; i < list1.Count; i++)
            {
                var length = list1[1].Length;

                if (length != p)
                {
                    answer = false;
                    Output("Incorrect line length found of " + length);
                    break;
                }
            }
            return answer;
        }

        // ========================================================================================

        private static string GenerateOutputFileName(string p, string d)
        {
            var a = Path.GetDirectoryName(p);
            var b = Path.GetFileNameWithoutExtension(p);
            var c = "_Converted_";
            // var d = DateTime.Now.ToString("yyyy-mm-dd_HH-mm-ss"); // 24Hour
            var e = Path.GetExtension(p);

            if (e != null && e.ToUpper() != ".TXT") { e = ".txt"; }

            if (a != null)
            {
                return Path.Combine(a, b + c + d + e);
            }

            return "";
        }

        // ========================================================================================

        private string Build_DV2000_Subscriber_line(string[] subscriberdata)
        {
            Outputtodebug("Build_DV2000_Subscriber_line");

            var ssn = subscriberdata[00];
            var lastName = subscriberdata[01];
            var firstName = subscriberdata[02];
            var middleInitial = subscriberdata[03];
            var address1 = subscriberdata[04];

            var address2 = subscriberdata[05];
            var city = subscriberdata[06];
            var state = subscriberdata[07];
            var zip = subscriberdata[08];

            // var num1 = subscriberdata[09];

            var dob = subscriberdata[10];
            var gender = subscriberdata[11];

            // [STL001O ]
            var num2 = subscriberdata[12];

            // [MSTL001O ]
            // var num3 = subscriberdata[13];
            // [VISI]
            // var num4 = subscriberdata[14];
            // [008404]
            // var num5 = subscriberdata[15];
            // [2018010124]
            var sbenefitbegin = subscriberdata[16];

            // [24]
            // var num6 = subscriberdata[17];
            // [Y]
            // var flag = subscriberdata[18];

            var s1 = Get_Subscriber_Identification(ssn, lastName, firstName, middleInitial, dob, gender);
            var s2 = Get_Subscriber_Demographics(address1, address2, city, state, zip);

            var benefitContract = MaptoBenefit(num2);

            var rider = MaptoRider(num2);

            // this is the benefit contract   10P100000493
            // var groupnumber = "group";


            // this is the rider  DE00000225
            //   var plan = "plan";

            var s3 = Get_Subscriber_Coverage_Information(sbenefitbegin, benefitContract, rider);
            var s4 = Get_Subscriber_PCP_Information();
            var s5 = Get_Subscriber_HIPAA_Information();
            var s6 = Get_Subscriber_Member_Category_Codes();
            var s7 = Get_Subscriber_Affordable_Care_Act_Information();

            var s9 = s1 + s2 + s3 + s4 + s5 + s6 + s7;


            //Output("Length of Get_Subscriber_Coverage_Information = " + s3.Length);


            //Output("Length of Get_Subscriber_PCP_Information = " + s4.Length);


            //Output("Length of Get_Subscriber_HIPAA_Information = " + s5.Length);


            //Output("Length of Get_Subscriber_Member_Category_Codes = " + s6.Length);


            //Output("Length of Get_Subscriber_Affordable_Care_Act_Information = " + s7.Length);

            //Output("Length of Get_Subscriber_Line = " + s9.Length);

            return s9;

        }

        // ========================================================================================

        private string Build_DV2000_Dependent_line(string line, int offset, string[] subscriberdata)
        {
            Outputtodebug("Build_DV2000_Dependent_line");

            var s1 = "";

            if (DependentExists(line, offset))
            {
                var subscriberSsn = subscriberdata[00];
                var subscriberAddress1 = subscriberdata[04];
                var subscriberAddress2 = subscriberdata[05];
                var subscriberCity = subscriberdata[06];
                var subscriberState = subscriberdata[07];
                var subscriberZip = subscriberdata[08];

                // var subscribernum1 = subscriberdata[09];
                // var subscriberdob = subscriberdata[10];
                // var subscribergender = subscriberdata[11];
                // [STL001O ]
                var num2 = subscriberdata[12];


                var subscriberGroupNumber = MaptoBenefit(num2);   // benefit contract

                var subscriberPlan = MaptoRider(num2);   //rider


                // [MSTL001O ]
                // var subscribernum3 = subscriberdata[13];
                // [VISI]
                // var subscribernum4 = subscriberdata[14];
                // [008404]
                // var subscribernum5 = subscriberdata[15];
                // [2018010124]
                // var subscribersbenefitbegin = subscriberdata[16];
                // [24]
                // var subscribernum6 = subscriberdata[17];
                // [Y]
                // var subscriberflag = subscriberdata[18];

                var dependentLastName = Get_FieldOneBased(line, offset + 0, offset + 19);
                var dependentFirstName = Get_FieldOneBased(line, offset + 20, offset + 34);
                var dependentMiddleInitial = Get_FieldOneBased(line, offset + 35, offset + 35);

                var dependentSsn = Get_FieldOneBased(line, offset + 36, offset + 44);

                var dependentBenefitBeginDate = Get_FieldOneBased(line, offset + 53, offset + 60);

                //// [C]
                //// var flag1 = Get_FieldOneBased(line, offset + 61, offset + 61);

                //// [M]
                //var dependentGender = Get_FieldOneBased(line, offset + 62, offset + 62);



                //var dependentDob = "";

                //var dependentType = "";

                //const string dependentStudent = " ";
                //const string dependentHandicapped = " ";



                // [C]
                var flag1 = Get_FieldOneBased(line, offset + 61, offset + 61);

                // [M]
                var dependentGender = Get_FieldOneBased(line, offset + 62, offset + 62);
                var dependentDob = Get_FieldOneBased(line, offset + 45, offset + 52);
                var dependentType = flag1;

                const string dependentStudent = " ";
                const string dependentHandicapped = " ";


                var d1 = Get_Dependent_Identification(subscriberSsn, dependentLastName,
                    dependentFirstName, dependentMiddleInitial, dependentSsn, dependentDob, dependentType, dependentGender, dependentStudent, dependentHandicapped);

                var d2 = Get_Dependent_Demographics(subscriberAddress1, subscriberAddress2, subscriberCity, subscriberState, subscriberZip);
                var d3 = Get_Dependent_Coverage_Information(dependentBenefitBeginDate, subscriberGroupNumber, subscriberPlan);
                var d4 = Get_Dependent_PCP_Information();
                var d5 = Get_Dependent_HIPPA_Information();
                var d6 = Get_Dependent_Member_Category_Codes();
                var d9 = d1 + d2 + d3 + d4 + d5 + d6;



                //Output("Length of Get_Dependent_Identification = " + d1.Length);


                //Output("Length of Get_Dependent_Demographics = " + d2.Length);


                //Output("Length of Get_Dependent_Coverage_Information = " + d3.Length);


                //Output("Length of Get_Dependent_PCP_Information = " + d4.Length);


                //Output("Length of Get_Dependent_HIPPA_Information = " + d5.Length);

                //Output("Length of Get_Dependent_Member_Category_Codes = " + d6.Length);

                //Output("Length of Get_Subscriber_Line = " + d9.Length);



                s1 = d9;
            }

            return s1;
        }

        // ========================================================================================

        private InputfileActiveorRetiree Getetype(string filename)
        {
            if (filename.ToUpper().StartsWith("CVXIN_10P11270"))  
            {
                return InputfileActiveorRetiree.Retiree;
            }
            else
            {
                return InputfileActiveorRetiree.Active;
            }
        }



        // ========================================================================================
        ////
        //private string Get_Dependent_Identification(string subscriberSsn, string dependentLastName,
        //    string dependentFirstName, string dependentMiddleInitial, string dependentSsn, string dependentDob,
        //    string dependentType, string dependentGender, string dependentStudent, string dependentHandicapped)
        //{
        //    Outputtodebug("Get_Dependent_Identification");

        //    string relationshipCode;
        //    if (dependentType == "S")
        //    {
        //        relationshipCode = "02";
        //    }
        //    else
        //    {
        //        relationshipCode = "05";

        //    }

        //    var d1 =
        //                "D" +                                        // Record Type
        //                subscriberSsn.PadRight(12, ' ') +           // Subscriber ID
        //                " ".PadRight(08, ' ') +                      // Filler
        //                dependentSsn.PadRight(12, ' ') +            // Dependent ID
        //                " ".PadRight(08, ' ') +                      // Filler

        //                dependentSsn.PadRight(09, ' ') +             // Dependent SSN
        //                "ENGLISH".PadRight(11, ' ') +                 // Language
        //                dependentFirstName.PadRight(25, ' ') +      // Dependent First Name
        //                dependentMiddleInitial.PadRight(01, ' ') +  // Dependent Middle Initial
        //                dependentLastName.PadRight(35, ' ') +       // Dependent Last Name

        //                dependentGender.PadRight(01, ' ') +        // Dependent Gender
        //                dependentDob.PadRight(08, ' ') +           // Date of Birth
        //                dependentHandicapped +                                       // Handicapped Indicator 
        //                dependentStudent.PadRight(01, ' ') +                     // Student Status Code

        //                relationshipCode.PadRight(02, ' ');                      // Relationship Code

        //    return d1;
        //}


        // ========================================================================================
        //

        private string Get_Dependent_Identification(string subscriberSsn, string dependentLastName,
            string dependentFirstName, string dependentMiddleInitial, string dependentSsn, string dependentDob,
            string dependentType, string dependentGender, string dependentStudent, string dependentHandicapped)
        {
            Outputtodebug("Get_Dependent_Identification");

            string relationshipCode;

            switch (dependentType)
            {
                case "S":
                    relationshipCode = "02";
                    break;
                case "C":
                    relationshipCode = "05";
                    break;
                default:
                    relationshipCode = "10";
                    break;
            }

            var d1 =
                        "D" +                                        // Record Type
                        subscriberSsn.PadRight(12, ' ') +           // Subscriber ID
                        " ".PadRight(08, ' ') +                      // Filler
                        dependentSsn.PadRight(12, ' ') +            // Dependent ID
                        " ".PadRight(08, ' ') +                      // Filler

                        dependentSsn.PadRight(09, ' ') +             // Dependent SSN
                        "ENGLISH".PadRight(11, ' ') +                 // Language
                        dependentFirstName.PadRight(25, ' ') +      // Dependent First Name
                        dependentMiddleInitial.PadRight(01, ' ') +  // Dependent Middle Initial
                        dependentLastName.PadRight(35, ' ') +       // Dependent Last Name

                        dependentGender.PadRight(01, ' ') +        // Dependent Gender
                        dependentDob.PadRight(08, ' ') +           // Date of Birth
                        dependentHandicapped +                                       // Handicapped Indicator 
                        dependentStudent.PadRight(01, ' ') +                     // Student Status Code

                        relationshipCode.PadRight(02, ' ');                      // Relationship Code

            return d1;
        }

        // ========================================================================================
        //

        private string Get_Dependent_Demographics(string subscriberAddress1, string subscriberAddress2,
            string subscriberCity, string subscriberState, string subscriberZip)
        {
            Outputtodebug("Get_Dependent_Demographics");

            var d2 =
            subscriberAddress1.PadRight(55, ' ') +     // Address1
            subscriberAddress2.PadRight(55, ' ') +     // Address2
            subscriberCity.PadRight(30, ' ') +         // City
            subscriberState.PadRight(02, ' ') +         // State
            subscriberZip.PadRight(15, ' ') +          // Zip

            " ".PadRight(03, ' ') +                      // Optional Country Code
            " ".PadRight(01, ' ') +                      // Optional Email Opt In/Out
            " ".PadRight(50, ' ') +                     // Optional Email Adress
            " ".PadRight(10, ' ');                      // Optional Telephone

            return d2;
        }

        // ========================================================================================
        // DEPENDENT COVERAGE INFORMATION

        private string Get_Dependent_Coverage_Information(string dependentBenefitBeginDate, string groupnumber3, string plan3)
        {
            Outputtodebug("Get_Dependent_Coverage_Information");

            var d3 =
                       dependentBenefitBeginDate.PadRight(08, ' ') +              // Benefit Begin Date  
                      " ".PadRight(8, ' ') +                                      // Benefit End Date  
                      plan3.PadRight(10, ' ') +                                   // Benefit Rider ID
                      groupnumber3.PadRight(30, ' ') +                            // Benefit Contract ID
                      " ".PadRight(3, ' ');                                       // Emplyoment Status

            return d3;
        }

        // ========================================================================================
        // DEPENDENT PCP INFORMATION

        private string Get_Dependent_PCP_Information()
        {
            Outputtodebug("Get_Dependent_PCP_Information");
            var d4 =
                        " ".PadRight(40, ' ') +           // Primary Care Physician Name
                        " ".PadRight(40, ' ') +           // Primary Care Physician ID
                        " ".PadRight(55, ' ') +           // Primary Care Physician Address1
                        " ".PadRight(55, ' ') +           // Primary Care Physician Address2
                        " ".PadRight(30, ' ') +           // Primary Care Physician City
                        " ".PadRight(02, ' ') +           // Primary Care Physician State
                        " ".PadRight(15, ' ') +           // Primary Care Physician Postal Code
                        " ".PadRight(03, ' ') +           // Primary Care Physician Country Code
                        " ".PadRight(10, ' ') +           // Primary Care Physician 
                        " ".PadRight(50, ' ');            // Filler
            return d4;
        }

        // ========================================================================================
        // DEPENDENT HIPAA INFORMATION (Customer Individual Rights)

        private string Get_Dependent_HIPPA_Information()
        {

            Outputtodebug("Get_Dependent_HIPPA_Information");
            var d5 =
                       " ".PadRight(05, ' ') +             // Filler
                       " ".PadRight(25, ' ') +             // Filler
                       " ".PadRight(35, ' ') +             // Filler
                       " ".PadRight(20, ' ') +             // Filler
                       " ".PadRight(05, ' ') +             // Filler
                       " ".PadRight(25, ' ') +             // Filler
                       " ".PadRight(35, ' ') +             // Filler
                       " ".PadRight(20, ' ') +             // Filler
                       " ".PadRight(05, ' ') +             // Filler
                       " ".PadRight(25, ' ') +             // Filler
                       " ".PadRight(35, ' ') +             // Filler
                       " ".PadRight(19, ' ') +             // Filler
                       " ".PadRight(1, ' ');               // Filler
            return d5;
        }

        // ========================================================================================
        // DEPENDENT MEMBER CATEGORY CODES

        private string Get_Dependent_Member_Category_Codes()
        {

            Outputtodebug("Get_Dependent_Member_Category_Codes");
            var d6 =
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(40, ' ') +             // Filler
                        " ".PadRight(40, ' ') +             // Filler
                        " ".PadRight(40, ' ') +             // Filler
                        " ".PadRight(40, ' ') +             // Filler
                        " ".PadRight(40, ' ') +             // Filler
                        " ".PadRight(40, ' ') +             // Filler
                        " ".PadRight(40, ' ') +             // Filler
                        " ".PadRight(40, ' ') +             // Filler
                        " ".PadRight(182, ' ') +             // Filler
                        " ".PadRight(08, ' ') +             // Filler
                        " ".PadRight(03, ' ') +             // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(3, ' ') +              // Filler
                        " ".PadRight(20, ' ') +             // Filler
                        " ".PadRight(175, ' ');             // Filler
            return d6;
        }

        // ========================================================================================

        private bool DependentExists(string line, int offset)
        {
            Outputtodebug("DependentExists");

            var lastname = Get_FieldOneBased(line, offset + 0, offset + 19);

            var firstname = Get_FieldOneBased(line, offset + 20, offset + 34);

            var middlename = Get_FieldOneBased(line, offset + 35, offset + 35);

            var check = (lastname + firstname + middlename);

            if (check.Trim() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // ========================================================================================
        //

        private string[] Parse_Subscriber_From_Line(string line)
        {

            Outputtodebug("Parse_Subscriber_From_Line");

            var ssn = Get_FieldOneBased(line, 1, 9);

            var lastName = Get_FieldOneBased(line, 10, 29);

            var firstName = Get_FieldOneBased(line, 30, 44);

            var middleInitial = Get_FieldOneBased(line, 45, 45);

            var address1 = Get_FieldOneBased(line, 46, 75);

            var address2 = Get_FieldOneBased(line, 76, 105);

            var city = Get_FieldOneBased(line, 106, 130);

            var state = Get_FieldOneBased(line, 131, 132);

            var zip = Get_FieldOneBased(line, 133, 137);

            // home phone
            var num1 = Get_FieldOneBased(line, 142, 151);

            // work phone
            // var workPhone = Get_FieldOneBased(line, 152, 161);

            var dob = Get_FieldOneBased(line, 162, 169);

            var gender = Get_FieldOneBased(line, 170, 170);

            // [STL001O ]
            var num2 = Get_FieldOneBased(line, 171, 178);

            // [MSTL001O ]
            var num3 = Get_FieldOneBased(line, 145, 147);

            // [VISI]
            var num4 = Get_FieldOneBased(line, 179, 182);

            // [008404]
            var num5 = Get_FieldOneBased(line, 183, 188);

            // [2018010124]
            var sbenefitbegin = Get_FieldOneBased(line, 189, 196);

            // [24]
            var num6 = Get_FieldOneBased(line, 197, 198);

            // [Y]
            var flag = Get_FieldOneBased(line, 199, 199);

            var data = new string[21];

            data[00] = ssn.Trim();
            data[01] = lastName.Trim();
            data[02] = firstName.Trim();
            data[03] = middleInitial.Trim();
            data[04] = address1.Trim();

            data[05] = address2.Trim();
            data[06] = city.Trim();
            data[07] = state.Trim();
            data[08] = zip.Trim();

            data[09] = num1;

            data[10] = dob;

            data[11] = gender;

            // [STL001O ]  // contains info for benefit and rider
            data[12] = num2;





            // [MSTL001O ]
            data[13] = num3;

            // [VISI]
            data[14] = num4;

            // [008404]
            data[15] = num5;

            // [2018010124]
            data[16] = sbenefitbegin;

            // [24]
            data[17] = num6;

            // [Y]
            data[18] = flag;

            return data;
        }

        // ========================================================================================

        private void PerformFileManipulations(string d, string infilenameonly, string outputFilename)
        {
            var dt1 = DateTime.Now;

            // CVXin_10P11216_20171124_090309

            infilenameonly = Path.GetFileName(infilenameonly);

            // var infilenameonly = Path.GetFileName(_fullpathfilename);
            infilenameonly = infilenameonly + "_Converted_" + d + ".txt";

            string ufilenameonly;

            if (_etype == InputfileActiveorRetiree.Active)
            {
                ufilenameonly = "CVXin_" + "10P11269" + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
            }
            else
            {
                ufilenameonly = "CVXin_" + "10P11270" + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
            }


            var archivefilefullpath1 = Path.Combine(_inputarchivepath, infilenameonly);
            File.Move(_fullpathfilename, archivefilefullpath1);
            File.SetLastWriteTime(archivefilefullpath1, dt1);

            //var outfilenameonly = Path.GetFileName(outputFilename);
            //if (ufilenameonly != null)
            {
                var archivefilefullpath2 = Path.Combine(_outputarchivepath, ufilenameonly);
                File.Copy(outputFilename, archivefilefullpath2);
                File.SetLastWriteTime(archivefilefullpath2, dt1);
            }

            var filenameonly = Path.GetFileName(ufilenameonly);
            // if (filenameonly != null)
            {
                var finaldestinationname = Path.Combine(_pathToOutputFiles, filenameonly);
                File.Move(outputFilename, finaldestinationname);
                File.SetLastWriteTime(finaldestinationname, dt1);
            }


            // Now Refresh For processing the next file
            textBox2.Clear();
            _fullpathfilename = SetInitialFile(_pathToInputFiles);
            textBox2.Text = Path.GetFileName(_fullpathfilename);

            Refresh();
        }




        // ========================================================================================

        private void Form1_Load(object sender, EventArgs e)
        {
            Outputtodebug("Form1_Load");
            SetTitle();

            SetInitialPathOperations();

        }

        // ========================================================================================

        private void CreateAndOrSetArchiveSubdirectories(string newdir1)
        {
            Outputtodebug("CreateAndOrSetArchiveSubdirectories");

            _inputarchivepath = Path.Combine(newdir1, "InputFiles");
            if (!Directory.Exists(_inputarchivepath))
            {
                Directory.CreateDirectory(_inputarchivepath);
            }

            _outputarchivepath = Path.Combine(newdir1, "OutputFiles");
            if (!Directory.Exists(_outputarchivepath))
            {
                Directory.CreateDirectory(_outputarchivepath);
            }

            _logarchivepath = Path.Combine(newdir1, "LogFiles");
            if (!Directory.Exists(_logarchivepath))
            {
                Directory.CreateDirectory(_logarchivepath);
            }

        }

        // ========================================================================================

        private void SetInitialPathOperations()
        {
            // First set to current if not in debug mode
            if (!Directory.Exists(_pathToInputFiles))
            {
                _pathToInputFiles = @".";
            }

            // Next Set up the Archive Directories
            if (!Directory.Exists(_pathToArchiveFiles))
            {
                _pathToArchiveFiles = @".";
                // Setup archive directories 
                _pathToArchiveFiles = Path.Combine(_pathToArchiveFiles, "Archive");
                if (!Directory.Exists(_pathToArchiveFiles))
                {
                    Directory.CreateDirectory(_pathToArchiveFiles);
                }
            }
            CreateAndOrSetArchiveSubdirectories(_pathToArchiveFiles);

            var logfilename = "Log_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".txt";
            _fulllogfile = Path.Combine(Path.GetFullPath(_logarchivepath), logfilename);




            // Lastly set up the Output Directory
            if (!Directory.Exists(_pathToOutputFiles))
            {
                _pathToOutputFiles = @"\\ad.davisvision.com\Files\Apps\CVXFTP\Prod\incoming";
            }


            var inputdirfullpath = Path.GetFullPath(_pathToInputFiles);

            _fullpathfilename = SetInitialFile(inputdirfullpath);

            textBox2.Text = Path.GetFileName(_fullpathfilename);

            _pathToArchiveFiles = Path.GetFullPath(_pathToArchiveFiles);
            _pathToInputFiles = Path.GetFullPath(_pathToInputFiles);
            _pathToOutputFiles = Path.GetFullPath(_pathToOutputFiles);
        }


        // ========================================================================================

        private List<string> ProcessList(List<string> list1B)
        {
            Outputtodebug("ProcessList");
            var list2B = new List<string>();

            foreach (var item in list1B)
            {
                var memberData = Parse_Subscriber_From_Line(item);

                var line = Build_DV2000_Subscriber_line(memberData);

                list2B.Add(line);

                var offset = 219;

                for (var i = 1; i <= 15; i++)
                {
                    var dep = Build_DV2000_Dependent_line(item, offset, memberData);

                    if (dep != "")
                    {
                        // add dependents line
                        list2B.Add(dep);
                    }
                    else
                    {
                        // break out of loop
                        break;
                    }

                    offset = offset + 73;
                }
            }

            return list2B;
        }

        // ====================================================================================
        //

        private static string DisplayTime6(Stopwatch sTime)
        {
            var frequency = Stopwatch.Frequency;
            var nanosecPerTick = (1000L * 1000L * 1000L) / frequency;
            var absoluteNanoseconds = sTime.ElapsedTicks * nanosecPerTick;
            absoluteNanoseconds = absoluteNanoseconds / 1000;
            absoluteNanoseconds = absoluteNanoseconds / 1000;
            var msec = (int)(absoluteNanoseconds % 1000);
            absoluteNanoseconds = absoluteNanoseconds / 1000;
            var sec = (int)(absoluteNanoseconds);

            var mins = 0; var hrs = 0; var day = 0; var newSec = sec;

            if (newSec >= 60)
            { mins = (newSec / 60); newSec = newSec % 60; }

            var newMin = mins;

            if (newMin >= 60)
            { hrs = (newMin / 60); newMin = newMin % 60; }

            var newHr = hrs;

            if (newHr >= 24)
            { day = (newHr / 24); newHr = newHr % 24; }

            var newDay = day;

            if (newDay > 0)
            { return "[Days = " + newDay + "] " + "[Hours = " + newHr + "]"; }

            if (newHr > 0)
            { return "[Hours = " + newHr + "] " + "[Minutes = " + newMin + "]"; }

            if (newMin > 0)
            { return "[Minutes = " + newMin + "] " + "[Seconds = " + newSec + "]"; }

            return "[Seconds = " + newSec + "] " + "[MilliSeconds = " + msec + "]";
        }

        // ========================================================================================

        private string AddTrailingBackSlash(string dir)
        {

            Outputtodebug("AddTrailingBackSlash");
            if (dir.EndsWith(@"\"))
            {
                return dir;
            }
            else
            {
                return dir + @"\";
            }
        }

        // ========================================================================================

        private string Getheader(string s2)
        {
            var s1 = "";
            s1 = s1 + "H";     // Record Type H for Header
            s1 = s1 + "P";     // File Status     T = test   P = production  O = open enrollment
            s1 = s1 + "DAVIS VISION".PadRight(40, ' ');     // Name
            s1 = s1 + s2.PadRight(50, ' ');
            s1 = s1 + DateTime.Now.ToString("yyyyMMdd");    // File Create Date
            s1 = s1 + "F";                                  // File Type  T, F or C
            //  Output_CVXFile(DateTime.Now.ToString("yyyyMMdd"));  // Full File Term Date
            s1 = s1 + "        ";                           // Full File Term Date
            s1 = s1 + " ";                                 // Full File Reporting Criteria
            s1 = s1 + " ".PadRight(1890, ' ');             // Filler

            return s1;
        }


        // ========================================================================================

        private string Gettrailer(int subscriberRecordCount, int dependentRecordCount)
        {
            var totalRecordCount = subscriberRecordCount + dependentRecordCount;

            var s1 = "";

            s1 = s1 + "T";
            s1 = s1 + totalRecordCount.ToString().PadLeft(10, '0');
            s1 = s1 + subscriberRecordCount.ToString().PadLeft(10, '0');
            s1 = s1 + dependentRecordCount.ToString().PadLeft(10, '0');
            s1 = s1 + " ".PadRight(1969, ' ');             // Filler

            return s1;
        }

        // ========================================================================================
        // SUBSCRIBER IDENTIFICATION

        private string Get_Subscriber_Identification(string ssn, string lastName, string firstName,
            string middleInitial, string dob, string gender)
        {
            var s1 =
                          "S" +                               // Required Record Type
                          ssn.PadRight(12, ' ') +             // Required Subscriber ID
                          " ".PadRight(08, ' ') +             // Optional Filler
                          " ".PadRight(12, ' ') +             // Optional Alternate ID
                          " ".PadRight(06, ' ') +             // Optional Filler

                          " ".PadRight(02, ' ') +             // Optional Dependent Number
                          ssn.PadRight(09, ' ') +             // Optional Subscriber SSN
                          "ENGLISH".PadRight(11, ' ') +       // Optional Language
                          firstName.PadRight(25, ' ') +      // Required Subscriber First Name
                          middleInitial.PadRight(01, ' ') +  // Optional Subscriber Middle Initial

                          lastName.PadRight(35, ' ') +       // Required Subscriber Last Name
                          gender.PadRight(1, ' ') +           // Required Subscriber Gender
                          dob.PadRight(8, ' ') +             // Required Date of Birth
                          "N";                                // Optional Handicapped Indicator

            return s1;
        }

        // ========================================================================================
        // SUBSCRIBER DEMOGRAPHICS

        private string Get_Subscriber_Demographics(string address1, string address2, string city, string state, string zip)
        {
            var s2 =
                        address1.PadRight(55, ' ') +      // Required Address1
                        address2.PadRight(55, ' ') +      // Optional Address2
                        city.PadRight(30, ' ') +          // Required City
                        state.PadRight(02, ' ') +         // Required State
                        zip.PadRight(15, ' ') +           // Required Zip

                        " ".PadRight(03, ' ') +           // Optional Country Code

                        " ".PadRight(55, ' ') +           // Optional Alternate Address1
                        " ".PadRight(55, ' ') +           // Optional Alternate Address2
                        " ".PadRight(30, ' ') +           // Optional Alternate City
                        " ".PadRight(02, ' ') +           // Optional Alternate State
                        " ".PadRight(15, ' ') +           // Optional Alternate Zip

                        " ".PadRight(03, ' ') +           // Optional Country Code
                        " ".PadRight(01, ' ') +           // Optional Email Opt In/Out
                        " ".PadRight(50, ' ') +           // Optional Email Adress
                        " ".PadRight(10, ' ');            // Optional Telephone
            return s2;
        }


        // ========================================================================================
        // SUBSCRIBER COVERAGE 

        private string Get_Subscriber_Coverage_Information(string subscriberBenefitbegin, string groupnumber2, string plan2)
        {

            //    Get_Coverage(groupnumber2, plan2, out benefitRiderId2, out benefitContractId2);

            var s3 =
                        "VIS" +                                         // Plan Type
                        subscriberBenefitbegin.PadRight(08, ' ') +     // Benefit Begin Date  
                        " ".PadRight(08, ' ') +                         // Benefit End Date  

                        plan2.PadRight(10, ' ') +            // Benefit Rider ID
                        groupnumber2.PadRight(30, ' ') +              // Benefit Contract ID
                        " ".PadRight(3, ' ') +                        // Emplyoment Status

                        " ".PadRight(01, ' ') +                         // Cobra Qualifying Event
                        " ".PadRight(12, ' ') +                         // Original Subscriber ID
                        " ".PadRight(8, ' ');                           // Filler
            return s3;
        }

        // ========================================================================================
        // SUBSCRIBER PCP INFORMATION

        private string Get_Subscriber_PCP_Information()
        {
            var s4 =
                        " ".PadRight(40, ' ') +            // Optional Primary Care Physician Name
                        " ".PadRight(40, ' ') +            // Optional Primary Care Physician ID
                        " ".PadRight(55, ' ') +           // Optional Primary Care Physician Address1

                        " ".PadRight(55, ' ') +           // Optional Primary Care Physician Address2
                        " ".PadRight(30, ' ') +           // Optional Primary Care Physician City
                        " ".PadRight(02, ' ') +           // Optional Primary Care Physician State

                        " ".PadRight(15, ' ') +            // Optional Primary Care Physician Postal Code
                        " ".PadRight(03, ' ') +            // Optional Primary Care Physician Country Code
                        " ".PadRight(10, ' ') +            // Optional Primary Care Physician 

                        " ".PadRight(50, ' ');          // Filler
            return s4;
        }

        // ========================================================================================
        // SUBSCRIBER HIPAA INFORMATION (Customer Individual Rights)

        private string Get_Subscriber_HIPAA_Information()
        {
            var s5 =
                        " ".PadRight(05, ' ') +      // Filler
                        " ".PadRight(25, ' ') +      // Filler
                        " ".PadRight(35, ' ') +      // Filler
                        " ".PadRight(20, ' ') +      // Filler
                        " ".PadRight(05, ' ') +      // Filler

                        " ".PadRight(25, ' ') +      // Filler
                        " ".PadRight(35, ' ') +      // Filler
                        " ".PadRight(20, ' ') +      // Filler
                        " ".PadRight(05, ' ') +      // Filler
                        " ".PadRight(25, ' ') +      // Filler

                        " ".PadRight(35, ' ') +      // Filler
                        " ".PadRight(20, ' ') +      // Filler
                        " ".PadRight(01, ' ');       // Filler
            return s5;
        }

        // ========================================================================================
        // SUBSCRIBER MEMBER CATEGORY CODES

        private string Get_Subscriber_Member_Category_Codes()
        {
            var s6 =
                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +           // Filler
                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +          // Filler
                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +           // Filler


                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +           // Filler
                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +           // Filler
                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +           // Filler

                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +           // Filler

                        " ".PadRight(40, ' ') +           // Filler
                        " ".PadRight(40, ' ') +          // Filler
                        " ".PadRight(40, ' ') +           // Filler
                        " ".PadRight(40, ' ') +          // Filler

                        " ".PadRight(40, ' ') +           // Filler
                        " ".PadRight(40, ' ') +           // Filler
                       " ".PadRight(40, ' ') +          // Filler
                        " ".PadRight(40, ' ') +           // Filler

                        " ".PadRight(08, ' ') +           // Filler

                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +          // Filler
                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +           // Filler
                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +            // Filler

                        " ".PadRight(03, ' ') +            // Filler
                        " ".PadRight(20, ' ') +            // Filler
                        " ".PadRight(03, ' ') +            // Filler
                        " ".PadRight(20, ' ') +            // Filler
                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +          // Filler

                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +          // Filler

                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +           // Filler

                        " ".PadRight(166, ' ');          // Filler
            return s6;
        }

        // ========================================================================================
        // SUBSCRIBER AFORDABLE CARE ACT

        private string Get_Subscriber_Affordable_Care_Act_Information()
        {
            var s7 =
                        " ".PadRight(01, ' ') +         // Filler
                        " ".PadRight(08, ' ');          // Filler
            return s7;
        }

        // ========================================================================================

        private List<string> AddHeaderTrailer(List<string> list1A)
        {
            var s = 0;
            var d = 0;
            var e = 0;

            var list2A = new List<string>();

            list2A.Add(Getheader("JacksonHealth"));

            foreach (var line in list1A)
            {
                var typ = Get_FieldOneBased(line, 1, 1);

                switch (typ)
                {
                    case "S":
                        s = s + 1;
                        break;
                    case "D":
                        d = d + 1;
                        break;
                    default:
                        e = e + 1;
                        break;
                }
            }

            foreach (var line in list1A)
            {
                list2A.Add(line);
            }

            if (e > 0)
            {
                Output("WARNING UNCLASSIFIED LINES DETECTED");
            }
            else
            {
                list2A.Add(Gettrailer(s, d));
            }

            return list2A;
        }

        // ========================================================================================

        private void button1_Click(object sender, EventArgs e)
        {
            Outputtodebug("button1_Click");

            var sTest = new Stopwatch();
            sTest.Reset();
            sTest.Start();


            _etype = Getetype(Path.GetFileName(_fullpathfilename));







            if (File.Exists(_fullpathfilename))
            {


                if (ValidFilename(_fullpathfilename))
                {
                    Output(Dash(70));

                    Output("Processing File " + _fullpathfilename);

                    var list1 = File.ReadAllLines(_fullpathfilename).ToList();

                    Output("Total Line Count = " + Commafystr(list1.Count.ToString()));

                    const int correctlength = 1314;

                    if (Validate_Lines_All(correctlength, list1))
                    {
                        Output("All Lines are of the correct length (" + correctlength + ")");

                        Output("Please Wait ... (takes a minute or so)");


                        var sin = ProcessList(list1);

                        var fin = AddHeaderTrailer(sin);

                        var d = DateTime.Now.ToString("yyyy-mm-dd_HH-mm-ss"); // 24Hour

                        var outputFilename = GenerateOutputFileName(_fullpathfilename, d);

                        File.WriteAllLines(outputFilename, fin.ToArray());

                        Output("Done Converting File");

                        PerformFileManipulations(d, _fullpathfilename, outputFilename);

                        sTest.Stop();
                        Output("Done in " + DisplayTime6(sTest));


                    }
                    else
                    {
                        Output("File of incorrect length");

                    }

                }
                else
                {
                    Output("Input FIle must start with CVXin_10P11269 (Active) or CVXin_10P11270 (Retiree)");
                }






            }
            else
            {
                Output("Input File Not Found");
                Output(_fullpathfilename);
            }

        }
        // ========================================================================================

        private bool ValidFilename(string fullpathfilename)
        {
            var filenameonly = Path.GetFileName(fullpathfilename);
            if (filenameonly != null)
            {
                filenameonly = filenameonly.ToUpper();


                if (filenameonly.StartsWith("CVXIN_10P11270"))
                {
                    return true;
                }

                if (filenameonly.StartsWith("CVXIN_10P11269"))
                {
                    return true;
                }



            }

            return false;
        }



        // ========================================================================================

        private void button2_Click(object sender, EventArgs e)
        {
            Outputtodebug("button2_Click");

            textBox2.Clear();
            textBox1.Clear();

            var openFileDialog1 = new OpenFileDialog();

            _pathToInputFiles = Path.GetFullPath(_pathToInputFiles);

            // Set default directory
            //openFileDialog1.RestoreDirectory = true;
            openFileDialog1.InitialDirectory = AddTrailingBackSlash(Path.Combine(_pathToInputFiles));
            openFileDialog1.Title = @"Select Incomming Enrollment File To Process";
            openFileDialog1.Filter = @"All files (*.*)|*.*|Text files (*.txt)|*.txt";

            var result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                var filename = openFileDialog1.FileName;
                textBox2.Text = Path.GetFileName(filename);
                _fullpathfilename = Path.GetFullPath(filename);
            }
        }
        // ========================================================================================

        private string MaptoRider(string code)
        {
            switch (code.Trim())
            {
                case "JHSRETB":
                    return "FV00000646";
                case "JHSRETP":
                    return "PR00000789";
                case "JHS001O":
                    return "FV00000646";
                case "JHS002O":
                    return "PR00000789";
                default:
                    Output("Warning no mapping to rider found to [" + code + "]");
                    return "XX345678XX";
            }
        }

        // ========================================================================================

        private string MaptoBenefit(string code)
        {
            switch (code.Trim())
            {
                case "JHSRETB":
                    return "10P100001270";
                case "JHSRETP":
                    return "10P100001273";
                case "JHS001O":
                    return "10P100001269";
                case "JHS002O":
                    return "10P100001272";
                default:
                    Output("Warning no mapping to benefit contract found to [" + code + "]");
                    return "XX34567890XX";
            }
        }
        // ========================================================================================
        //

        private static string Getuser()
        {
            // System.Security.Principal.WindowsIdentity.GetCurrent().Name  FQDN
            return Environment.UserName;
        }


        // ========================================================================================
        //

        private static string Getmachine()
        {
            // System.Environment.MachineName from a console or WinForms app.
            // HttpContext.Current.Server.MachineName from a web app
            // System.Net.Dns.GetHostName() to get the FQDN


            return Environment.MachineName;
        }

        // ========================================================================================

        private void Form1_Activated(object sender, EventArgs e)
        {
            Outputtodebug("Form1_Activated");

            if (!(_ranonce))
            {
                Output("Run as: " + Getuser());

                Output("Run on: " + Getmachine());

                Output("InputFiles   = " + _pathToInputFiles);
                Output("ArchiveFiles = " + _pathToArchiveFiles);
                Output("OutputFiles  = " + _pathToOutputFiles);

                _ranonce = true;

                var args = Environment.GetCommandLineArgs();

                if (args.Length > 1)
                {
                    var arg1 = args[1].ToUpper();

                    if (arg1 == "AUTO")
                    {
                        OutputToLog(DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " Auto Run");


                        // this will block till complete
                        button1_Click(new object(), new EventArgs());
                    }
                    else
                    {
                        //  do nothing incorrect command line arguments

                        OutputToLog(DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " Manual Run");

                    }

                    // Close the windows form
                    BeginInvoke(new MethodInvoker(Close));

                }
            }
        }
        // ========================================================================================




    }
}
