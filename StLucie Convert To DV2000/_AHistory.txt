Purpose: To Convert an incomming file to a DV2000 layout enrollment file for St Lucie

Version 1.1     12 Dec 2014 	Initial Code From Scratch

Version 1.2     15 Dec 2014 	Started work against Real File

Version 1.3     15 Dec 2014 	Switched to Line Reader

Version 1.4     15 Dec 2014 	Completed Input Half workd around dependent bug

Version 1.5     15 Dec 2014 	resolved output issues

Version 1.6     17 Dec 2014 	Final Code Tested against boni and boyce

Version 1.9     14 Jan 2015 	Reversed Duval and Ecambia

Version 2.3     28 Nov 2016		Straight Port To VS2013 	

Version 2.4     09 Dec 2016		Completed changes for new mapping

Version 2.6     2017 01 20		Small Tweaks to troubleshoot path issue

Version 2.6     2017 01 23 		Added LogFile to track changes

Version 2.7     2017 10 18 		Added "2014": // Calhoun to mapping

Version 2.8     2018 01 15      Added user and machine to log trail 		


-----------------------------------------------------------------------------------------------
Ideas

Automate by detecting incoming file and then immediately converting it after being able to 
obtain an exclusive lock on the file - My main objection here is that any errors or concerns 
would be suppressed this could be alleviated by sending an email report but often its best to 
keep it simple especially on these low frequency conversions



Pull out switch statement mapping into an external file

Make incomming directory not hard coded 

Add stronger input validation

add post processing CVX2000 Layout Validator

missing header

missing footer

record counts
 
Add Summary of Gender, Student, Spouse-Child, Handicapped

Add Summary Breakdown By Group Plan

-----------------------------------------------------------------------------------------------

Bugs 

-----------------------------------------------------------------------------------------------

Mods

-----------------------------------------------------------------------------------------------

Other
