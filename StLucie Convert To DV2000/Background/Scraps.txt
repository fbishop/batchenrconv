


        // ========================================================================================
        // Process header

        private void Create_CVX2000_Header()
        {
            Outputtodebug("Create_CVX2000_Header");

            Output_CVXFile("H");  // Record Type H for Header
            Output_CVXFile("P");  // File Status     T = test   P = production  O = open enrollment
            Output_CVXFile("DAVIS VISION".PadRight(40, ' '));  // Name
            Output_CVXFile("Florida School Retiree Benefit Consortium".PadRight(50, ' '));
            Output_CVXFile(DateTime.Now.ToString("yyyyMMdd"));  // File Create Date
            Output_CVXFile("F");                                // File Type  T, F or C
            //  Output_CVXFile(DateTime.Now.ToString("yyyyMMdd"));  // Full File Term Date
            Output_CVXFile("        ");  // Full File Term Date
            Output_CVXFile(" ");                                // Full File Reporting Criteria
            Output_CVXFile(" ".PadRight(1890, ' '));            // Filler
            Output_CVXFile(Environment.NewLine);
        }



        // ========================================================================================
        //

        private void Create_CVX2000_Footer(int subscriberRecordCount, int dependentRecordCount)
        {
            Outputtodebug("Create_CVX2000_Footer");
            var totalRecordCount = subscriberRecordCount + dependentRecordCount;
            Output_CVXFile("T");
            Output_CVXFile(totalRecordCount.ToString().PadLeft(10, '0'));
            Output_CVXFile(subscriberRecordCount.ToString().PadLeft(10, '0'));
            Output_CVXFile(dependentRecordCount.ToString().PadLeft(10, '0'));
            Output_CVXFile(" ".PadRight(1969, ' '));             // Filler
            Output_CVXFile(Environment.NewLine);
        }







        // ========================================================================================
        // SUBSCRIBER DEMOGRAPHICS

        private string Get_Subscriber_Demographics(string address1, string address2, string city, string state, string zip)
        {
            var s2 =
                        address1.PadRight(55, ' ') +      // Required Address1
                        address2.PadRight(55, ' ') +      // Optional Address2
                        city.PadRight(30, ' ') +          // Required City
                        state.PadRight(02, ' ') +         // Required State
                        zip.PadRight(15, ' ') +           // Required Zip

                        " ".PadRight(03, ' ') +           // Optional Country Code

                        " ".PadRight(55, ' ') +           // Optional Alternate Address1
                        " ".PadRight(55, ' ') +           // Optional Alternate Address2
                        " ".PadRight(30, ' ') +           // Optional Alternate City
                        " ".PadRight(02, ' ') +           // Optional Alternate State
                        " ".PadRight(15, ' ') +           // Optional Alternate Zip

                        " ".PadRight(03, ' ') +           // Optional Country Code
                        " ".PadRight(01, ' ') +           // Optional Email Opt In/Out
                        " ".PadRight(50, ' ') +           // Optional Email Adress
                        " ".PadRight(10, ' ');            // Optional Telephone
            return s2;
        }

        // ========================================================================================
        // COVERAGE 

        private void Get_Coverage(string groupnumber, string plan, out string benefitRiderId, out string benefitContractId)
        {
            switch (groupnumber)
            {
                case "2001": // Brevard 
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000309";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000369";
                        benefitContractId = "10P100000868";
                    }
                    break;

                case "2002": // Escambia
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000324";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000371";
                        benefitContractId = "10P100000868";
                    }
                    break;

                case "2003": // Duval
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000323";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000370";
                        benefitContractId = "10P100000868";
                    }
                    break;


                case "2005": // Charlotte
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000368";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000373";
                        benefitContractId = "10P100000868";
                    }
                    break;


                case "2006": // Orange
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000367";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000372";
                        benefitContractId = "10P100000868";
                    }
                    break;


                case "2007": // Putnam
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000428";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000430";
                        benefitContractId = "10P100000868";
                    }
                    break;


                case "2008": // Flagler
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000427";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000429";
                        benefitContractId = "10P100000868";
                    }
                    break;


                case "2009": // Jackson
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000449";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000454";
                        benefitContractId = "10P100000868";
                    }
                    break;


                case "2010": // Gilchrist
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000448";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000453";
                        benefitContractId = "10P100000868";
                    }
                    break;


                case "2011": // Washington
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000452";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000457";
                        benefitContractId = "10P100000868";
                    }
                    break;


                case "2012": // Walton
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000451";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000456";
                        benefitContractId = "10P100000868";
                    }
                    break;


                case "2013": // Osceola
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000450";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000455";
                        benefitContractId = "10P100000868";
                    }
                    break;


                case "2014": // Calhoun
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000545";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000546";
                        benefitContractId = "10P100000868";
                    }
                    break;



                default:
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000309";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000369";
                        benefitContractId = "10P100000868";
                    }
                    break;
            }

        }

        // ========================================================================================
        // SUBSCRIBER COVERAGE 

        private string Get_Subscriber_Coverage_Information(string subscriberBenefitbegin, string groupnumber2, string plan2)
        {
            string benefitRiderId2;
            string benefitContractId2;

            Get_Coverage(groupnumber2, plan2, out benefitRiderId2, out benefitContractId2);

            var s3 =
                        "VIS" +                                         // Plan Type
                        subscriberBenefitbegin.PadRight(08, ' ') +     // Benefit Begin Date  
                        " ".PadRight(08, ' ') +                         // Benefit End Date  

                        benefitRiderId2.PadRight(10, ' ') +            // Benefit Rider ID
                        benefitContractId2.PadRight(30, ' ') +              // Benefit Contract ID
                        "ACT".PadRight(3, ' ') +                        // Emplyoment Status

                        " ".PadRight(01, ' ') +                         // Cobra Qualifying Event
                        " ".PadRight(12, ' ') +                         // Original Subscriber ID
                        " ".PadRight(8, ' ');                           // Filler
            return s3;
        }

        // ========================================================================================
        // SUBSCRIBER PCP INFORMATION

        private string Get_Subscriber_PCP_Information()
        {
            var s4 =
                        " ".PadRight(40, ' ') +            // Optional Primary Care Physician Name
                        " ".PadRight(40, ' ') +            // Optional Primary Care Physician ID
                        " ".PadRight(55, ' ') +           // Optional Primary Care Physician Address1

                        " ".PadRight(55, ' ') +           // Optional Primary Care Physician Address2
                        " ".PadRight(30, ' ') +           // Optional Primary Care Physician City
                        " ".PadRight(02, ' ') +           // Optional Primary Care Physician State

                        " ".PadRight(15, ' ') +            // Optional Primary Care Physician Postal Code
                        " ".PadRight(03, ' ') +            // Optional Primary Care Physician Country Code
                        " ".PadRight(10, ' ') +            // Optional Primary Care Physician 

                        " ".PadRight(50, ' ');          // Filler
            return s4;
        }

        // ========================================================================================
        // SUBSCRIBER HIPAA INFORMATION (Customer Individual Rights)

        private string Get_Subscriber_HIPAA_Information()
        {
            var s5 =
                        " ".PadRight(05, ' ') +      // Filler
                        " ".PadRight(25, ' ') +      // Filler
                        " ".PadRight(35, ' ') +      // Filler
                        " ".PadRight(20, ' ') +      // Filler
                        " ".PadRight(05, ' ') +      // Filler

                        " ".PadRight(25, ' ') +      // Filler
                        " ".PadRight(35, ' ') +      // Filler
                        " ".PadRight(20, ' ') +      // Filler
                        " ".PadRight(05, ' ') +      // Filler
                        " ".PadRight(25, ' ') +      // Filler

                        " ".PadRight(35, ' ') +      // Filler
                        " ".PadRight(20, ' ') +      // Filler
                        " ".PadRight(01, ' ');       // Filler
            return s5;
        }

        // ========================================================================================
        // SUBSCRIBER MEMBER CATEGORY CODES

        private string Get_Subscriber_Member_Category_Codes()
        {
            var s6 =
                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +           // Filler
                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +          // Filler
                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +           // Filler


                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +           // Filler
                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +           // Filler
                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +           // Filler

                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +           // Filler

                        " ".PadRight(40, ' ') +           // Filler
                        " ".PadRight(40, ' ') +          // Filler
                        " ".PadRight(40, ' ') +           // Filler
                        " ".PadRight(40, ' ') +          // Filler

                        " ".PadRight(40, ' ') +           // Filler
                        " ".PadRight(40, ' ') +           // Filler
                       " ".PadRight(40, ' ') +          // Filler
                        " ".PadRight(40, ' ') +           // Filler

                        " ".PadRight(08, ' ') +           // Filler

                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +          // Filler
                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +           // Filler
                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +            // Filler

                        " ".PadRight(03, ' ') +            // Filler
                        " ".PadRight(20, ' ') +            // Filler
                        " ".PadRight(03, ' ') +            // Filler
                        " ".PadRight(20, ' ') +            // Filler
                        " ".PadRight(03, ' ') +          // Filler
                        " ".PadRight(20, ' ') +          // Filler

                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +          // Filler

                        " ".PadRight(03, ' ') +           // Filler
                        " ".PadRight(20, ' ') +           // Filler

                        " ".PadRight(166, ' ');          // Filler
            return s6;
        }

        // ========================================================================================
        // SUBSCRIBER AFORDABLE CARE ACT

        private string Get_Subscriber_Affordable_Care_Act_Information()
        {
            var s7 =
                        " ".PadRight(01, ' ') +         // Filler
                        " ".PadRight(08, ' ');          // Filler
            return s7;
        }



        // ========================================================================================
        //

        private void Output_CVX2000_Subscriber_Data(string[] memberdata)
        {
            Outputtodebug("Output_CVX2000_Subscriber_Data");

            var ssn = memberdata[0];
            var lastName = memberdata[1];
            var firstName = memberdata[2];
            var middleInitial = memberdata[3];
            var address1 = memberdata[4];

            var address2 = memberdata[5];
            var city = memberdata[6];
            var state = memberdata[7];
            var zip = memberdata[8];
            //	var homephone = memberdata[9];

            //	var workphone = memberdata[10];
            var dob = memberdata[11];
            var gender = memberdata[12];
            //	var group = memberdata[13];
            var plan = memberdata[14];

            //	var premium = memberdata[15];
            //	var coverage = memberdata[16];
            var benefitbegin = memberdata[17];
            //	var payfreq = memberdata[18];
            //	var emppays = memberdata[19];

            var groupnumber = memberdata[20];

            var s1 = Get_Subscriber_Identification(ssn, lastName, firstName, middleInitial, dob, gender);

            var s2 = Get_Subscriber_Demographics(address1, address2, city, state, zip);

            var s3 = Get_Subscriber_Coverage_Information(benefitbegin, groupnumber, plan);

            var s4 = Get_Subscriber_PCP_Information();

            var s5 = Get_Subscriber_HIPAA_Information();

            var s6 = Get_Subscriber_Member_Category_Codes();

            var s7 = Get_Subscriber_Affordable_Care_Act_Information();

            var s9 = s1 + s2 + s3 + s4 + s5 + s6 + s7;

            Output_CVXFile(s9 + Environment.NewLine);
        }

        // ========================================================================================
        // SUBSCRIBER IDENTIFICATION

        private string Get_Subscriber_Identification(string ssn, string lastName, string firstName,
            string middleInitial, string dob, string gender)
        {
            var s1 =
                          "S" +                               // Required Record Type
                          ssn.PadRight(12, ' ') +             // Required Subscriber ID
                          " ".PadRight(08, ' ') +             // Optional Filler
                          " ".PadRight(12, ' ') +             // Optional Alternate ID
                          " ".PadRight(06, ' ') +             // Optional Filler

                          " ".PadRight(02, ' ') +             // Optional Dependent Number
                          ssn.PadRight(09, ' ') +             // Optional Subscriber SSN
                          "ENGLISH".PadRight(11, ' ') +       // Optional Language
                          firstName.PadRight(25, ' ') +      // Required Subscriber First Name
                          middleInitial.PadRight(01, ' ') +  // Optional Subscriber Middle Initial

                          lastName.PadRight(35, ' ') +       // Required Subscriber Last Name
                          gender.PadRight(1, ' ') +           // Required Subscriber Gender
                          dob.PadRight(8, ' ') +             // Required Date of Birth
                          "N";                                // Optional Handicapped Indicator

            return s1;
        }








        // ========================================================================================

        private void Parse_Dependent_Line(string line,int start)
        {
            var i = start;

            var dependent1Lastname = Get_FieldOneBased(line, i, i + 19);

            var dependent1Firstname = Get_FieldOneBased(line, i + 20, i + 20 + 14);  // i + 20   i + 20 + 14

            var dependent1Middlename = Get_FieldOneBased(line, i + 34, i + 34);      // i + 34   i + 34

            var dependent1Num = Get_FieldOneBased(line, i + 35, i + 35 + 9);         // + 35  + i + 35 + 

            var dependent1Dob = Get_FieldOneBased(line, i + 45, i + 45 + 7);         // i + 45

            var dependent1Flag1 = Get_FieldOneBased(line, i + 53, i + 53);           // i + 53

            var dependent1Gender = Get_FieldOneBased(line, i + 54, i + 54);          // i + 54

            var dependent1Flag2 = Get_FieldOneBased(line, i + 55, i + 55);           // i + 55

            var dependent1Flag3 = Get_FieldOneBased(line, i + 56, i + 56);           // i + 56


            if ((dependent1Lastname.Trim() +
                 dependent1Firstname.Trim() +
                 dependent1Middlename.Trim() +
                 dependent1Num.Trim() +
                 dependent1Dob.Trim() +
                 dependent1Flag1.Trim() +
                 dependent1Gender.Trim() +
                 dependent1Flag2.Trim() +
                 dependent1Flag3.Trim()) != "")
            {

                Output("         dependent1_lastname       [" + dependent1Lastname + "]");
                Output("         dependent1_firstname      [" + dependent1Firstname + "]");
                Output("         dependent1_middlename     [" + dependent1Middlename + "]");


                Output("         dependent1_num            [" + dependent1Num + "]");
                Output("         dependent1_dob            [" + dependent1Dob + "]");
                Output("         dependent1_flag1          [" + dependent1Flag1 + "]");

                Output("         dependent1_gender         [" + dependent1Gender + "]");
                Output("         dependent1_flag2          [" + dependent1Flag2 + "]");
                Output("         dependent1_flag3          [" + dependent1Flag3 + "]");

                Output("");
            }


        }


        // ========================================================================================

        private void Parse_Line(string line)
        {
            var planyear = Get_FieldOneBased(line, 1, 4);

            var number = Get_FieldOneBased(line, 5, 14);

            var lastname = Get_FieldOneBased(line, 15, 34);

            var firstname = Get_FieldOneBased(line, 35, 49);

            var middlename = Get_FieldOneBased(line, 50, 50);

            var address1 = Get_FieldOneBased(line, 51, 70);

            var address2 = Get_FieldOneBased(line, 71, 90);

            var city = Get_FieldOneBased(line, 91, 105);

            var state = Get_FieldOneBased(line, 106, 107);

            var zip = Get_FieldOneBased(line, 108, 116);

            var num1 = Get_FieldOneBased(line, 117, 126);

            var num2 = Get_FieldOneBased(line, 127, 136);

            var dob = Get_FieldOneBased(line, 137, 144);

            var group = Get_FieldOneBased(line, 145, 147);

            var subgroup = Get_FieldOneBased(line, 148, 151);

            var plan = Get_FieldOneBased(line, 154, 156);

            var plan2 = Get_FieldOneBased(line, 161, 166);

            var plan3 = Get_FieldOneBased(line, 167, 167);

            var plan4 = Get_FieldOneBased(line, 168, 181);


            var date = Get_FieldOneBased(line, 182, 192);

            var num3 = Get_FieldOneBased(line, 192, 194);

            var flag = Get_FieldOneBased(line, 195, 194);

            Output("planyear    [" + planyear + "]");
            Output("number      [" + number + "]");
            Output("lastname    [" + lastname + "]");
            Output("firstname   [" + firstname + "]");
            Output("middlename  [" + middlename + "]");
            Output("address1    [" + address1 + "]");
            Output("address2    [" + address2 + "]");

            Output("city        [" + city + "]");
            Output("state       [" + state + "]");

            Output("zip         [" + zip + "]");
            Output("num1        [" + num1 + "]");

            Output("num2        [" + num2 + "]");
            Output("dob         [" + dob + "]");

            Output("group       [" + group + "]");
            Output("subgroup    [" + subgroup + "]");


            Output("plan        [" + plan + "]");
            Output("plan2       [" + plan2 + "]");
            Output("plan3       [" + plan3 + "]");
            Output("plan4       [" + plan4 + "]");

            Output("date        [" + date + "]");
            Output("num3        [" + num3 + "]");
            Output("flag        [" + flag + "]");


            var start = 209;
            for (var i = 1; i <= 12; i++)
            {
                Parse_Dependent_Line(line, start);
                start = start + 66;
            }


            Output("");
        }



        // ====================================================================================
        //

        private string[] Parse_Subscribers_From_Line(string singleLine)
        {
            Outputtodebug("Parse_Subscribers_From_Line");

            var groupNumber = singleLine.Substring(0, 5);

            var ssn = singleLine.Substring(5, 9);
            var lastName = singleLine.Substring(14, 20);
            var firstName = singleLine.Substring(34, 15);
            var middleInitial = singleLine.Substring(49, 1);

            var address1 = singleLine.Substring(50, 20);
            var address2 = singleLine.Substring(70, 20);
            var city = singleLine.Substring(90, 15);
            var state = singleLine.Substring(105, 2);
            var zip = singleLine.Substring(107, 9);

            var homephone = singleLine.Substring(116, 10);
            var workphone = singleLine.Substring(126, 10);
            var dob = singleLine.Substring(136, 8);
            var gender = singleLine.Substring(144, 1);
            var group = singleLine.Substring(145, 8);
            var plan = singleLine.Substring(153, 8);
            var premium = singleLine.Substring(161, 5);

            var coverage = singleLine.Substring(166, 1);
            var benefitbegin = singleLine.Substring(167, 8);
            var payfreq = singleLine.Substring(175, 2);
            var emppays = singleLine.Substring(177, 1);

            var data = new string[21];

            data[0] = ssn.Trim();
            data[1] = lastName.Trim();
            data[2] = firstName.Trim();
            data[3] = middleInitial.Trim();
            data[4] = address1.Trim();

            data[5] = address2.Trim();
            data[6] = city.Trim();
            data[7] = state.Trim();
            data[8] = zip.Trim();
            data[9] = homephone.Trim();

            data[10] = workphone.Trim();
            data[11] = dob.Trim();
            data[12] = gender.Trim();
            data[13] = group.Trim();
            data[14] = plan.Trim();

            data[15] = premium.Trim();
            data[16] = coverage.Trim();
            data[17] = benefitbegin.Trim();
            data[18] = payfreq.Trim();
            data[19] = emppays.Trim();

            data[20] = groupNumber.Trim();

            return data;
        }










            //GetInitialInputFile();

        // ========================================================================================
        //

        private void Output_CVXFile(string s1)
        {
            using (var w = File.AppendText(_archiveoutputfullfilepath))
            {
                w.Write(s1);
            }
        }





        // ========================================================================================

        //string _basepath = "";
        //string _archiveinputpath = "";
        //string _archiveoutputpath = "";
        //string _archiveoutputfullfilepath = "";

        string _archiveoutputpath = "";
        string _archiveoutputfullfilepath = "";		

        //string _enrollmentkickoffdirectory = "";

        private string _pathToFiles = "..\\..\\..\\zSource\\";

        string _fullpathfilename = "";

        //// Updated when button pushed
        //string _processedDate = DateTime.Now.ToString("yyyyMMdd");
        //string _processedTime = DateTime.Now.ToString("hhmmss");

        //string _fullpathfilename = "";

        //Stopwatch _sTest;

        //bool _fatalErrorCondition;
        
        
        
        
        private void GetInitialInputFile()
        {
            var p1 = Path.Combine(_pathToFiles, "CVXin_10P1701_20171012_080332829.Input");

            var p2 = Path.GetFullPath(p1);

            Output("Filename = " + p2);

            _fullpathfilename = p2;

            textBox2.Text = Path.GetFileName(p2);
        }


        
        
        






        // ========================================================================================
        //

        private void ProcessFile(string fullpathfilename)
        {

            Outputtodebug("ProcessFile");

            var acceptedLines = 0;

            var subscriberRecordCount = 0;

            var dependentRecordCount = 0;

            var droppedLines = 0;

           // Validate_Input_File(fullpathfilename);

            Create_CVX2000_Header();

            var lineBeingProcessed = 1;

            using (var sr = new StreamReader(fullpathfilename))
            {
                while (sr.Peek() >= 0)
                {
                    var singleLine = sr.ReadLine();

                    const int properlinelength = 1182;

                    if (singleLine != null && singleLine.Length != properlinelength)
                    {
                        var s = "                                                                                                   ";
                        Output(s + "Warning Incorrect Line Length (" + singleLine.Length + ") Should be " + properlinelength);
                        var id = singleLine.Substring(9, 36);
                        Output(s + id);
                        droppedLines = droppedLines + 1;

                    }
                    else
                    {

                        if (singleLine != null)
                        {

                            acceptedLines = acceptedLines + 1;
                            lineBeingProcessed = lineBeingProcessed + 1;

                            // Process Member Data
                            var memberData = Parse_Subscribers_From_Line(singleLine);


                            //	Print_Subscribers_Data(memberData);

                            Output_CVX2000_Subscriber_Data(memberData);

                            //subscriberRecordCount = subscriberRecordCount + 1;

                            //// Process Dependent Data
                            //var numDependents = Get_Num__Dependents_From_Line(singleLine);

                            //for (var i = 0; i < numDependents; i++)
                            //{
                            //    var offset = i * 66;

                            //    var dependentData = Parse_One_Dependent_From_Line(singleLine, offset);

                            //    //	Print_One_Dependent_Data(dependentData);

                            //    Output_CVX2000_Dependent_Data(memberData, dependentData);

                            //    dependentRecordCount = dependentRecordCount + 1;

                            //}


                            //if (_fatalErrorCondition == false)
                            //{
                            //    if (numDependents > 0)
                            //    {
                            //        OutputToScreen(acceptedLines.ToString().PadLeft(6, ' ') + " Processed " + memberData[1] + ", " + memberData[2] + " (" + numDependents + " Dependents)");
                            //    }
                            //    else
                            //    {
                            //        OutputToScreen(acceptedLines.ToString().PadLeft(6, ' ') + " Processed " + memberData[1] + ", " + memberData[2]);
                            //    }
                            //}

                        }






                    }

                    //    if (accepted_lines == 10) { break; }


                 //   if (_fatalErrorCondition) { break; }


                } // while (sr.Peek() >= 0)

            } // using (StreamReader sr = new StreamReader(s1))

            Create_CVX2000_Footer(subscriberRecordCount, dependentRecordCount);



        }













            //if (_fatalErrorCondition)
            //{
            //    OutputToScreen("");
            //    OutputToScreen("Fatal Error Detected (Output File Unreliable)");
            //}
            //else
            //{
            //    OutputToScreen("");
            //    OutputToScreen("Total subscriber_record_count = " + subscriberRecordCount);

            //    //OutputToScreen("");
            //    OutputToScreen("Total dependent_record_count = " + dependentRecordCount);

            //    //OutputToScreen("");
            //    OutputToScreen("Total Records Converted = " + (subscriberRecordCount + dependentRecordCount));

            //    //OutputToScreen("");
            //    OutputToScreen("");
            //    OutputToScreen("Total Lines Accepted = " + acceptedLines);

            //    OutputToScreen("");
            //    OutputToScreen("Total Lines Dropped (Incorrect Line Length) = " + droppedLines);

            //}





                //// for (var i = 0; i < list1.Count; i++)
                //for (var i = 0; i < 5; i++)
                //{
                //    Parse_Line(list1[i]);
                //    //var length = list1[1].Length;

                //    //  Output(length.ToString());
                //}
                
                
                
                
                
                



            // 1182 line length



            //// Load the data for checking null bytes
            //byte[] inputfilebytearray = File.ReadAllBytes(fullpathfilename);

            //int full_record_size = 1175;  // 1177 including OD OA line ending


            //Int64 size = inputfilebytearray.GetLength(0);

            //OutputToScreen(nl);
            //OutputToScreen(nl);

            //OutputToScreen("size = " + size.ToString() + nl);

            //var processedDate = DateTime.Now.ToString("yyyyMMdd");

            //var processedTime = DateTime.Now.ToString("hhmmss");

            //var outputfilename = "CVXIN_" + "10P1701" + "_" + processedDate + "_" + processedTime;

            //_archiveoutputfullfilepath = Path.Combine(_archiveoutputpath, outputfilename);


            //ProcessFile(_fullpathfilename);

            //LogConversionResults(outputfilename);

            //MoveSource(_fullpathfilename, _archiveinputpath, ".Input");

            //CopySource(_archiveoutputfullfilepath, _archiveoutputpath, ".Output");

            //// Now finally copy to the directory to initiate enrollment processing
            //MoveSource(_archiveoutputfullfilepath, _enrollmentkickoffdirectory, "");

