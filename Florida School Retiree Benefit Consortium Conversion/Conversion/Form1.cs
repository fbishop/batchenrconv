﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using CodeDebuggingNamespace;

namespace Conversion
{
	public partial class Form1 : Form
	{
		// ========================================================================================

		string _basepath = "";
		string _archiveinputpath = "";
		string _archiveoutputpath = "";
		string _archiveoutputfullfilepath = "";		
		string _enrollmentkickoffdirectory = "";

		// Updated when button pushed
		string _processedDate = DateTime.Now.ToString("yyyyMMdd");
		string _processedTime = DateTime.Now.ToString("hhmmss");

		string _fullpathfilename = "";

		Stopwatch _sTest;

		bool _fatalErrorCondition;

        private bool _ranonce;

		// ========================================================================================

		public Form1()
		{
			InitializeComponent();
		}

		// =============================================================================================

		private void Outputtodebug(string s1)
		{
			UDP_Debugging.LogFlat("Conversion \\Form1.cs " + s1);
		}

		// ========================================================================================
		//

		private void Output_CVXFile(string s1)
		{
			using (var w = File.AppendText(_archiveoutputfullfilepath))
			{
				w.Write(s1);
			}
		}

		// ========================================================================================
		//

		private void OutputToScreen(string s1)
		{
			Outputtodebug("OutputToScreen");

			textBox2.AppendText(s1 + Environment.NewLine);
			
			Outputtodebug(s1);
		}


        // ========================================================================================

        private void SetTitle()
        {
            const string baseFormTitle = "Florida School Retiree Benefit Consortium Conversion V";

            var major = Assembly.GetExecutingAssembly().GetName().Version.Major.ToString();

            var minor = Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString();

            var build = Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();

            Text = baseFormTitle + major + @"." + minor + @" (Build " + build + @")";
        }

        // ========================================================================================
        //

        private void Validate_Input_File(string fullpathfilename)
        {
            //// Load the data for checking null bytes
            //byte[] inputfilebytearray = File.ReadAllBytes(fullpathfilename);

            //int full_record_size = 1175;  // 1177 including OD OA line ending


            //Int64 size = inputfilebytearray.GetLength(0);

            //OutputToScreen(nl);
            //OutputToScreen(nl);

            //OutputToScreen("size = " + size.ToString() + nl);






            //if ((size % full_record_size) == 0)
            //{
            //    OutputToScreen("Matching Exact Bytes" + nl);

            //    OutputToScreen("Number of Records Found = " + (size / full_record_size).ToString() + nl);

            //}
            //else
            //{
            //    OutputToScreen("Not Matching Exact Bytes" + nl);

            //    OutputToScreen("Remainder = " + (size % full_record_size).ToString() + nl);

            //    OutputToScreen("Number of Records Found = " + (size / full_record_size).ToString() + nl);


            //}

            //OutputToScreen(nl);




        }

        //// ========================================================================================
        ////

        //private void Create_Dependent_CVX2000_Data(string[] data2)
        //{
        //    throw new NotImplementedException();
        //}

        //// ========================================================================================
        ////

        //private string[] Process_Dependent_Data(ref long one_based_index, ref byte[] inputfilebytearray)
        //{
        //    throw new NotImplementedException();
        //}




        // ========================================================================================
        // Used only when debugging

        private void Print_Subscribers_Data(string[] data)
        {
            var ssn = data[0];
            var lastName = data[1];
            var firstName = data[2];
            var middleInitial = data[3];
            var address1 = data[4];

            var address2 = data[5];
            var city = data[6];
            var state = data[7];
            var zip = data[8];
            var homephone = data[9];

            var workphone = data[10];
            var dob = data[11];
            var gender = data[12];
            var group = data[13];
            var plan = data[14];

            var premium = data[15];
            var coverage = data[16];
            var benefitbegin = data[17];
            var payfreq = data[18];
            var emppays = data[19];

            var groupNumber = data[20];

            OutputToScreen("");

            //OutputToScreen("group_number = " + group_number + nl);

            OutputToScreen("SSN = " + ssn);
            OutputToScreen("last_name = " + lastName);
            OutputToScreen("first_name = " + firstName);

            //OutputToScreen("middle_initial = " + middle_initial + nl);
            //OutputToScreen("address1 = " + address1 + nl);

            //OutputToScreen("address2 = " + address2 + nl);
            //OutputToScreen("city = " + city + nl);
            //OutputToScreen("state = " + state + nl);
            //OutputToScreen("zip = " + zip + nl);
            //OutputToScreen("homephone = " + homephone + nl);

            //OutputToScreen("workphone = " + workphone + nl);
            //OutputToScreen("dob = " + dob + nl);
            //OutputToScreen("gender = " + gender + nl);
            //OutputToScreen("group = " + group + nl);
            //OutputToScreen("plan = " + plan + nl);

            //OutputToScreen("premium = " + premium + nl);
            //OutputToScreen("coverage = " + coverage + nl);
            //OutputToScreen("benefitbegin = " + benefitbegin + nl);
            //OutputToScreen("payfreq = " + payfreq + nl);
            //OutputToScreen("emppays = " + emppays + nl);



        }

        // ========================================================================================
        // Used only when debugging

        private void Print_One_Dependent_Data(string[] dependentData)
        {
            var s = "                                                    ";

            var lastName = dependentData[0];
            var firstName = dependentData[1];
            var middleInitial = dependentData[2];
            var ssn = dependentData[3];
            var dob = dependentData[4];
            var type = dependentData[5];
            var gender = dependentData[6];
            var student = dependentData[7];
            var handicapped = dependentData[8];

            if (lastName.Trim() != "")
            {
                OutputToScreen(s + "last_name = " + lastName);
                OutputToScreen(s + "first_name = " + firstName);
                OutputToScreen(s + "middle_initial = " + middleInitial);
                OutputToScreen(s + "ssn = " + ssn);
                OutputToScreen(s + "dob = " + dob);
                OutputToScreen(s + "type = " + type);
                OutputToScreen(s + "gender = " + gender);
                OutputToScreen(s + "student = " + student);
                OutputToScreen(s + "handicapped = " + handicapped);
            }

        }

        // ========================================================================================
        //

        private bool Show_Size_Analysis(byte[] inputfilebytearray, int fullRecordSize)
        {
            // Check for size a multiple of 
            bool result;

            Int64 size = inputfilebytearray.GetLength(0);

            OutputToScreen("");
            OutputToScreen("");

            if ((size % fullRecordSize) == 0)
            {
                OutputToScreen("Matching Exact Bytes");

                OutputToScreen("Number of Records Found = " + (size / fullRecordSize));
                result = true;
            }
            else
            {
                OutputToScreen("Not Matching Exact Bytes");

                OutputToScreen("Remainder = " + (size % fullRecordSize));

                OutputToScreen("Number of Records Found = " + (size / fullRecordSize));

                result = false;
            }

            OutputToScreen("");

            return result;
        }

        // ====================================================================================
        //

        private string DisplayTime(Stopwatch sTime)
        {
            var baseHour = 0;
            var baseMinute = 0;
            var baseSecond = 0;

            var frequency = Stopwatch.Frequency;

            var nanosecPerTick = (1000L * 1000L * 1000L) / frequency;

            var absoluteNanoseconds = sTime.ElapsedTicks * nanosecPerTick;

            //		var nano = (int)(absoluteNanoseconds % 1000);

            absoluteNanoseconds = absoluteNanoseconds / 1000;

            //		var usec = (int)(absoluteNanoseconds % 1000);

            absoluteNanoseconds = absoluteNanoseconds / 1000;

            var msec = (int)(absoluteNanoseconds % 1000);

            absoluteNanoseconds = absoluteNanoseconds / 1000;

            var sec = (int)(absoluteNanoseconds);
            var min = 0;
            var hr = 0;
            var newSec = baseSecond + sec;

            if (newSec >= 60)
            {
                min = min + (newSec / 60);
                newSec = newSec % 60;
            }

            var newMin = baseMinute + min;

            if (newMin >= 60)
            {
                hr = hr + (newMin / 60);
                newMin = newMin % 60;
            }

            var newHr = baseHour + hr;

            if (newHr >= 24)
            {
                newHr = newHr % 24;
            }

            return newHr.ToString().PadLeft(2, '0')
                    + ":"
                    + newMin.ToString().PadLeft(2, '0')
                    + ":"
                    + newSec.ToString().PadLeft(2, '0')
                    + "."
                    + msec.ToString().PadLeft(3, '0');
        }




        // ========================================================================================
        //



		// ====================================================================================
		//

		private string[] Parse_Subscribers_From_Line(string singleLine)
		{
			Outputtodebug("Parse_Subscribers_From_Line");

			var groupNumber = singleLine.Substring(0, 5);

			var ssn = singleLine.Substring(5, 9);
			var lastName = singleLine.Substring(14, 20);
			var firstName = singleLine.Substring(34, 15);
			var middleInitial = singleLine.Substring(49, 1);

			var address1 = singleLine.Substring(50, 20);
			var address2 = singleLine.Substring(70, 20);
			var city = singleLine.Substring(90, 15);
			var state = singleLine.Substring(105, 2);
			var zip = singleLine.Substring(107, 9);

			var homephone = singleLine.Substring(116, 10);
			var workphone = singleLine.Substring(126, 10);
			var dob = singleLine.Substring(136, 8);
			var gender = singleLine.Substring(144, 1);
			var group = singleLine.Substring(145, 8);
			var plan = singleLine.Substring(153, 8);
			var premium = singleLine.Substring(161, 5);

			var coverage = singleLine.Substring(166, 1);
			var benefitbegin = singleLine.Substring(167, 8);
			var payfreq = singleLine.Substring(175, 2);
			var emppays = singleLine.Substring(177, 1);

			var data = new string[21];

			data[0] = ssn.Trim();
			data[1] = lastName.Trim();
			data[2] = firstName.Trim();
			data[3] = middleInitial.Trim();
			data[4] = address1.Trim();

			data[5] = address2.Trim();
			data[6] = city.Trim();
			data[7] = state.Trim();
			data[8] = zip.Trim();
			data[9] = homephone.Trim();

			data[10] = workphone.Trim();
			data[11] = dob.Trim();
			data[12] = gender.Trim();
			data[13] = group.Trim();
			data[14] = plan.Trim();

			data[15] = premium.Trim();
			data[16] = coverage.Trim();
			data[17] = benefitbegin.Trim();
			data[18] = payfreq.Trim();
			data[19] = emppays.Trim();

			data[20] = groupNumber.Trim();

			return data;
		}

		// ========================================================================================
		// Process header

		private void Create_CVX2000_Header()
		{
			Outputtodebug("Create_CVX2000_Header");

			Output_CVXFile("H");  // Record Type H for Header
			Output_CVXFile("P");  // File Status     T = test   P = production  O = open enrollment
			Output_CVXFile("DAVIS VISION".PadRight(40, ' '));  // Name
			Output_CVXFile("Florida School Retiree Benefit Consortium".PadRight(50, ' '));
			Output_CVXFile(DateTime.Now.ToString("yyyyMMdd"));  // File Create Date
			Output_CVXFile("F");                                // File Type  T, F or C
			//  Output_CVXFile(DateTime.Now.ToString("yyyyMMdd"));  // Full File Term Date
			Output_CVXFile("        ");  // Full File Term Date
			Output_CVXFile(" ");                                // Full File Reporting Criteria
			Output_CVXFile(" ".PadRight(1890, ' '));            // Filler
			Output_CVXFile(Environment.NewLine);
		}

		// ========================================================================================
		//

		private void Output_CVX2000_Subscriber_Data(string[] memberdata)
		{
			Outputtodebug("Output_CVX2000_Subscriber_Data");

			var ssn = memberdata[0];
			var lastName = memberdata[1];
			var firstName = memberdata[2];
			var middleInitial = memberdata[3];
			var address1 = memberdata[4];

			var address2 = memberdata[5];
			var city = memberdata[6];
			var state = memberdata[7];
			var zip = memberdata[8];
		//	var homephone = memberdata[9];

		//	var workphone = memberdata[10];
			var dob = memberdata[11];
			var gender = memberdata[12];
		//	var group = memberdata[13];
			var plan = memberdata[14];

		//	var premium = memberdata[15];
		//	var coverage = memberdata[16];
			var benefitbegin = memberdata[17];
		//	var payfreq = memberdata[18];
		//	var emppays = memberdata[19];

			var groupnumber = memberdata[20];

			var s1 = Get_Subscriber_Identification(ssn, lastName, firstName, middleInitial, dob, gender);

			var s2 = Get_Subscriber_Demographics(address1, address2, city, state, zip);

			var s3 = Get_Subscriber_Coverage_Information(benefitbegin, groupnumber, plan);

			var s4 = Get_Subscriber_PCP_Information();

			var s5 = Get_Subscriber_HIPAA_Information();

			var s6 = Get_Subscriber_Member_Category_Codes();

			var s7 = Get_Subscriber_Affordable_Care_Act_Information();

			var s9 = s1 + s2 + s3 + s4 + s5 + s6 + s7;

			Output_CVXFile(s9 + Environment.NewLine);
		}

		// ========================================================================================
		// SUBSCRIBER IDENTIFICATION

		private string Get_Subscriber_Identification(string ssn, string lastName, string firstName,
			string middleInitial, string dob, string gender)
		{
			var s1 =
						  "S" +                               // Required Record Type
						  ssn.PadRight(12, ' ') +             // Required Subscriber ID
						  " ".PadRight(08, ' ') +             // Optional Filler
						  " ".PadRight(12, ' ') +             // Optional Alternate ID
						  " ".PadRight(06, ' ') +             // Optional Filler

						  " ".PadRight(02, ' ') +             // Optional Dependent Number
						  ssn.PadRight(09, ' ') +             // Optional Subscriber SSN
						  "ENGLISH".PadRight(11, ' ') +       // Optional Language
						  firstName.PadRight(25, ' ') +      // Required Subscriber First Name
						  middleInitial.PadRight(01, ' ') +  // Optional Subscriber Middle Initial

						  lastName.PadRight(35, ' ') +       // Required Subscriber Last Name
						  gender.PadRight(1, ' ') +           // Required Subscriber Gender
						  dob.PadRight(8, ' ') +             // Required Date of Birth
						  "N";                                // Optional Handicapped Indicator

			return s1;
		}

		// ========================================================================================
		// SUBSCRIBER DEMOGRAPHICS

		private string Get_Subscriber_Demographics(string address1, string address2, string city, string state, string zip)
		{
			var s2 =
						address1.PadRight(55, ' ') +      // Required Address1
						address2.PadRight(55, ' ') +      // Optional Address2
						city.PadRight(30, ' ') +          // Required City
						state.PadRight(02, ' ') +         // Required State
						zip.PadRight(15, ' ') +           // Required Zip

						" ".PadRight(03, ' ') +           // Optional Country Code

						" ".PadRight(55, ' ') +           // Optional Alternate Address1
						" ".PadRight(55, ' ') +           // Optional Alternate Address2
						" ".PadRight(30, ' ') +           // Optional Alternate City
						" ".PadRight(02, ' ') +           // Optional Alternate State
						" ".PadRight(15, ' ') +           // Optional Alternate Zip

						" ".PadRight(03, ' ') +           // Optional Country Code
						" ".PadRight(01, ' ') +           // Optional Email Opt In/Out
						" ".PadRight(50, ' ') +           // Optional Email Adress
						" ".PadRight(10, ' ');            // Optional Telephone
			return s2;
		}

		// ========================================================================================
		// COVERAGE 

		private void Get_Coverage(string groupnumber, string plan, out string benefitRiderId, out string benefitContractId)
		{
			switch (groupnumber)
			{
				case "2001": // Brevard 
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000309";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000369";
						benefitContractId = "10P100000868";
					}
					break;

				case "2002": // Escambia
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000324";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000371";
						benefitContractId = "10P100000868";
					}
					break;

				case "2003": // Duval
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000323";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000370";
						benefitContractId = "10P100000868";
					}
					break;


				case "2005": // Charlotte
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000368";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000373";
						benefitContractId = "10P100000868";
					}
					break;


				case "2006": // Orange
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000367";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000372";
						benefitContractId = "10P100000868";
					}
					break;


				case "2007": // Putnam
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000428";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000430";
						benefitContractId = "10P100000868";
					}
					break;


				case "2008": // Flagler
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000427";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000429";
						benefitContractId = "10P100000868";
					}
					break;


				case "2009": // Jackson
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000449";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000454";
						benefitContractId = "10P100000868";
					}
					break;


				case "2010": // Gilchrist
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000448";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000453";
						benefitContractId = "10P100000868";
					}
					break;


				case "2011": // Washington
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000452";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000457";
						benefitContractId = "10P100000868";
					}
					break;


				case "2012": // Walton
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000451";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000456";
						benefitContractId = "10P100000868";
					}
					break;


				case "2013": // Osceola
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000450";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000455";
						benefitContractId = "10P100000868";
					}
					break;


                case "2014": // Calhoun
                    if (plan == "1FU")
                    {
                        benefitRiderId = "DE00000545";
                        benefitContractId = "10P100000701";
                    }
                    else
                    {
                        benefitRiderId = "DE00000546";
                        benefitContractId = "10P100000868";
                    }
                    break;



				default:
					if (plan == "1FU")
					{
						benefitRiderId = "DE00000309";
						benefitContractId = "10P100000701";
					}
					else
					{
						benefitRiderId = "DE00000369";
						benefitContractId = "10P100000868";
					}
					break;
			}

		}

		// ========================================================================================
		// SUBSCRIBER COVERAGE 

		private string Get_Subscriber_Coverage_Information(string subscriberBenefitbegin, string groupnumber2, string plan2)
		{
			string benefitRiderId2;
			string benefitContractId2;

			Get_Coverage(groupnumber2, plan2, out benefitRiderId2, out benefitContractId2);
			
			var s3 =
						"VIS" +                                         // Plan Type
						subscriberBenefitbegin.PadRight(08, ' ') +     // Benefit Begin Date  
						" ".PadRight(08, ' ') +                         // Benefit End Date  

						benefitRiderId2.PadRight(10, ' ') +            // Benefit Rider ID
						benefitContractId2.PadRight(30, ' ') +              // Benefit Contract ID
						"ACT".PadRight(3, ' ') +                        // Emplyoment Status

						" ".PadRight(01, ' ') +                         // Cobra Qualifying Event
						" ".PadRight(12, ' ') +                         // Original Subscriber ID
						" ".PadRight(8, ' ');                           // Filler
			return s3;
		}

		// ========================================================================================
		// SUBSCRIBER PCP INFORMATION

		private string Get_Subscriber_PCP_Information()
		{
			var s4 =
						" ".PadRight(40, ' ') +            // Optional Primary Care Physician Name
						" ".PadRight(40, ' ') +            // Optional Primary Care Physician ID
						" ".PadRight(55, ' ') +           // Optional Primary Care Physician Address1

						" ".PadRight(55, ' ') +           // Optional Primary Care Physician Address2
						" ".PadRight(30, ' ') +           // Optional Primary Care Physician City
						" ".PadRight(02, ' ') +           // Optional Primary Care Physician State

						" ".PadRight(15, ' ') +            // Optional Primary Care Physician Postal Code
						" ".PadRight(03, ' ') +            // Optional Primary Care Physician Country Code
						" ".PadRight(10, ' ') +            // Optional Primary Care Physician 

						" ".PadRight(50, ' ');          // Filler
			return s4;
		}

		// ========================================================================================
		// SUBSCRIBER HIPAA INFORMATION (Customer Individual Rights)

		private string Get_Subscriber_HIPAA_Information()
		{
			var s5 =
						" ".PadRight(05, ' ') +      // Filler
						" ".PadRight(25, ' ') +      // Filler
						" ".PadRight(35, ' ') +      // Filler
						" ".PadRight(20, ' ') +      // Filler
						" ".PadRight(05, ' ') +      // Filler

						" ".PadRight(25, ' ') +      // Filler
						" ".PadRight(35, ' ') +      // Filler
						" ".PadRight(20, ' ') +      // Filler
						" ".PadRight(05, ' ') +      // Filler
						" ".PadRight(25, ' ') +      // Filler

						" ".PadRight(35, ' ') +      // Filler
						" ".PadRight(20, ' ') +      // Filler
						" ".PadRight(01, ' ');       // Filler
			return s5;
		}

		// ========================================================================================
		// SUBSCRIBER MEMBER CATEGORY CODES

		private string Get_Subscriber_Member_Category_Codes()
		{
			var s6 =
						" ".PadRight(03, ' ') +          // Filler
						" ".PadRight(20, ' ') +           // Filler
						" ".PadRight(03, ' ') +           // Filler
						" ".PadRight(20, ' ') +          // Filler
						" ".PadRight(03, ' ') +          // Filler
						" ".PadRight(20, ' ') +           // Filler


						" ".PadRight(03, ' ') +           // Filler
						" ".PadRight(20, ' ') +           // Filler
						" ".PadRight(03, ' ') +          // Filler
						" ".PadRight(20, ' ') +           // Filler
						" ".PadRight(03, ' ') +          // Filler
						" ".PadRight(20, ' ') +           // Filler

						" ".PadRight(03, ' ') +           // Filler
						" ".PadRight(20, ' ') +           // Filler

						" ".PadRight(40, ' ') +           // Filler
						" ".PadRight(40, ' ') +          // Filler
						" ".PadRight(40, ' ') +           // Filler
						" ".PadRight(40, ' ') +          // Filler

						" ".PadRight(40, ' ') +           // Filler
						" ".PadRight(40, ' ') +           // Filler
					   " ".PadRight(40, ' ') +          // Filler
						" ".PadRight(40, ' ') +           // Filler

						" ".PadRight(08, ' ') +           // Filler

						" ".PadRight(03, ' ') +          // Filler
						" ".PadRight(20, ' ') +          // Filler
						" ".PadRight(03, ' ') +           // Filler
						" ".PadRight(20, ' ') +           // Filler
						" ".PadRight(03, ' ') +           // Filler
						" ".PadRight(20, ' ') +            // Filler

						" ".PadRight(03, ' ') +            // Filler
						" ".PadRight(20, ' ') +            // Filler
						" ".PadRight(03, ' ') +            // Filler
						" ".PadRight(20, ' ') +            // Filler
						" ".PadRight(03, ' ') +          // Filler
						" ".PadRight(20, ' ') +          // Filler

						" ".PadRight(03, ' ') +           // Filler
						" ".PadRight(20, ' ') +          // Filler

						" ".PadRight(03, ' ') +           // Filler
						" ".PadRight(20, ' ') +           // Filler

						" ".PadRight(166, ' ');          // Filler
			return s6;
		}

		// ========================================================================================
		// SUBSCRIBER AFORDABLE CARE ACT

		private string Get_Subscriber_Affordable_Care_Act_Information()
		{
			var s7 =
						" ".PadRight(01, ' ') +         // Filler
						" ".PadRight(08, ' ');          // Filler
			return s7;
		}

		// ========================================================================================
		//

		private void Output_CVX2000_Dependent_Data(string[] memberdata, string[] dependentdata)
		{
			Outputtodebug("Output_CVX2000_Dependent_Data");

			var subscriberSsn = memberdata[0];
	//		var subscriberLastName = memberdata[1];
	//		var subscriberFirstName = memberdata[2];
	//		var subscriberMiddleInitial = memberdata[3];
			var subscriberAddress1 = memberdata[4];

			var subscriberAddress2 = memberdata[5];
			var subscriberCity = memberdata[6];
			var subscriberState = memberdata[7];
			var subscriberZip = memberdata[8];
	//		var subscriberHomephone = memberdata[9];

	//		var subscriberWorkphone = memberdata[10];
	//		var subscriberDob = memberdata[11];
	//		var subscriberGender = memberdata[12];
	//		var subscriberGroup = memberdata[13];
			var subscriberPlan = memberdata[14];

	//		var subscriberPremium = memberdata[15];
	//		var subscriberCoverage = memberdata[16];
			var subscriberBenefitbegin = memberdata[17];
	//		var subscriberPayfreq = memberdata[18];
	//		var subscriberEmppays = memberdata[19];
			var subscriberGroupNumber = memberdata[20];

			var dependentLastName = dependentdata[0];
			var dependentFirstName = dependentdata[1];
			var dependentMiddleInitial = dependentdata[2];

			var dependentSsn = dependentdata[3];
			var dependentDob = dependentdata[4];

			var dependentType = dependentdata[5];
			var dependentGender = dependentdata[6];
			var dependentStudent = dependentdata[7];
			var dependentHandicapped = dependentdata[8];

			var dependentBenefitBeginDate = subscriberBenefitbegin;

			var d1 = Get_Dependent_Identification(subscriberSsn, dependentLastName,
				dependentFirstName, dependentMiddleInitial, dependentSsn, dependentDob, dependentType, dependentGender, dependentStudent, dependentHandicapped);
			//OutputToScreen("Length d1 = " + d1.Length.ToString() + nl);

			var d2 = Get_Dependent_Demographics(subscriberAddress1, subscriberAddress2, subscriberCity, subscriberState, subscriberZip);
			// OutputToScreen("Length d2 = " + d2.Length.ToString() + nl);

			var d3 = Get_Dependent_Coverage_Information(dependentBenefitBeginDate, subscriberGroupNumber, subscriberPlan);
			//OutputToScreen("Length d3 = " + d3.Length.ToString() + nl);

			var d4 = Get_Dependent_PCP_Information();
			//OutputToScreen("Length d4 = " + d4.Length.ToString() + nl);

			var d5 = Get_Dependent_HIPPA_Information();
			//OutputToScreen("Length d5 = " + d5.Length.ToString() + nl);

			var d6 = Get_Dependent_Member_Category_Codes();
			//OutputToScreen("Length d6 = " + d6.Length.ToString() + nl);

			var d9 = d1 + d2 + d3 + d4 + d5 + d6;
			//    OutputToScreen("Length d9 = " + d9.Length.ToString() + nl);

			Output_CVXFile(d9 + Environment.NewLine);
		}

		// ========================================================================================
		//
		private string Get_Dependent_Identification(string subscriberSsn, string dependentLastName,
			string dependentFirstName, string dependentMiddleInitial, string dependentSsn, string dependentDob,
			string dependentType, string dependentGender, string dependentStudent, string dependentHandicapped)
		{
			string relationshipCode;
			if (dependentType == "S")
			{
				relationshipCode = "02";
			}
			else
			{
				relationshipCode = "05";

			}

			var d1 =
						"D" +                                        // Record Type
						subscriberSsn.PadRight(12, ' ') +           // Subscriber ID
						" ".PadRight(08, ' ') +                      // Filler
						dependentSsn.PadRight(12, ' ') +            // Dependent ID
						" ".PadRight(08, ' ') +                      // Filler

						dependentSsn.PadRight(09, ' ') +             // Dependent SSN
						"ENGLISH".PadRight(11, ' ') +                 // Language
						dependentFirstName.PadRight(25, ' ') +      // Dependent First Name
						dependentMiddleInitial.PadRight(01, ' ') +  // Dependent Middle Initial
						dependentLastName.PadRight(35, ' ') +       // Dependent Last Name

						//dependentGender.PadRight(01, ' ') +        // Dependent Gender
						//dependentDob.PadRight(08, ' ') +           // Date of Birth
						//" " +                                       // Handicapped Indicator 
						//" ".PadRight(01, ' ') +                     // Student Status Code

						dependentGender.PadRight(01, ' ') +        // Dependent Gender
						dependentDob.PadRight(08, ' ') +           // Date of Birth
						dependentHandicapped +                                       // Handicapped Indicator 
						dependentStudent.PadRight(01, ' ') +                     // Student Status Code

						relationshipCode.PadRight(02, ' ');                      // Relationship Code

			return d1;
		}

		// ========================================================================================
		//

		private string Get_Dependent_Demographics(string subscriberAddress1, string subscriberAddress2,
			string subscriberCity, string subscriberState, string subscriberZip)
		{
			var d2 =
			subscriberAddress1.PadRight(55, ' ') +     // Address1
			subscriberAddress2.PadRight(55, ' ') +     // Address2
			subscriberCity.PadRight(30, ' ') +         // City
			subscriberState.PadRight(02, ' ') +         // State
			subscriberZip.PadRight(15, ' ') +          // Zip

			" ".PadRight(03, ' ') +                      // Optional Country Code
			" ".PadRight(01, ' ') +                      // Optional Email Opt In/Out
			" ".PadRight(50, ' ') +                     // Optional Email Adress
			" ".PadRight(10, ' ');                      // Optional Telephone

			return d2;
		}

		// ========================================================================================
		// DEPENDENT COVERAGE INFORMATION

		private string Get_Dependent_Coverage_Information(string dependentBenefitBeginDate, string groupnumber3, string plan3)
		{
			string benefitRiderId3;
			string benefitContractId3;

			Get_Coverage(groupnumber3, plan3, out benefitRiderId3, out benefitContractId3);

			var d3 =
					   dependentBenefitBeginDate.PadRight(08, ' ') +              // Benefit Begin Date  
					  " ".PadRight(8, ' ') +              // Benefit End Date  

					  benefitRiderId3.PadRight(10, ' ') +   // Benefit Rider ID
					  benefitContractId3.PadRight(30, ' ') +  // Benefit Contract ID

					  " ".PadRight(3, ' ');           // Emplyoment Status

			return d3;
		}

		// ========================================================================================
		// DEPENDENT PCP INFORMATION

		private string Get_Dependent_PCP_Information()
		{
			var d4 =
						" ".PadRight(40, ' ') +           // Primary Care Physician Name
						" ".PadRight(40, ' ') +           // Primary Care Physician ID
						" ".PadRight(55, ' ') +           // Primary Care Physician Address1

						" ".PadRight(55, ' ') +           // Primary Care Physician Address2
						" ".PadRight(30, ' ') +           // Primary Care Physician City
						" ".PadRight(02, ' ') +           // Primary Care Physician State

						" ".PadRight(15, ' ') +           // Primary Care Physician Postal Code
						" ".PadRight(03, ' ') +           // Primary Care Physician Country Code
						" ".PadRight(10, ' ') +           // Primary Care Physician 

						" ".PadRight(50, ' ');            // Filler
			return d4;
		}

		// ========================================================================================
		// DEPENDENT HIPAA INFORMATION (Customer Individual Rights)

		private string Get_Dependent_HIPPA_Information()
		{
			var d5 =
					   " ".PadRight(05, ' ') +             // Filler
					   " ".PadRight(25, ' ') +             // Filler
					   " ".PadRight(35, ' ') +             // Filler
					   " ".PadRight(20, ' ') +             // Filler
					   " ".PadRight(05, ' ') +             // Filler

					   " ".PadRight(25, ' ') +             // Filler
					   " ".PadRight(35, ' ') +             // Filler
					   " ".PadRight(20, ' ') +             // Filler
					   " ".PadRight(05, ' ') +             // Filler
					   " ".PadRight(25, ' ') +             // Filler

					   " ".PadRight(35, ' ') +             // Filler
					   " ".PadRight(19, ' ') +             // Filler
					   " ".PadRight(1, ' ');               // Filler
			return d5;
		}

		// ========================================================================================
		// DEPENDENT MEMBER CATEGORY CODES

		private string Get_Dependent_Member_Category_Codes()
		{
			var d6 =
						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler
						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler
						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler

						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler
						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler
						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler

						" ".PadRight(3, ' ') +              // Filler

						" ".PadRight(20, ' ') +             // Filler

						" ".PadRight(40, ' ') +             // Filler
						" ".PadRight(40, ' ') +             // Filler
						" ".PadRight(40, ' ') +             // Filler
						" ".PadRight(40, ' ') +             // Filler

						" ".PadRight(40, ' ') +             // Filler
						" ".PadRight(40, ' ') +             // Filler
						" ".PadRight(40, ' ') +             // Filler
						" ".PadRight(40, ' ') +             // Filler

					   " ".PadRight(182, ' ') +             // Filler

						" ".PadRight(08, ' ') +             // Filler

						" ".PadRight(03, ' ') +             // Filler
						" ".PadRight(20, ' ') +             // Filler
						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler
						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler

						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler
						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler
						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler

						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler
						" ".PadRight(3, ' ') +              // Filler
						" ".PadRight(20, ' ') +             // Filler

						" ".PadRight(175, ' ');             // Filler
			return d6;
		}

		// ========================================================================================
		//

		private void Create_CVX2000_Footer(int subscriberRecordCount, int dependentRecordCount)
		{
			Outputtodebug("Create_CVX2000_Footer");
			var totalRecordCount = subscriberRecordCount + dependentRecordCount;
			Output_CVXFile("T");
			Output_CVXFile(totalRecordCount.ToString().PadLeft(10, '0'));
			Output_CVXFile(subscriberRecordCount.ToString().PadLeft(10, '0'));
			Output_CVXFile(dependentRecordCount.ToString().PadLeft(10, '0'));
			Output_CVXFile(" ".PadRight(1969, ' '));             // Filler
			Output_CVXFile(Environment.NewLine);
		}

		// ========================================================================================
		//

		private void ProcessFile(string fullpathfilename)
		{

			Outputtodebug("ProcessFile");

			var acceptedLines = 0;

			var subscriberRecordCount = 0;

			var dependentRecordCount = 0;

			var droppedLines = 0;

			Validate_Input_File(fullpathfilename);

			Create_CVX2000_Header();

			var lineBeingProcessed = 1;

			using (var sr = new StreamReader(fullpathfilename))
			{
				while (sr.Peek() >= 0)
				{
					var singleLine = sr.ReadLine();

					const int properlinelength = 1182;

					if (singleLine != null && singleLine.Length != properlinelength)
					{
						var s = "                                                                                                   ";
						OutputToScreen(s + "Warning Incorrect Line Length (" + singleLine.Length + ") Should be " + properlinelength);
						var id = singleLine.Substring(9, 36);
						OutputToScreen(s + id);
						droppedLines = droppedLines + 1;

					}
					else
					{

						if (singleLine != null)
						{

							acceptedLines = acceptedLines + 1;
							lineBeingProcessed = lineBeingProcessed + 1;

							// Process Member Data
							var memberData = Parse_Subscribers_From_Line(singleLine);
						
							
						//	Print_Subscribers_Data(memberData);

							Output_CVX2000_Subscriber_Data(memberData);

							subscriberRecordCount = subscriberRecordCount + 1;

							// Process Dependent Data
							var numDependents = Get_Num__Dependents_From_Line(singleLine);

							for (var i = 0; i < numDependents; i++)
							{
								var offset = i * 66;

								var dependentData = Parse_One_Dependent_From_Line(singleLine, offset);

							//	Print_One_Dependent_Data(dependentData);

								Output_CVX2000_Dependent_Data(memberData, dependentData);

								dependentRecordCount = dependentRecordCount + 1;

							}


							if (_fatalErrorCondition == false)
							{
								if (numDependents > 0)
								{
									OutputToScreen(acceptedLines.ToString().PadLeft(6, ' ') + " Processed " + memberData[1] + ", " + memberData[2] + " (" + numDependents + " Dependents)");
								}
								else
								{
									OutputToScreen(acceptedLines.ToString().PadLeft(6, ' ') + " Processed " + memberData[1] + ", " + memberData[2]);
								}
							}

						}






					}

					//    if (accepted_lines == 10) { break; }


					if (_fatalErrorCondition) { break; }


				} // while (sr.Peek() >= 0)

			} // using (StreamReader sr = new StreamReader(s1))

			Create_CVX2000_Footer(subscriberRecordCount, dependentRecordCount);

			if (_fatalErrorCondition)
			{
				OutputToScreen("");
				OutputToScreen("Fatal Error Detected (Output File Unreliable)");
			}
			else
			{
				OutputToScreen("");
				OutputToScreen("Total subscriber_record_count = " + subscriberRecordCount);

				//OutputToScreen("");
				OutputToScreen("Total dependent_record_count = " + dependentRecordCount);

				//OutputToScreen("");
				OutputToScreen("Total Records Converted = " + (subscriberRecordCount + dependentRecordCount));

				//OutputToScreen("");
				OutputToScreen("");
				OutputToScreen("Total Lines Accepted = " + acceptedLines);

				OutputToScreen("");
				OutputToScreen("Total Lines Dropped (Incorrect Line Length) = " + droppedLines);

			}

		}


		// ========================================================================================
		//

		private string[] Parse_One_Dependent_From_Line(string singleLine, int offset)
		{
			//var col = 0;

			Outputtodebug("Parse_One_Dependent_From_Line");

			var data = new string[9];

			// initialize string array to all blanks
			for (var i = 0; i < data.Length; i++) { data[i] = ""; }

		//	col = 208 + offset;
			var lastName = singleLine.Substring(208 + offset, 20);

		//	col = 228 + offset;
			var firstName = singleLine.Substring(228 + offset, 15);

		//	col = 243 + offset;
			var middleInitial = singleLine.Substring(243 + offset, 1);

		//	col = 244 + offset;
			var ssn = singleLine.Substring(244 + offset, 9);

			//col = 253 + offset;
			var dob = singleLine.Substring(253 + offset, 8);

			//col = 261 + offset;
			var type = singleLine.Substring(261 + offset, 1);

			//col = 262 + offset;
			var gender = singleLine.Substring(262 + offset, 1);

			var student = singleLine.Substring(263 + offset, 1);

			var handicapped = singleLine.Substring(264 + offset, 1);

			data[0] = lastName.Trim();
			data[1] = firstName.Trim();
			data[2] = middleInitial.Trim();
			data[3] = ssn.Trim();
			data[4] = dob.Trim();
			data[5] = type.Trim();
			data[6] = gender.Trim();
			data[7] = student.Trim();
			data[8] = handicapped.Trim();







			return data;

		}

		// ========================================================================================
		//

		private int Get_Num__Dependents_From_Line(string singleLine)
		{

			Outputtodebug("Get_Num__Dependents_From_Line");

			var cnt = 0;
			//  int sz = 65;

			try
			{
				var lastName01 = singleLine.Substring(203, 20); if (lastName01.Trim() != "") { cnt = cnt + 1; }
				var lastName02 = singleLine.Substring(268, 20); if (lastName02.Trim() != "") { cnt = cnt + 1; }
				var lastName03 = singleLine.Substring(333, 20); if (lastName03.Trim() != "") { cnt = cnt + 1; }
				var lastName04 = singleLine.Substring(398, 20); if (lastName04.Trim() != "") { cnt = cnt + 1; }
				var lastName05 = singleLine.Substring(463, 20); if (lastName05.Trim() != "") { cnt = cnt + 1; }
			}
			catch (ArgumentOutOfRangeException ex1)
			{
				// OutputToScreen(ex1.ToString() + "\r\n");
				OutputToScreen(ex1.Message + "\r\n");
				// OutputToScreen(ex1.StackTrace.ToString() + "\r\n");
				OutputToScreen("Method = " + ex1.TargetSite + "\r\n");
				// OutputToScreen(ex1 + "\r\n");
				// OutputToScreen(ex1.GetType() + "\r\n");
				_fatalErrorCondition = true;
			}

			return cnt;
		}





		// ========================================================================================
		// Choose a different file

		private void btn_Choose_Source_File_Click(object sender, EventArgs e)
		{
			Outputtodebug("btn_Choose_Source_File_Click");

			openFileDialog1.InitialDirectory = _basepath;

			openFileDialog1.FileName = "";

			openFileDialog1.Multiselect = false;                  //sets to multiple selects

			// Show the dialog and get result.
			var result = openFileDialog1.ShowDialog();

			// Test result.
			if (result == DialogResult.OK)
			{
				// Create a List Of Files To Process

				// loop through each file
				foreach (var filenametouse in openFileDialog1.FileNames)
				{
					textBox1.Text = filenametouse;
				}
			}
		}




		// ========================================================================================
		//

		private void CopySource(string sourcepath, string destinationpath, string addtodestination)
		{
			// first get the filename

			var filename = Path.GetFileName(sourcepath);

			try
			{
				File.Copy(sourcepath, Path.Combine(destinationpath, filename + addtodestination));
			}
			catch (Exception ex)
			{

				OutputToScreen("Error Coping File To " + destinationpath);
				OutputToScreen(ex.Message);
			}



		}

		// ========================================================================================
		//

		private void MoveSource(string sourcepath, string destinationpath, string addtodestination)
		{
			// first get the filename

			var filename = Path.GetFileName(sourcepath);

			var output = Path.Combine(destinationpath, filename + addtodestination);

			if (File.Exists(output)) {  File.Delete(output); }

			File.Copy(sourcepath, output);

			File.Delete(sourcepath);

		}

		// ========================================================================================
		//

		private void SetGlobalPaths()
		{
			try
			{


				_basepath = @"\\hvhcvision.com\Files\Apps\CVXFTP\Prod\XS5FRC\";
				_enrollmentkickoffdirectory = @"\\hvhcvision.com\Files\Apps\CVXFTP\Prod\incoming\";


				//_basepath = @"C:\DotNetApps_Output\XS5FRC\";
				//_enrollmentkickoffdirectory = @"C:\DotNetApps_Output\XS5FRC\Archive\Final\";


				// first build a list of all the files

				Outputtodebug("SOURCE PATH = " + _basepath);

				var dt1 = DateTime.Now;
				var cYear = dt1.Year.ToString().PadLeft(4, '0');

				_archiveinputpath = Path.Combine(_basepath, "Archive");
				_archiveinputpath = Path.Combine(_archiveinputpath, "Inputs");
				_archiveinputpath = Path.Combine(_archiveinputpath, cYear);


				Outputtodebug("ARCHIVE INPUT PATH = " + _archiveinputpath);
				if (!(Directory.Exists(_archiveinputpath)))
				{
					Directory.CreateDirectory(_archiveinputpath);
				}

				_archiveoutputpath = Path.Combine(_basepath, "Archive");
				_archiveoutputpath = Path.Combine(_archiveoutputpath, "Outputs");
				_archiveoutputpath = Path.Combine(_archiveoutputpath, cYear);


				Outputtodebug("ARCHIVE OUTPUT PATH = " + _archiveoutputpath);
				if (!(Directory.Exists(_archiveoutputpath)))
				{
					Directory.CreateDirectory(_archiveoutputpath);
				}




			}
			catch (Exception ex)
			{
				OutputToScreen("SOURCE PATH ISSUES");
				OutputToScreen(_basepath);

				OutputToScreen(ex.Message);

				_basepath = "";
			}


		}

		// ========================================================================================
		//

		private void Form1_Load(object sender, EventArgs e)
		{
			SetTitle();

			Outputtodebug("Form1_Load");

			textBox1.Clear();

			SetGlobalPaths();

			if (_basepath != "")
			{
				// Load up all the files in the target directory
				var files = Directory.GetFiles(_basepath);

				Outputtodebug("FILES FOUND = " + files.Length);

				foreach (var s in files)
				{
					var fileNameext = Path.GetExtension(s);

					_fullpathfilename = Path.GetFullPath(s);

					var matchinguppercaseextension = "";

					if (fileNameext != null && fileNameext.ToUpper() == matchinguppercaseextension)
					{
						textBox1.AppendText(_fullpathfilename);
						break;
					}
				}	


			}

			_sTest = new Stopwatch();

		}

		// ========================================================================================
		// Convert Button

		private void btn_Convert_File_Click(object sender, EventArgs e)
		{
			Outputtodebug("btn_Convert_File_Click");

			textBox2.Clear();

			_processedDate = DateTime.Now.ToString("yyyyMMdd");

			_processedTime = DateTime.Now.ToString("hhmmss");

			if (textBox1.Text.Trim() != "")
			{

				_fullpathfilename = Path.GetFullPath(textBox1.Text.Trim());

				OutputToScreen("Processing File: " + _fullpathfilename);
				OutputToScreen("");

				if (File.Exists(_fullpathfilename))
				{
					// To Use and Reuse the timer
					// Use at Beginning (In Method Under Test)
					_sTest.Reset();
					_sTest.Start();

					var outputfilename = "CVXIN_" + "10P1701" + "_" + _processedDate + "_" + _processedTime;

     				_archiveoutputfullfilepath = Path.Combine(_archiveoutputpath, outputfilename);

					ProcessFile(_fullpathfilename);

					LogConversionResults(outputfilename);

					MoveSource(_fullpathfilename, _archiveinputpath, ".Input");

					CopySource(_archiveoutputfullfilepath, _archiveoutputpath, ".Output");

					// Now finally copy to the directory to initiate enrollment processing
					MoveSource(_archiveoutputfullfilepath, _enrollmentkickoffdirectory, "");


					// Use at End (In Method Under Test)
					_sTest.Stop();

					OutputToScreen("");
					OutputToScreen("");
					OutputToScreen("Elapsed Time: " + DisplayTime(_sTest));
					// Elapsed Time: 00:00:00.016.504   this is hours:minutes:seconds.milliseconds.microseconds

				}

			}
			else
			{
			    _fullpathfilename = "(no input file)";


				OutputToScreen("Input File: " + Path.GetFileName(_fullpathfilename) + " Not Found");
			}
		}

		// ========================================================================================
		//

		private void LogConversionResults(string outputfilename)
		{

			var logpathonly = Path.Combine(_basepath, "Logs");

			Outputtodebug("LOG PATH = " + logpathonly);
			if (!(Directory.Exists(logpathonly)))
			{
				Directory.CreateDirectory(logpathonly);
			}

			var dt1 = DateTime.Now;
			var cYear = dt1.Year.ToString().PadLeft(4, '0');

			var fulllogfilenameandpath = Path.Combine(logpathonly, cYear + "_Files_Processed.txt");


			var infile = Path.GetFileName(_fullpathfilename);
			var outfile = outputfilename;

			var stamp = DateTime.Now.ToString("[yyyy-MM-dd]") + DateTime.Now.ToString("[hh:mm:ss]");

			try
			{
				using (var w = File.AppendText(fulllogfilenameandpath))
				{
					w.WriteLine(stamp + "Input File:  " + infile);
					w.WriteLine(stamp + "Output File: " + outfile);
					w.WriteLine("");
				}
			}
			catch (Exception ex)
			{
				OutputToScreen(ex.Message);
			}
		}

		// ========================================================================================
		//

		private void textBox2_KeyDown(object sender, KeyEventArgs e)
		{
			if ((e.Control) && (e.KeyCode == Keys.A))
			{
				textBox2.SelectAll();
				e.Handled = true;
			}
		}


        // ========================================================================================
        //

        private static string Getuser()
        {
            // System.Security.Principal.WindowsIdentity.GetCurrent().Name  FQDN
            return Environment.UserName;
        }


        // ========================================================================================
        //

        private static string Getmachine()
        {
            // System.Environment.MachineName from a console or WinForms app.
            // HttpContext.Current.Server.MachineName from a web app
            // System.Net.Dns.GetHostName() to get the FQDN


            return Environment.MachineName;
        }

        // ========================================================================================
        //

        private void Form1_Activated(object sender, EventArgs e)
        {
            Outputtodebug("Form1_Activated");

            if (!(_ranonce))
            {
                // Output("Form1_Activated");



                OutputToScreen("Run as: " + Getuser());

                OutputToScreen("Run on: " + Getmachine());


                OutputToScreen("_basepath   = " + _basepath);
                OutputToScreen("_archiveinputpath = " + _archiveinputpath);
                OutputToScreen("_archiveoutputpath  = " + _archiveoutputpath);

                OutputToScreen("_enrollmentkickoffdirectory   = " + _enrollmentkickoffdirectory);

                _ranonce = true;

                var args = Environment.GetCommandLineArgs();

                if (args.Length > 1)
                {
                    var arg1 = args[1].ToUpper();

                    if (arg1 == "AUTO")
                    {

                        OutputToScreen(DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " Auto Run");

                        // this will block till complete
                        btn_Convert_File_Click(new object(), new EventArgs());
                    }
                    else
                    {
                        //  do nothing incorrect command line arguments

                        OutputToScreen(DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " Manual Run");
                    }

                    // Close the windows form
                    BeginInvoke(new MethodInvoker(Close));

                }
            }
        }

		// ========================================================================================

	}
}
