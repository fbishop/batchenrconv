Purpose: Take an incomming file from State of TN and convert to our standard DV2000 enrollment file layout

Version 1.1     2017 11 09      Initial Code From Scratch

Version 1.2     2017 11 10      Implemented mapping for rider and benefit contract
                                Implemented conversion for subscriber id which starts with C

Version 1.3     2017 11 10      Added switch to rider NR00000001 when SPO, IND, DEP, CHD or SPC 

Version 1.4     2017 11 27      Refactoring

Version 1.5     2017 11 27      Removed dead code and adjusted for new mapping
                                Added support for auto command line parameter

Version 1.7     2018 01 15      Added automation      

-----------------------------------------------------------------------------------------------
Ideas

done - respond to auto Auto AUTO

look at input files matching beginning pattern

email when file converted

config file for smtp server & emailto address

-----------------------------------------------------------------------------------------------

Bugs 

-----------------------------------------------------------------------------------------------

Mods

-----------------------------------------------------------------------------------------------

Other
