Del /A:H *.v12.suo
Del /A:H *.suo

cd ConvertToDV2000

dir

@IF NOT EXIST .\bin GOTO nextitem
@Echo Removing the bin directory structure
rd .\bin /s/q

: nextitem

@IF NOT EXIST .\obj GOTO bottom
@Echo Removing the obj directory structure
rd .\obj /s/q
GOTO end
: bottom

pause

