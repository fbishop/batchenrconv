﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace ConvertToDV2000
{

    public partial class Form1 : Form
    {

        // ========================================================================================
       
        private bool _ranonce;


        private string _pathToInputFiles = "..\\..\\..\\zSource\\01Input\\";
        private string _pathToArchiveFiles = "..\\..\\..\\zSource\\02Archive\\";
        private string _pathToOutputFiles = "..\\..\\..\\zSource\\03Output\\";

        private string _logarchivepath = "";

        private string _pathToFiles = "..\\..\\..\\zSource\\";

        private string _fullpathfilename = "";
        private string _fulllogfile = "";
        private string _inputarchivepath = "";

        private string _outputarchivepath = "";

        private int _numberofCsfound;


        // ========================================================================================
        //

        private void OutputToLog(string p)
        {
            try
            {
                using (var writer = File.AppendText(_fulllogfile))
                {
                    writer.WriteLine(p);
                }
            }
            catch (Exception ex)
            {
                //
            }
        }

        // ========================================================================================

        private void Output(string s1)
        {
            textBox1.AppendText(s1 + Environment.NewLine);

            OutputToLog(s1);
        }

        // ========================================================================================

        private string Dash(int p)
        {
            var s = "";
            for (var i = 0; i < p; i++) { s = s + "-"; }
            return s;
        }

        // ========================================================================================

        public Form1()
        {
            InitializeComponent();
        }

        // ========================================================================================

        private void SetTitle()
        {
            const string baseFormTitle = "State of TN Convert To DV2000 V";

            var major = Assembly.GetExecutingAssembly().GetName().Version.Major.ToString();

            var minor = Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString();

            var build = Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();

            Text = baseFormTitle + major + @"." + minor + @" (Build " + build + @")";
        }

        // ====================================================================================
        //

        private static string DisplayTime6(Stopwatch sTime)
        {
            var frequency = Stopwatch.Frequency;
            var nanosecPerTick = (1000L * 1000L * 1000L) / frequency;
            var absoluteNanoseconds = sTime.ElapsedTicks * nanosecPerTick;
            absoluteNanoseconds = absoluteNanoseconds / 1000;
            absoluteNanoseconds = absoluteNanoseconds / 1000;
            var msec = (int)(absoluteNanoseconds % 1000);
            absoluteNanoseconds = absoluteNanoseconds / 1000;
            var sec = (int)(absoluteNanoseconds);

            var mins = 0; var hrs = 0; var day = 0; var newSec = sec;

            if (newSec >= 60)
            { mins = (newSec / 60); newSec = newSec % 60; }

            var newMin = mins;

            if (newMin >= 60)
            { hrs = (newMin / 60); newMin = newMin % 60; }

            var newHr = hrs;

            if (newHr >= 24)
            { day = (newHr / 24); newHr = newHr % 24; }

            var newDay = day;

            if (newDay > 0)
            { return "[Days = " + newDay + "] " + "[Hours = " + newHr + "]"; }

            if (newHr > 0)
            { return "[Hours = " + newHr + "] " + "[Minutes = " + newMin + "]"; }

            if (newMin > 0)
            { return "[Minutes = " + newMin + "] " + "[Seconds = " + newSec + "]"; }

            return "[Seconds = " + newSec + "] " + "[MilliSeconds = " + msec + "]";
        }

        // ========================================================================================

        private string SetInitialFile(string path)
        {
            var files = Directory.GetFiles(path);

            if (files.Length > 0)
            {



                var list1 = FilterByExtension(files);


                var list2 = FilterByStartingPattern(list1);


                if (list2.Count > 0)
                {
                    return list2[0];

                }

                return "";
            }
            else
            {
                return "";
            }

        }

        // ========================================================================================

        private void CreateAndOrSetArchiveSubdirectories(string newdir1)
        {
            Output("CreateAndOrSetArchiveSubdirectories");

            _inputarchivepath = Path.Combine(newdir1, "InputFiles");
            if (!Directory.Exists(_inputarchivepath))
            {
                Directory.CreateDirectory(_inputarchivepath);
            }

            _outputarchivepath = Path.Combine(newdir1, "OutputFiles");
            if (!Directory.Exists(_outputarchivepath))
            {
                Directory.CreateDirectory(_outputarchivepath);
            }

            _logarchivepath = Path.Combine(newdir1, "LogFiles");
            if (!Directory.Exists(_logarchivepath))
            {
                Directory.CreateDirectory(_logarchivepath);
            }

        }

        // ========================================================================================

        private void SetInitialPathOperations()
        {
            // First set to current if not in debug mode
            if (!Directory.Exists(_pathToInputFiles))
            {
                _pathToInputFiles = @".";
            }

            // Next Set up the Archive Directories
            if (!Directory.Exists(_pathToArchiveFiles))
            {
                _pathToArchiveFiles = @".";
                // Setup archive directories 
                _pathToArchiveFiles = Path.Combine(_pathToArchiveFiles, "Archive");
                if (!Directory.Exists(_pathToArchiveFiles))
                {
                    Directory.CreateDirectory(_pathToArchiveFiles);
                }
            }
            CreateAndOrSetArchiveSubdirectories(_pathToArchiveFiles);


            var logfilename = "Log_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".txt";
            _fulllogfile = Path.Combine(Path.GetFullPath(_logarchivepath), logfilename);


            // Lastly set up the Output Directory
            if (!Directory.Exists(_pathToOutputFiles))
            {
                _pathToOutputFiles = @"\\ad.davisvision.com\Files\Apps\CVXFTP\Prod\incoming";
            }


            var inputdirfullpath = Path.GetFullPath(_pathToInputFiles);

            _fullpathfilename = SetInitialFile(inputdirfullpath);

            textBox2.Text = Path.GetFileName(_fullpathfilename);

            _pathToArchiveFiles = Path.GetFullPath(_pathToArchiveFiles);
            _pathToInputFiles = Path.GetFullPath(_pathToInputFiles);
            _pathToOutputFiles = Path.GetFullPath(_pathToOutputFiles);
        }


        // ========================================================================================

        private static List<string> FilterByStartingPattern(List<string> files)
        {
            var list1 = new List<string>();

            foreach (var line in files)
            {
                var filenameonly = Path.GetFileName(line);

                if (filenameonly != null)
                {
                    if (filenameonly.StartsWith("CVXin_10P11216"))
                    {
                        list1.Add(line);
                    }
                }
            }

            return list1;
        }

        // ========================================================================================

        private static List<string> FilterByExtension(string[] files)
        {
            var list1 = new List<string>();

            // filter out certain extensions
            foreach (var line in files)
            {
                var s1 = Path.GetExtension(line);

                if (s1 != null)
                {
                    if (s1.ToUpper() != ".EXE")
                    {
                        //if (s1.ToUpper() != ".TXT")
                        {
                            //if (s1.ToUpper() != ".INPUT")
                            {
                                list1.Add(line);
                            }
                        }
                    }
                }
            }

            return list1;
        }

        // ========================================================================================

        private void Form1_Load(object sender, EventArgs e)
        {
            SetTitle();

            SetInitialPathOperations();

            //if (!Directory.Exists(_pathToFiles)) { _pathToFiles = @"."; }

            //// Setup archive directories 
            //var newdir1 = Path.Combine(_pathToFiles, "Archive");
            //if (!Directory.Exists(newdir1)) { Directory.CreateDirectory(newdir1); }

            //_inputarchivepath = Path.Combine(newdir1, "InputFiles");
            //if (!Directory.Exists(_inputarchivepath)) { Directory.CreateDirectory(_inputarchivepath); }

            //_outputarchivepath = Path.Combine(newdir1, "OutputFiles");
            //if (!Directory.Exists(_outputarchivepath)) { Directory.CreateDirectory(_outputarchivepath); }

            //var inputdirfullpath = Path.GetFullPath(_pathToFiles);

            //_fullpathfilename = SetInitialFile(inputdirfullpath);

            textBox2.Text = Path.GetFileName(_fullpathfilename);
        }

        // ====================================================================================

        private static string Commafystr(string ss)
        {
            var s1 = ss.Trim();

            if (s1.Contains(",")) return ss;

            var s2 = "";

            for (var i = (s1.Length - 1); i > -1; i--)
            {
                s2 = s1[i] + s2;

                if (i == 0) continue;

                // get inverse count
                var j = s1.Length - i;

                if ((j % 3) == 0) { s2 = "," + s2; }
            }

            return s2;
        }

        // ========================================================================================

        private bool Validate_Lines_All(int p, List<string> list1)
        {
            var answer = true;

            for (var i = 0; i < list1.Count; i++)
            {

                var length = list1[1].Length;

                if (length != p)
                {
                    answer = false;
                    Output("Incorrect line length found of " + length);
                    break;
                }
            }

            return answer;
        }

        // ========================================================================================

        private bool RemoveMultipleHeaderTrailers(ref List<string> list1)
        {

            Output("Checking for MemberID's Starting With C");

            var list2 = new List<string>();
            
            var answer = false;

            var initialheader = list1[0];

            list2.Add(initialheader);

            var totalsubscribers = 0;
            var totalrecords = 0;
            var totaldependents = 0;

            var countedtotalsubscribers = 0;
            var countedtotaldependents = 0;
            var countedtotalrecords = 0;
            var countederrorrecords = 0;

            foreach (var line in list1)
            {
                var input = line.Substring(0, 1);


                if (input == "T")
                {

                    AddCountstoRunningTotal(line, ref totalsubscribers, ref totaldependents, ref totalrecords);
                }
                else
                {
                    if (input != "H")
                    {
                        countedtotalrecords = countedtotalrecords + 1;

                        switch (input)
                        {
                            case "S":
                                countedtotalsubscribers = countedtotalsubscribers + 1;
                                break;
                            case "D":
                                countedtotaldependents = countedtotaldependents + 1;
                                break;
                            default:
                                countederrorrecords = countederrorrecords + 1;
                                break;
                        }

                        //var newline = FixLeadingC(countedtotalrecords,line);


                        //var newline2 = MapSubscriberCatToBenifit(newline);

                        list2.Add(line);


                        //OutputSubscribersInfo(countedtotalrecords, newline);


                    }
                }


            }

          //  Output("Found MemberID's Starting With C (" + _numberofCsfound + ")");


            Addtrailerline(ref list2, totalsubscribers, totaldependents, totalrecords);

            if ((countedtotalsubscribers == totalsubscribers) && (countedtotaldependents == totaldependents) && (countedtotalrecords == totalrecords))
            {

                Output("");
                Output("Total Subscribers = " + Commafystr(countedtotalsubscribers.ToString()));
                Output("Total Dependents  = " + Commafystr(countedtotaldependents.ToString()));
                Output("                   ---------------------");
                Output("                    " + Commafystr((countedtotalsubscribers + countedtotaldependents).ToString()));
                Output("");
         
                answer = true;
            }
            else
            {
                Output("countedtotalsubscribers = " + countedtotalsubscribers);
                Output("countedtotaldependents = " + countedtotaldependents);
                Output("countedtotalrecords = " + countedtotalrecords);
                Output("");
                Output("totalsubscribers = " + totalsubscribers);
                Output("totaldependents = " + totaldependents);
                Output("totalrecords = " + totalrecords);
            }


            if (countederrorrecords > 0) answer = false;



            list1 = list2;

            return answer;
        }

        // ====================================================================================

        private void ReplaceRiderandContract(ref List<string> list1)
        {
            Output("Initial Mappping of Riders and Contracts based on Cat 6 and 7");

            Output("Replacement Mappping of Riders based on Cat 7");

            var cnt = 0;

            var list2 = new List<string>();

            foreach (var line in list1)
            {
                cnt = cnt + 1;

                var input = line.Substring(0, 1);


                if (input == "H")
                {

                    list2.Add(line);
                }
                else if (input == "S")
                {

                    var newline = FixLeadingC(line);

                    var newline2 = MapSubscriberCatToBenifit(newline);

         
                    var line3 = CheckCategoryField7(cnt, newline2);

                    list2.Add(line3);

                  //  OutputSubscribersInfo(cnt, newline);

                }
                else if (input == "D")
                {


                    var newline2 = MapDependentCatToBenifit(line);

                    var line3 = CheckCategoryField7(cnt, newline2);

                    list2.Add(line3);
                }
                else if (input == "T")
                {
                    list2.Add(line);
                }
                else
                {
                    Output("Warning Bad Line Detected starting with " + input + " On line " + cnt);
                }

                //if ((input == "S") || (input == "D"))
                //{
                //    CheckCategoryField7(cnt, line);

                //}



            }


            list1 = list2;


        }

        // ====================================================================================

        private string CheckCategoryField7(int cnt, string line)
        {
            if (cnt < 100)
            {
                var input = line.Substring(0, 1);

                if (input == "S")
                {
                   // Output("--------------------------------------------------------------");
                    //Display("SCategory 7 Descriptor", GetOneBasedSubstring(line, 1291, 1293));
                    //Display("SCategory 7", GetOneBasedSubstring(line, 1294, 1313));


                    var seven = GetOneBasedSubstring(line, 1294, 1313).Trim();
                    if ((seven == "SPO") || (seven == "IND") || (seven == "DEP") || (seven == "CHD") || (seven == "SPC"))
                    {
                        //Display("Subscriber Benefit Rider ID",                GetOneBasedSubstring(line, 533, 542));  // s.Substring(532, 010));


                        var a = GetOneBasedSubstring(line, 001, 532);

                        //var b = GetOneBasedSubstring(line, 533, 542); // Benefit Rider ID

                        const string b = "NR00000001";

                        var c = GetOneBasedSubstring(line, 543, 2000);

                        return a + b + c;

                    }

                    return line;

                }

                if (input == "D")
                {
                    //Display("DCategory 7 Descriptor", GetOneBasedSubstring(line, 1109, 1111));
                    //Display("DCategory 7", GetOneBasedSubstring(line, 1112, 1131));

                    var seven = GetOneBasedSubstring(line, 1112, 1131).Trim();


                    if ((seven == "SPO") || (seven == "IND") || (seven == "DEP") || (seven == "CHD") || (seven == "SPC"))
                    {
                        Output("Dependent Replacement on line " + cnt);
                        var a = GetOneBasedSubstring(line, 001, 532);

                        const string b = "NR00000001";

                        var c = GetOneBasedSubstring(line, 543, 2000);

                        return a + b + c;

                    }


                    return line;

                }

                return line;

            }

            return line;

        }

        // ====================================================================================

        private string GetOneBasedSubstring(string s, int p1, int p2)
        {
            // shift to zero based numbers
            p1 = p1 - 1;
            p2 = p2 - 1;

            var i = p2 - p1 + 1;
            return s.Substring(p1, i);
        }

        // ====================================================================================

        private string FixLeadingC(string line)
        {
            var rtype = GetOneBasedSubstring(line, 1, 1);
            var subscriberid = GetOneBasedSubstring(line, 2, 13);

            if (subscriberid.StartsWith("C"))  
            {
                if (rtype == "S")
                {
                  //  Output("Found Leading C on line " + linenum);
                    var filler = GetOneBasedSubstring(line, 14, 21);
                    var rest = GetOneBasedSubstring(line, 34, 2000);
                    _numberofCsfound = _numberofCsfound + 1;

                    var altid = subscriberid.Substring(1, 11) + " ";

                    return rtype + subscriberid + filler + altid + rest;
                }

                return line;
            }

            return line;
        }

        // ====================================================================================

        private string MapSubscriberCatToBenifit(string line)
        {
            var inputa = line.Substring(0, 1);

            if (inputa == "S")
            {
                var a = GetOneBasedSubstring(line, 001, 532);
               // var b = GetOneBasedSubstring(line, 533, 542);    // Benefit Rider ID
              //  var c = GetOneBasedSubstring(line, 543, 572);    // Benefit Contract ID
                var d = GetOneBasedSubstring(line, 573, 575);    // Employment Status
                var e = GetOneBasedSubstring(line, 576, 1270);
                var f = GetOneBasedSubstring(line, 1271, 1290);  // Category 6
                var g = GetOneBasedSubstring(line, 1291, 2000);

                var newline = PerformRiderBenefitMapping(a, d, e, f, g);

                return newline;
            }
            else
            {
                return line;
            }
        }

        // ====================================================================================

        private string MapDependentCatToBenifit(string line)
        {
            var inputa = line.Substring(0, 1);

            if (inputa == "D")
            {
                var a = GetOneBasedSubstring(line, 001, 372);
            //    var b = GetOneBasedSubstring(line, 373, 382); // Benefit Rider ID
           //     var c = GetOneBasedSubstring(line, 383, 412); // Benefit Contract ID
                var d = GetOneBasedSubstring(line, 413, 415); // Employment Status
                var e = GetOneBasedSubstring(line, 416, 1088);
                var f = GetOneBasedSubstring(line, 1089, 1108); // Category 6
                var g = GetOneBasedSubstring(line, 1109, 2000);

                var newline = PerformRiderBenefitMapping(a,d,e,f,g);

                return newline;
            }
            else
            {
                return line;
            }
        }

        // ====================================================================================

        private string PerformRiderBenefitMapping(string a, string d, string e, string f, string g)
        {
            // a = start
            // b =  Benefit Rider ID
            // c =  Benefit Contract ID
            // d =  Employment Status
            // e = filer
            // f =  Category 6
            // g = tail

            var dorg = d;

            var forg = f;

            d = d.Trim();

            f = f.Trim();

            var b = GetBenefitRider(f,d);

            var c = GetBenefitContract(f, d);

            b = b.PadRight(10, ' ');
            c = c.PadRight(30, ' ');

            return a + b + c + dorg + e + forg + g;
        }

        // ====================================================================================

        private string GetBenefitRider(string cat6, string empstat)
        {
            var comp = cat6 + empstat;

            switch (comp)
            {
                case "VISBASACT":
                    return "FV00000640";

                case "VISBASRET":
                    return "FV00000640";

                case "VISBASLOA":
                    return "FV00000640";

                case "VISBASPT":
                    return "FV00000640";

                case "VISBASCOB":
                    return "FV00000640";


                case "VISEXPACT":
                    return "PR00000735";

                case "VISEXPRET":
                    return "PR00000735";

                case "VISEXPLOA":
                    return "PR00000735";

                case "VISEXPPT":
                    return "PR00000735";

                case "VISEXPCOB":
                    return "PR00000735";

                default:
                    Output(".............................WARNING UNMAPPED BENEFIT RIDER");
                    return "";
            }
        }

        // ====================================================================================

        private string GetBenefitContract(string cat6, string empstat)
        {
            var comp = cat6 + empstat;

            switch (comp)
            {
                case "VISBASACT":
                    return "10P100001216";

                case "VISBASRET":
                    return "10P100001216";

                case "VISBASLOA":
                    return "10P100001216";

                case "VISBASPT":
                    return "10P100001216";

                case "VISBASCOB":
                    return "10P100001217";


                case "VISEXPACT":
                    return "10P100001218";

                case "VISEXPRET":
                    return "10P100001218";

                case "VISEXPLOA":
                    return "10P100001218";

                case "VISEXPPT":
                    return "10P100001218";

                case "VISEXPCOB":
                    return "10P100001219";

                default:
                    Output(".............................WARNING UNMAPPED BENEFIT CONTRACT");
                    return "";
            }
        }

        // ====================================================================================

        private void Addtrailerline(ref List<string> list2, int totalsubscribers, int totaldependents, int totalrecords)
        {
            var s = totalsubscribers.ToString().PadLeft(10, '0');
            var d = totaldependents.ToString().PadLeft(10, '0');
            var t = totalrecords.ToString().PadLeft(10, '0');

            var s1 = "T" + t + s + d + Blank(1970);

            list2.Add(s1);
        }

        // ====================================================================================

        private string Blank(int p)
        {
            var s1 = "";
            for (var i = 1; i < p; i++)
            {
                s1 = s1 + " ";
            }
            return s1;
        }

        // ====================================================================================

        private void AddCountstoRunningTotal(string line, ref int totalsubscribers, ref int totaldependents, ref int totalrecords)
        {
            totalrecords = totalrecords + Convert.ToInt32(line.Substring(1, 10));
            totalsubscribers = totalsubscribers + Convert.ToInt32(line.Substring(11, 10));
            totaldependents = totaldependents + Convert.ToInt32(line.Substring(21, 10));
        }

        // ========================================================================================

        private static string GenerateOutputFileName(string p, string d)
        {
            var a = Path.GetDirectoryName(p);
            var b = Path.GetFileNameWithoutExtension(p);
            var c = "_Converted_";
            // var d = DateTime.Now.ToString("yyyy-mm-dd_HH-mm-ss"); // 24Hour
            var e = Path.GetExtension(p);

            if (e != null && e.ToUpper() != ".TXT") { e = ".txt"; }

            if (a != null)
            {
                return Path.Combine(a, b + c + d + e);
            }

            return "";
        }

        // ========================================================================================

        private void Archive_source(string d, DateTime dt, string fullpathfilename)
        {

            var infilenameonly = Path.GetFileName(fullpathfilename);
            infilenameonly = infilenameonly + "_InputFile_" + d + ".txt";



            var archivefilefullpath1 = Path.Combine(_pathToArchiveFiles, "InputFiles");

            archivefilefullpath1 = Path.Combine(archivefilefullpath1, infilenameonly);


            File.Move(fullpathfilename, archivefilefullpath1);
            File.SetLastWriteTime(archivefilefullpath1, dt);

        }


        // ========================================================================================

        private void Archive_source1(string d, DateTime dt, string fullpathfilename)
        {

            var infilenameonly = "CVXin_10P11216_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";

            var archivefilefullpath1 = Path.Combine(_pathToOutputFiles, infilenameonly);

            File.Copy(fullpathfilename, archivefilefullpath1);
            File.SetLastWriteTime(archivefilefullpath1, dt);



        }

        // ========================================================================================

        private void Archive_source2(string d, DateTime dt, string fullpathfilename)
        {


            var infilenameonly = Path.GetFileName(fullpathfilename);
            infilenameonly = infilenameonly + "_DV2000File_" + ".txt";


            var archivefilefullpath1 = Path.Combine(_pathToArchiveFiles, "OutputFiles");

            archivefilefullpath1 = Path.Combine(archivefilefullpath1, infilenameonly);

            File.Move(fullpathfilename, archivefilefullpath1);
            File.SetLastWriteTime(archivefilefullpath1, dt);

        }


        // ========================================================================================

        private void PerformFileManipulations(string d, string infilenameonly, string outputFilename)
        {
            var dt1 = DateTime.Now;


            Archive_source(d, dt1, infilenameonly);

            Archive_source1(d, dt1, outputFilename);

            Archive_source2(d, dt1, outputFilename);


            //CVXin_10P11216

            //var archivefilefullpath1 = Path.Combine(_inputarchivepath, infilenameonly);
            //File.Move(_fullpathfilename, archivefilefullpath1);
            //File.SetLastWriteTime(archivefilefullpath1, dt1);



            //        var outfilenameonly = Path.GetFileName(outputFilename);
            //        var archivefilefullpath2 = Path.Combine(_outputarchivepath, outfilenameonly);
            //        File.Copy(outputFilename, archivefilefullpath2);
            //        File.SetLastWriteTime(archivefilefullpath2, dt1);



            //// CVXin_10P11216_20171124_090309

            //infilenameonly = Path.GetFileName(infilenameonly);

            //// var infilenameonly = Path.GetFileName(_fullpathfilename);
            //infilenameonly = infilenameonly + "_Converted_" + d + ".txt";

            //string ufilenameonly;

            //if (_etype == InputfileActiveorRetiree.Active)
            //{
            //    ufilenameonly = "CVXin_" + "10P11220" + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
            //}
            //else
            //{
            //    ufilenameonly = "CVXin_" + "10P11221" + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
            //}


            //var archivefilefullpath1 = Path.Combine(_inputarchivepath, infilenameonly);
            //File.Move(_fullpathfilename, archivefilefullpath1);
            //File.SetLastWriteTime(archivefilefullpath1, dt1);

            ////var outfilenameonly = Path.GetFileName(outputFilename);
            ////if (ufilenameonly != null)
            //{
            //    var archivefilefullpath2 = Path.Combine(_outputarchivepath, ufilenameonly);
            //    File.Copy(outputFilename, archivefilefullpath2);
            //    File.SetLastWriteTime(archivefilefullpath2, dt1);
            //}

            //var filenameonly = Path.GetFileName(ufilenameonly);
            //// if (filenameonly != null)
            //{
            //    var finaldestinationname = Path.Combine(_pathToOutputFiles, filenameonly);
            //    File.Move(outputFilename, finaldestinationname);
            //    File.SetLastWriteTime(finaldestinationname, dt1);
            //}


            //// Now Refresh For processing the next file
            textBox2.Clear();
            _fullpathfilename = SetInitialFile(_pathToInputFiles);
            textBox2.Text = Path.GetFileName(_fullpathfilename);

            Refresh();
        }



        // ====================================================================================



        private void ProcessSomething()
        {
            var list1 = File.ReadAllLines(_fullpathfilename).ToList();

            Output("Total Line Count = " + Commafystr(list1.Count.ToString()));

            const int correctlinelength = 2000;

            if (Validate_Lines_All(correctlinelength, list1))
            {
                Output("All Lines are of the corrrect length of " + correctlinelength);

                //   ProcessHeader(list1[0]);

                //   Process_Trailer(list1[list1.Count - 1]);

                //    CheckCounts(list1);


                Output("Combining headers and trailers");
                var result1 = RemoveMultipleHeaderTrailers(ref list1);

                if (result1)
                {
                    ReplaceRiderandContract(ref list1);
                }


                if (result1)
                {
                    // Everything okay generate output file

                    //   var p1 = Path.Combine(_pathToOutputFiles, "CVXin_10P11216HM_20171107_071539");

                    var d = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"); // 24Hour

                    var outputFilename = GenerateOutputFileName(_fullpathfilename, d);
                    // var outputFilename = Path.GetFullPath(p1);

                    File.WriteAllLines(outputFilename, list1.ToArray());


                    Output("Done Converting File");

                    var d2 = DateTime.Now.ToString("yyyy-mm-dd_HH-mm-ss"); // 24Hour


                   // var dt1 = DateTime.Now;

                    PerformFileManipulations(d2, _fullpathfilename, outputFilename);


                }
                else
                {
                    Output("Warning Counts did not match Totals in incomming Trailer Records");
                    Output("Conversion aborted");
                }
            }
            else
            {
                Output("All Lines are of the INCORRECT length");
            }
        }
        // ========================================================================================
        //

        private static string Getuser()
        {
            // System.Security.Principal.WindowsIdentity.GetCurrent().Name  FQDN
            return Environment.UserName;
        }


        // ========================================================================================
        //

        private static string Getmachine()
        {
            // System.Environment.MachineName from a console or WinForms app.
            // HttpContext.Current.Server.MachineName from a web app
            // System.Net.Dns.GetHostName() to get the FQDN


            return Environment.MachineName;
        }
        // ====================================================================================

        private void Form1_Activated(object sender, EventArgs e)
        {
            if (!(_ranonce))
            {
                Output("Form1_Activated");


                Output("Run as: " + Getuser());

                Output("Run on: " + Getmachine());


                Output("InputFiles   = " + _pathToInputFiles);
                Output("ArchiveFiles = " + _pathToArchiveFiles);
                Output("OutputFiles  = " + _pathToOutputFiles);


                _ranonce = true;

                var args = Environment.GetCommandLineArgs();

                if (args.Length > 1)
                {
                    var arg1 = args[1].ToUpper();

                    if (arg1 == "AUTO")
                    {
                        Output(DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " Auto Run");

                        // this will block till complete
                        button1_Click(new object(), new EventArgs());
                    }
                    else
                    {
                        Output(DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " Manual Run");
                        //  do nothing incorrect command line arguments
                    }

                    // Close the windows form
                    BeginInvoke(new MethodInvoker(Close));

                }
            }
        }

        // ====================================================================================


        private void button1_Click(object sender, EventArgs e)
        {
            // if auto


            var sTest = new Stopwatch();
            sTest.Reset();
            sTest.Start();

            Output(Dash(70));

            if (_fullpathfilename != "")
            {
                ProcessSomething();
            }
            else
            {
                Output("No File Selected For Processing");
            }

            sTest.Stop();
            Output("");
            Output("Done in " + DisplayTime6(sTest));

            // if file processed
            // email log file

            // clean old log files

            //  Output("Done");
        }


        // ========================================================================================

        private string AddTrailingBackSlash(string dir)
        {
            if (dir.EndsWith(@"\"))
            {
                return dir;
            }
            else
            {
                return dir + @"\";
            }
        }

        // ========================================================================================

        private void button2_Click(object sender, EventArgs e)
        {
            var openFileDialog1 = new OpenFileDialog();

            _pathToFiles = Path.GetFullPath(_pathToFiles);

            // Set default directory
            //openFileDialog1.RestoreDirectory = true;
            openFileDialog1.InitialDirectory = AddTrailingBackSlash(Path.Combine(_pathToFiles));
            openFileDialog1.Title = @"Select Incomming State of Tennessee Enrollment File To Process";
            openFileDialog1.Filter = @"All files (*.*)|*.*|Text files (*.txt)|*.txt|Enrollment files (*.enr)|*.enr";

            var result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                var filename = openFileDialog1.FileName;
                textBox1.Text = filename;
            }
        }

        // ====================================================================================


    }
}
