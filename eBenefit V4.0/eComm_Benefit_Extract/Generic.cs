﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Reflection;
using System.Threading;

namespace eComm_Benefit_Extract
{

    public partial class Form1
    {
        // ====================================================================================

        private void ClearOldSqlDump()
        {
            var path1 = Path.Combine(_relativebasepath, DateTime.Now.Year.ToString());


            for (var i = 1; i <= 3; i++)
            {
                var p1 = Path.Combine(path1, "AllSQL" + i + ".txt");

                if (File.Exists(p1))
                {
                    File.Delete(p1);
                } 
            }
        }

        // ========================================================================================

        private void Addheader2(List<string> listofpipeddata, List<string> columnnames)
        {
            // Adds a one based column count
            var h1 = "";
            for (var i = 0; i < columnnames.Count - 1; i++)
            {
                h1 = h1 + (i + 1) + "|";
            }
            h1 = h1 + (columnnames.Count);

            listofpipeddata.Insert(0, h1);
        }


        // ========================================================================================

        private void Addheader1(List<string> listofpipeddata, List<string> columnnames)
        {
            // Adds a row of column names
            var h1 = "";
            for (var i = 0; i < columnnames.Count - 1; i++)
            {
                h1 = h1 + columnnames[i] + "|";
            }
            h1 = h1 + columnnames[columnnames.Count - 1];

            listofpipeddata.Insert(0, h1);
        }

        // ========================================================================================

        private static void SetupLog_and_BasePath()
        {
            if (!Directory.Exists(_relativebasepath))
            {
                _relativebasepath = @".\\";
            }

            var newdir = Path.Combine(_relativebasepath, DateTime.Now.Year.ToString());
            if (!Directory.Exists(newdir))
            {
                Directory.CreateDirectory(newdir);
            }

            var logpath = Path.Combine(newdir, DateTime.Now.ToString("yyyy-MM-dd") + "_ALog.txt");
            _logpath = Path.GetFullPath(logpath);
        }

        // ========================================================================================

        private static void OutputD(string s1)
        {
            try
            {
                using (var writer = File.AppendText(_logpath))
                {
                    writer.WriteLine(DateTime.Now.ToString("[HH-mm-ss] ") + s1);
                }
            }
            catch (Exception)
            {
                //
            }
        }

        // ========================================================================================
        //

        private string Getuser2()
        {
            // Gives NT AUTHORITY\SYSTEM
            return System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            // HVHCVISION\fbishop
        }

        // ========================================================================================
        //

        private string Getmachine()
        {
            // System.Environment.MachineName from a console or WinForms app.
            // HttpContext.Current.Server.MachineName from a web app
            // System.Net.Dns.GetHostName() to get the FQDN

            return Environment.MachineName;
        }

        // ========================================================================================

        private void DisplayVersion(string svr)
        {
            const string baseFormTitle = "Generate Benefits Extract For eCommerce V";

            var major = Assembly.GetExecutingAssembly().GetName().Version.Major.ToString();

            var minor = Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString();

            var build = Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();

            Output(baseFormTitle + major + @"." + minor + @" (Build " + build + @")");

            Text = baseFormTitle + major + @"." + minor + @" (Build " + build + @")";


            Output("");
            Output("Will Be Run on Machine : " + Getmachine());
            Output("Will Be Run against .. : " + svr);
            Output("Will Be Run on Date .. : " + DateTime.Now.ToString("yyyy-MM-dd"));
            Output("Will Be Run at Time .. : " + DateTime.Now.ToString("HH-mm-ss"));
            Output("Will Be Run By ....... : " + Getuser2());
            Output("");
        }

        // ========================================================================================

        private void conn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            Output("|conn_InfoMessage = " + e.Message);
        }

        // ========================================================================================

        private string GetUserPasswordString1(string sn, string dn, string us, string ps)
        {
            const string extra = "tcp:"; // "tcp:"      "np:"

            var s1 = "Server=" + extra + sn.Trim() + ";";
            var s2 = "Database=" + dn.Trim() + ";";

            var s3 = "UID=" + us.Trim() + ";";
            var s4 = "PWD=" + ps.Trim() + ";";

            const string s5 = "Pooling=True;";
            const string s6 = "Min Pool Size=5;";
            const string s7 = "Connection Timeout=5;";
            const string s8 = "Application Name=TestConnection;";

            return s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8;
        }

        // ========================================================================================

        private string GetTrustedString1(string serverName, string databaseName)
        {
            var s1 = "Server=" + serverName.Trim() + ";";
            var s2 = "Database=" + databaseName.Trim() + ";";
            const string s3 = "Integrated Security=SSPI;";

            const string s4 = "Pooling=True;";
            const string s5 = "Min Pool Size=5;";
            const string s6 = "Connection Timeout=5;";
            const string s7 = "Application Name=GetQueryInfo;";

            return s1 + s2 + s3 + s4 + s5 + s6 + s7;
        }

        // ========================================================================================

        private SqlConnection GetConnectionGeneric(string domain, string server, string database, string username,
            string password)
        {
            string connectionString;

            if (domain != "")
            {
                server = server + "." + domain;
            }

            if (username == "")
            {
                connectionString = GetTrustedString1(server, database);
            }
            else
            {
                connectionString = GetUserPasswordString1(server, database, username, password);
            }

            try
            {
                var con = new SqlConnection(connectionString);

                return con;
            }
            catch (Exception ex2)
            {
                Output(@"Connection Exception: " + ex2.Message);

                return null;
            }
        }

        // ========================================================================================

        private SqlConnection GetOpenConnection(string domain, string server, string database, string username,
            string password)
        {
            var con = GetConnectionGeneric(domain, server, database, username, password);

            try
            {
                con.Open();
            }
            catch (Exception ex2)
            {
                Output(@"Connection Open Exception: " + ex2.Message);
            }

            Thread.Sleep(1000);

            return con;
        }

        // ====================================================================================
        //

        private string DisplayTime6(Stopwatch sTime)
        {
            var frequency = Stopwatch.Frequency;
            var nanosecPerTick = (1000L*1000L*1000L)/frequency;
            var absoluteNanoseconds = sTime.ElapsedTicks*nanosecPerTick;
            absoluteNanoseconds = absoluteNanoseconds/1000;
            absoluteNanoseconds = absoluteNanoseconds/1000;
            var msec = (int) (absoluteNanoseconds%1000);
            absoluteNanoseconds = absoluteNanoseconds/1000;
            var sec = (int) (absoluteNanoseconds);

            var mins = 0;
            var hrs = 0;
            var day = 0;
            var newSec = sec;

            if (newSec >= 60) { mins = (newSec/60); newSec = newSec%60; }
            var newMin = mins;
            if (newMin >= 60) { hrs = (newMin/60); newMin = newMin%60; }
            var newHr = hrs;
            if (newHr >= 24) { day = (newHr/24); newHr = newHr%24; }
            var newDay = day;

            if (newDay > 0) { return "[Days = " + newDay + "] " + "[Hours = " + newHr + "]"; }

            if (newHr > 0)
            {
                return "[Hours = " + newHr + "] " + "[Minutes = " + newMin + "]";
            }

            if (newMin > 0)
            {
                return "[Minutes = " + newMin + "] " + "[Seconds = " + newSec + "]";
            }

            return "[Seconds = " + newSec + "] " + "[MilliSeconds = " + msec + "]";
        }

        // ========================================================================================

        private void RunNonQuery(SqlConnection con, bool b, string s2, string s1)
        {
            if (b) { Output(s2);} 

            using (var cmd3 = con.CreateCommand())
            {
                //The time in seconds to wait for the command to execute. The default is 30 seconds.
                cmd3.CommandTimeout = 15*60;

                cmd3.CommandText = s1;
                cmd3.ExecuteNonQuery();
            }
        }

        // ========================================================================================

        private List<string> GetTopRowForListOfColumns(SqlConnection con2,string tablename)
        {
            var columns = new List<string>();

            using (var cmd1 = con2.CreateCommand())
            {
                //The time in seconds to wait for the command to execute. The default is 30 seconds.
                cmd1.CommandTimeout = 15*60;

                cmd1.CommandText = "SELECT TOP 1 * FROM " + tablename;

                try
                {
                    // Blocking command
                    using (var reader = cmd1.ExecuteReader())
                    {
                        // Cast to get HasRows functionality
                        if (reader.HasRows)
                        {
                            // Loop through all the rows in the DataTableReader 
                            while (reader.Read())
                            {
                                // list1.Add(reader[0].ToString());

                                // Loop though all the fields in each row
                                for (var i = 0; i < reader.FieldCount; i++)
                                {
                                    columns.Add(reader.GetName(i));
                                }
                            }
                        }
                        else
                        {
                            // list1.Add("Empty ReturnSet");
                        }
                    } // using
                }
                catch (Exception ex1)
                {
                    Output("Exception: " + ex1);
                    Output("Exception: " + ex1.Message);
                }
            }

            return columns;
        }


        // ========================================================================================

        private DataTable Get_TableofDataFromDatabase(ref SqlConnection con, string s1)
        {

            var test = new Stopwatch();
            test.Reset();
            test.Start();

            // Define and allocate a Block of data for the result
            var dt = new DataTable();

            try
            {
                using (var cmd1 = con.CreateCommand())
                {
                    cmd1.CommandText = s1;
                    cmd1.CommandTimeout = 600;

                    try
                    {
                        // this is a blocking command loading the result set directly into a datatable
                        dt.Load(cmd1.ExecuteReader(), LoadOption.OverwriteChanges);
                        dt.TableName = "Table1";
                    }
                    catch (Exception ex1)
                    {
                        Output("Exception: " + ex1);
                        Output("Exception: " + ex1.Message);
                    }
                }
            }
            catch (Exception ex1)
            {
                Output("Exception: " + ex1);
                Output("Exception: " + ex1.Message);
            }

            test.Stop();
            OutputD("Get_TableofDataFromDatabase" + " in Elapsed Time: " + DisplayTime6(test));

            return dt;
        }

        // ========================================================================================

        private string Dash(int p)
        {
            var s = "";
            for (var i = 0; i < p; i++)
            {
                s = s + "-";
            }

            return s;
        }

        // ========================================================================================

        private static string SqlFormat(string sql)
        {
            sql = PrettySqlBefore("CREATE TABLE #MonsterTempTable1 (", sql);

            sql = PrettySqlBefore("INSERT INTO #MonsterTempTable1 (", sql);

            sql = PrettySql("UPDATE T2 SET", sql);

            sql = PrettySql("FROM", sql);
            sql = PrettySql("JOIN", sql);
            sql = PrettySql("AND", sql);
            //  sql = PrettySql("NOT LIKE", sql);
            sql = PrettySql("WHERE", sql);

            sql = PrettySql(", [", sql);
            sql = PrettySql(", T1.[", sql);

            sql = PrettySql(", T2.[", sql);

            sql = PrettySql("COALESCE(CONVERT(VARCHAR", sql);

            sql = PrettySql(", gc", sql);

            sql = PrettySql(", mcs", sql);
            return sql;
        }




        // ========================================================================================

        private void Dumpsql(string digit, string name, string sql)
        {
            var path1 = Path.Combine(_relativebasepath, DateTime.Now.Year.ToString());

            // var p1 = Path.Combine(path1, name);

            var p1 = Path.Combine(path1, "AllSQL" + digit + ".txt");

            sql = SqlFormat(sql);

            var addedbit = "--" + name;

            File.AppendAllText(p1, Environment.NewLine);
            File.AppendAllText(p1, addedbit);
            File.AppendAllText(p1, Environment.NewLine);
            File.AppendAllText(p1, sql.Trim()); //WriteAllText(p1, sql);
            File.AppendAllText(p1, Environment.NewLine);

            Refresh();

        }
        // ========================================================================================

        private void DumpsqlLine(string digit, string s1)
        {
            var path1 = Path.Combine(_relativebasepath, DateTime.Now.Year.ToString());

            var p1 = Path.Combine(path1, "AllSQL" + digit + ".txt");

            File.AppendAllText(p1, Dash(120) + Environment.NewLine);
            File.AppendAllText(p1, Dash(120) + Environment.NewLine);
            if (s1 != "")
            {
                File.AppendAllText(p1, @"-- " + s1);
            }
        }

        // ========================================================================================

        private void DumpSqlComment(string digit, string s11)
        {
            var path1 = Path.Combine(_relativebasepath, DateTime.Now.Year.ToString());
            var p1 = Path.Combine(path1, "AllSQL" + digit + ".txt");

            File.AppendAllText(p1, Dash(120) + Environment.NewLine);
            File.AppendAllText(p1, @"-- " + s11);
            File.AppendAllText(p1, Dash(120) + Environment.NewLine);
        }

        // ========================================================================================

        //private static void Dumpsql2(string digit, string name, string sql)
        //{
        //    var path1 = Path.Combine(_relativebasepath, DateTime.Now.Year.ToString());

        //    // var p1 = Path.Combine(path1, name);

        //    var p1 = Path.Combine(path1, "AllSQL" + digit + ".txt");

        //    sql = SqlFormat(sql);

        //    var addedbit = "--" + name;

        //    File.AppendAllText(p1, Environment.NewLine);
        //    File.AppendAllText(p1, addedbit);
        //    File.AppendAllText(p1, Environment.NewLine);

        //    var lines1 = sql.Split(new[] { Environment.NewLine },StringSplitOptions.None);

        //    //This will correctly split on either type of line break, and preserve empty lines and spacing in the text

        //    //var lines2 = sql.Split(new[] { "\r\n", "\r", "\n" },StringSplitOptions.None);

        //    foreach (var line in lines1)
        //    {
        //        File.AppendAllText(p1, @"--" + line);          
        //    }


        //    File.AppendAllText(p1, sql.Trim()); //WriteAllText(p1, sql);
        //    File.AppendAllText(p1, Environment.NewLine);

        //}

        // ========================================================================================
        //

        private static string PrettySql(string key,string sql)
        {
            var s1 = sql.Split(new[] { key }, StringSplitOptions.None);
            var s2 = "";
            for (var i = 0; i < s1.Length; i++)
            {
                if (i < (s1.Length - 1))
                {
                    s2 = s2 + s1[i] + Environment.NewLine + key;
                }
                else
                {
                    if (s1[i] != "") { s2 = s2 + s1[i] + Environment.NewLine; } 
                }
            }

            return s2;
        }

        // ========================================================================================
        //

        private static string PrettySqlBefore(string key, string sql)
        {
            var s1 = sql.Split(new[] { key }, StringSplitOptions.None);
            var s2 = "";
            for (var i = 0; i < s1.Length; i++)
            {
                if (i < (s1.Length - 1))
                {
                    s2 = s2 + Environment.NewLine + s1[i] + key;
                }
                else
                {
                    if (s1[i] != "") { s2 = s2 + Environment.NewLine + s1[i]; }
                }
            }

            return s2;
        }

        // ========================================================================================
        // Used to ensure no additional delimeter data is in the data sections
        // or   s = s.Replace("\"", "");

        private string Rmd(string p)
        {
            var s1 = p.Replace("|", "");
            var s2 = s1.Replace("'", "");
            var s3 = s2.Replace("\t", "");
            var s4 = s3.Replace(@"""", "");
            return s4;
        }

        // ========================================================================================

        private string GetTableDataAsPiped(DataTable tbl1, int j)
        {
            var s1 = "";
            for (var i = 0; i < tbl1.Columns.Count; i++)
            {
                if (i == 0)
                {
                    if (tbl1.Rows[j][i] is DateTime)
                    {
                        var a = tbl1.Rows[j][i].ToString();
                        var k = a.IndexOf(" ", StringComparison.Ordinal);
                        var b = a.Substring(0, k);
                        s1 = s1 + b;
                    }
                    else
                    {
                        s1 = s1 + Rmd(tbl1.Rows[j][i].ToString());
                    }
                }
                else
                {
                    if (tbl1.Rows[j][i] is DateTime)
                    {
                        var a = tbl1.Rows[j][i].ToString();
                        var k = a.IndexOf(" ", StringComparison.Ordinal);
                        var b = a.Substring(0, k);
                        s1 = s1 + "|" + b;
                    }
                    else
                    {
                        s1 = s1 + "|" + Rmd(tbl1.Rows[j][i].ToString());
                    }
                }
            }
            return s1;
        }

        // ========================================================================================

        private List<string> TableToPipedStrings(DataTable tbl1)
        {
            var test = new Stopwatch();
            test.Reset();
            test.Start();

            var list1 = new List<string>();

            if (tbl1.Rows.Count > 0)
            {
                for (var j = 0; j < tbl1.Rows.Count; j++)
                {
                    var rowline = GetTableDataAsPiped(tbl1, j);
                    list1.Add(rowline);
                }
            }
            else
            {
                Output("The Result Is Empty");
            }

            test.Stop();
            OutputD("TableToPipedStrings" + " in Elapsed Time: " + DisplayTime6(test));

            return list1;
        }

        // ========================================================================================

        // needs to work when removing column 1

        private string remove_onebased_column(int p, string line)
        {
            var newline = "";

            if (p > 0)
            {
                p = p - 1;
            }

            if (line != null)
            {
                var pieces = line.Split('|');

                for (var i = 0; i < pieces.Length; i++)
                {
                    if (i == 0)
                    {
                        if (i == p)
                        {

                            newline = "";
                        }
                        else
                        {
                            newline = newline + pieces[i];
                        }
                    }
                    else
                    {
                        if (i != p)
                        {
                            newline = newline + "|" + pieces[i];
                        }
                    }
                }

                // doesnt fix issue with removing column 1
                //if (p == 1)
                //{
                //    if (newline.StartsWith("|"))
                //    {
                //        newline = newline.Remove(0);
                //    }
                //}

            }
            return newline;
        }

        // ===================================================================================
        // Run Once for Each Page in the Print Job  Changing the Printsettings like Paper Source and Page Orientation

        private void pd_QueryPageSettings(object sender, QueryPageSettingsEventArgs e)
        {
            var ps = new PaperSize();
            //ps.RawKind = (int)PaperKind.Tabloid;
            //ps.RawKind = (int)PaperKind.Legal;
            ps.RawKind = (int)PaperKind.Letter;
            e.PageSettings.PaperSize = ps;
            e.PageSettings.Landscape = false;
            e.PageSettings.PrinterSettings.DefaultPageSettings.PrinterResolution.Kind = PrinterResolutionKind.High;
        }

        // ========================================================================================

        private void PrintDocumentOnPrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawString(textBox1.Text, textBox1.Font, Brushes.Black, 10, 25);
        }

        // ========================================================================================

        private void PrintDocument()
        {
            var printDocument = new PrintDocument();
            printDocument.PrintPage += PrintDocumentOnPrintPage;
            printDocument.QueryPageSettings += pd_QueryPageSettings;

            printDocument.Print();
        }

        // ========================================================================================

    }
}
  

