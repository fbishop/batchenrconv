﻿namespace eComm_Benefit_Extract
{
    public partial class Form1
    {

        // ========================================================================================

        private string GetSqlForEligibility_Counts()
        {
            // note numbers may be low because of the no benefit contracts being filtered out

            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[eligibility_cnt] = [cnt] " +
                              "FROM " +
                              "( " +
                              "SELECT Count(*) AS cnt,  e.[employergroup_id], " +
                              "e.[group_contract_id], emcsx.member_category_set_id " +
                               "FROM [eligibility] e  " +
                              "JOIN [eligibility_member_category_set_xref] emcsx ON emcsx.[eligibility_id] = e.[eligibility_id] " +
                              "AND emcsx.[eff_date] < GetDate() " +
                              "GROUP BY e.[employergroup_id] ,e.[group_contract_id], emcsx.member_category_set_id " +
                              ") A1 " +
                              "WHERE A1.[employergroup_id] = [#MonsterTempTable1].[employergroup_id] " +
                              "AND A1.[group_contract_id] = [#MonsterTempTable1].[group_contract_id] " +
                              "AND A1.[member_category_set_id] = [#MonsterTempTable1].[member_category_set_id] ";

            Dumpsql("2", "GetEligibility Counts", s1);

            return s1;
        }

        // ========================================================================================


        private string GetSqlForActiveEligibility_Counts()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[active_eligibility_cnt] = [cnt] " +
                              "FROM " +
                              "( " +
                              "SELECT Count(*) AS cnt,  e.[employergroup_id], e.[group_contract_id], emcsx.member_category_set_id " +
                              "FROM [eligibility] e  " +
                              "JOIN [eligibility_member_category_set_xref] emcsx ON emcsx.[eligibility_id] = e.[eligibility_id] " +
                              "AND emcsx.[eff_date] < GetDate() " +
                              "WHERE e.[eligibility_ud] NOT LIKE '$PPL%' " +
                              "AND e.eff_date <= GetDate() " +
                              "AND ((e.term_date >= GetDate()) OR (e.term_date IS NULL)) " +
                              "GROUP BY e.[employergroup_id] ,e.[group_contract_id], emcsx.member_category_set_id " +
                              ") A1 " +
                              "WHERE A1.[employergroup_id] = [#MonsterTempTable1].[employergroup_id] " +
                              "AND A1.[group_contract_id] = [#MonsterTempTable1].[group_contract_id] " +
                              "AND A1.[member_category_set_id] = [#MonsterTempTable1].[member_category_set_id] ";

            Dumpsql("2", "Get Active Eligibility Counts", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForFutureEligibility_Counts()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[future_eligibility_cnt] = [cnt] " +
                              "FROM " +
                              "( " +
                              "SELECT Count(*) AS cnt,  e.[employergroup_id], e.[group_contract_id], emcsx.member_category_set_id " +
                              "FROM [eligibility] e  " +
                              "JOIN [eligibility_member_category_set_xref] emcsx ON emcsx.[eligibility_id] = e.[eligibility_id] " +
                              "AND emcsx.[eff_date] < GetDate() " +
                              "AND ((e.[eligibility_ud] NOT LIKE '$PPL%' AND e.term_date IS NULL AND e.eff_date > GetDate()) " +
                              "OR " +
                              "(e.[eligibility_ud] NOT LIKE '$PPL%' AND e.term_date IS NOT NULL AND e.eff_date > GetDate() AND e.eff_date >= e.term_date)) " +
                              "GROUP BY e.[employergroup_id] ,e.[group_contract_id], emcsx.member_category_set_id " +
                              ") A1 " +
                              "WHERE A1.[employergroup_id] = [#MonsterTempTable1].[employergroup_id] " +
                              "AND A1.[group_contract_id] = [#MonsterTempTable1].[group_contract_id] " +
                              "AND A1.[member_category_set_id] = [#MonsterTempTable1].[member_category_set_id] ";

            Dumpsql("2", "Get Future Eligibility Counts", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForTermEligibility_Counts()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[term_eligibility_cnt] = [cnt] " +
                              "FROM " +
                              "( " +
                              "SELECT Count(*) AS cnt,  e.[employergroup_id], e.[group_contract_id], emcsx.member_category_set_id " +
                              "FROM [eligibility] e  " +
                              "JOIN [eligibility_member_category_set_xref] emcsx ON emcsx.[eligibility_id] = e.[eligibility_id] " +
                              "AND emcsx.[eff_date] < GetDate() " +
                               "AND e.[eligibility_ud] NOT LIKE '$PPL%' AND " +
                              "((e.term_date IS NOT NULL AND e.eff_date <= GetDate() AND e.term_date < GetDate()) " +
                              "OR " +
                              "(e.term_date IS NOT NULL AND e.eff_date > GetDate() AND e.eff_date < e.term_date)) " +
                              "GROUP BY e.[employergroup_id] ,e.[group_contract_id], emcsx.member_category_set_id " +
                              ") A1 " +
                              "WHERE A1.[employergroup_id] = [#MonsterTempTable1].[employergroup_id] " +
                              "AND A1.[group_contract_id] = [#MonsterTempTable1].[group_contract_id] " +
                              "AND A1.[member_category_set_id] = [#MonsterTempTable1].[member_category_set_id] ";

            Dumpsql("2", "Get Term Eligibility Counts", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForPPLEligibility_Counts()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[ppl_eligibility_cnt] = [cnt] " +
                              "FROM " +
                              "( " +
                              "SELECT Count(*) AS cnt,  e.[employergroup_id], e.[group_contract_id], emcsx.member_category_set_id " +
                              "FROM [eligibility] e  " +
                              "JOIN [eligibility_member_category_set_xref] emcsx ON emcsx.[eligibility_id] = e.[eligibility_id] " +
                              "AND emcsx.[eff_date] < GetDate() " +
                              "AND e.[eligibility_ud] LIKE '$PPL%' " +
                              "GROUP BY e.[employergroup_id] ,e.[group_contract_id], emcsx.member_category_set_id " +
                              ") A1 " +
                              "WHERE A1.[employergroup_id] = [#MonsterTempTable1].[employergroup_id] " +
                              "AND A1.[group_contract_id] = [#MonsterTempTable1].[group_contract_id] " +
                              "AND A1.[member_category_set_id] = [#MonsterTempTable1].[member_category_set_id] ";

            Dumpsql("2", "Get PPL Eligibility Counts", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForPplFirstEligibilityId()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[ppl_eligibility_id] = [eligibility_id] " +
                              "FROM " +
                              "( " +
                              "SELECT e.[eligibility_id],  e.[employergroup_id], e.[group_contract_id], emcsx.member_category_set_id " +
                              "FROM [eligibility] e  " +
                              "JOIN [eligibility_member_category_set_xref] emcsx ON emcsx.[eligibility_id] = e.[eligibility_id] " +
                              "AND emcsx.[eff_date] < GetDate() " +
                              "AND e.[eligibility_ud] LIKE '$PPL%' " +
                              "GROUP BY e.[eligibility_id], e.[employergroup_id] ,e.[group_contract_id], emcsx.member_category_set_id " +
                              ") A1 " +
                              "WHERE A1.[employergroup_id] = [#MonsterTempTable1].[employergroup_id] " +
                              "AND A1.[group_contract_id] = [#MonsterTempTable1].[group_contract_id] " +
                              "AND A1.[member_category_set_id] = [#MonsterTempTable1].[member_category_set_id] ";

            Dumpsql("2", "Get First PPL Eligibility Id", s1);

            return s1;
        }


        // ========================================================================================

        private string GetSqlForPplFirstEligibilityUd()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[ppl_eligibility_ud] = [eligibility_ud] " +
                              "FROM " +
                              "( " +
                              "SELECT e.[eligibility_ud],  e.[employergroup_id], e.[group_contract_id], emcsx.member_category_set_id " +
                              "FROM [eligibility] e  " +
                              "JOIN [eligibility_member_category_set_xref] emcsx ON emcsx.[eligibility_id] = e.[eligibility_id] " +
                              "AND emcsx.[eff_date] < GetDate() " +
                              "AND e.[eligibility_ud] LIKE '$PPL%' " +
                              "GROUP BY e.[eligibility_ud],e.[employergroup_id] ,e.[group_contract_id], emcsx.member_category_set_id " +
                              ") A1 " +
                              "WHERE A1.[employergroup_id] = [#MonsterTempTable1].[employergroup_id] " +
                              "AND A1.[group_contract_id] = [#MonsterTempTable1].[group_contract_id] " +
                              "AND A1.[member_category_set_id] = [#MonsterTempTable1].[member_category_set_id] ";

            Dumpsql("2", "Get First PPL Eligibility Id", s1);

            return s1;
        }


        // ========================================================================================

        private string GetSqlForCleanEligibility_Counts()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[eligibility_cnt] = " +
                              "COALESCE([eligibility_cnt],'0') ";

            Dumpsql("2", "Get Clean Eligibility Counts", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForCleanActiveEligibility_Counts()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[active_eligibility_cnt] = " +
                              "COALESCE([active_eligibility_cnt],'0') ";

            Dumpsql("2", "Get Clean Active Eligibility Counts", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForCleanFutureEligibility_Counts()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[future_eligibility_cnt] = " +
                              "COALESCE([future_eligibility_cnt],'0') ";

            Dumpsql("2", "Get Clean Future Eligibility Counts", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForCleanTermEligibility_Counts()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[term_eligibility_cnt] = " +
                              "COALESCE([term_eligibility_cnt],'0') ";

            Dumpsql("2", "Get Clean Term Eligibility Counts", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForCleanPPLEligibility_Counts()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[PPL_eligibility_cnt] = " +
                              "COALESCE([PPL_eligibility_cnt],'0') ";

            Dumpsql("2", "Get Clean PPL Eligibility Counts", s1);

            return s1;
        }

        // ========================================================================================

    }
}
