﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace eComm_Benefit_Extract
{
    public partial class Form1 : Form
    {
        // ========================================================================================

        private static string _relativebasepath = "..\\..\\..\\ZOutputFiles\\";
        private static string _logpath;

        //const string Server = "NYL-D-CVXSQL1";
        private const string Server = "NYL-D-RPTSQL1";

        // ========================================================================================

        public Form1()
        {
            InitializeComponent();
        }

        // ========================================================================================

        private void Output(string s1)
        {
            textBox1.AppendText(s1 + Environment.NewLine);
            //  _emailbody = _emailbody + s1 + Environment.NewLine;
            OutputD(s1);
        }

        // ====================================================================================

        private void Form1_Load(object sender, EventArgs e)
        {
            // Get config options
            SetupLog_and_BasePath();

            DisplayVersion(Server);
        }

        // ========================================================================================

        private void AddHeaders(SqlConnection con2, List<string> listofpipeddata2,string tablename)
        {
            var columns2 = GetTopRowForListOfColumns(con2, tablename);

            // Add Header of column names
            Addheader1(listofpipeddata2, columns2);

            // Add a header on top of one based numbering
         //   Addheader2(listofpipeddata2, columns2);
        }

        // ========================================================================================

        private void Remove_Defined_Columns_From_List(ref List<string> listofpipeddata, bool[] dec)
        {
            var list1 = new List<string>();

            var pieces = listofpipeddata[0].Split('|');

            for (var i = 0; i < listofpipeddata.Count; i++)
            {
                var s1 = listofpipeddata[i];

                for (var k = pieces.Length; k > 0; k--)
                {
                    if (!dec[k-1])
                    {
                        // use actual one based column numbers in descending order
                        s1 = remove_onebased_column(k, s1);
                    }
                }

                list1.Add(s1);
            }

            listofpipeddata = list1;

          //  FixupTopCounter(ref listofpipeddata);
        }

        // ========================================================================================

        private DataTable GetFromDatabase1(ref SqlConnection con, string gfilter)
        {

            RunNonQuery(con, true, "Create Active Benefit Plans Temp Table", GetSqlCreateActiveBenefitPlans());
            RunNonQuery(con, true, "Create Benefit And Term Temp Table", GetSqlCreateBenefitAndTerm());
            RunNonQuery(con, true, "Create Benefit And Term And RiderTemp Table", GetSqlCreateBenefitAndTermAndRider());
            RunNonQuery(con, true, "Create Employer Contract Riders Temp Table", GetSqlCreateEmployerContractRiders());
            RunNonQuery(con, true, "Create Final Results Temp Table", GetSqlForBenefitPlans());
            RunNonQuery(con, true, "Create Active Riders Temp Table", GetSqlCreateActiveRiders());

            // NOTE order of inserts is important dont change
            RunNonQuery(con, true, "Insert ActiveBenefitPlans", GetSqlForActiveBenefitPlans());
            RunNonQuery(con, true, "Insert BenefitAndTerm", GetSqlForBenefitAndTerm());
            RunNonQuery(con, true, "Insert Initial Data Into Final Results Table", GetSqlForEmployerContractRiders());
            RunNonQuery(con, true, "Insert ActiveRiders", GetSqlForActiveRiders());
            RunNonQuery(con, true, "Insert BenefitAndTermAndRider", GetSqlForBenefitAndTermAndRider());
            RunNonQuery(con, true, "Insert Initial Groups", GetSqlForInitialBenefit(gfilter));

            RunNonQuery(con, true, "Update Group Name", GetSqlForemployergroup_nm());
            RunNonQuery(con, true, "Update Group_Code", GetSqlForGroup_Code());
            RunNonQuery(con, true, "Update SubGroup_Code", GetSqlForSubgroup());
            RunNonQuery(con, true, "Update Employer_Group_ud", GetSqlForEmployerGroupUd());
            RunNonQuery(con, true, "Update Group_Contract_ud", GetSqlForGroupContractUd());
            RunNonQuery(con, true, "Update Group Contract Effective_Date", GetSqlForGroupContractEffDate());
            RunNonQuery(con, true, "Update Group Name", GetSqlForGroupName());
            RunNonQuery(con, true, "Update Plan_Code", GetSqlForPlanCode());

            // Filtering
            // not sure if this is 100% correct but it may clean up issues with joins that don't join up
            // demonstrates dvriderplan and riders are out of sync
            RunNonQuery(con, true, "Update NoPlanCode", GetSqlToRemovePlansWithNoPlanCode());

            RunNonQuery(con, true, "Update Rider", GetSqlForRider());

            // More Filtering 
            // not actually needed because this is done in initial GetSqlForInitialBenefit query (but no harm no foul)
            RunNonQuery(con, true, "Update NoBenefitPlans", GetSqlToRemoveNoBenefitRiders());

            RunNonQuery(con, true, "Update Auth_Prefix", GetSqlForPrefix());
            RunNonQuery(con, true, "Update Plan_Name", GetSqlForPlanName());
            RunNonQuery(con, true, "Update Plan_Number", GetSqlForPlanNumber());
            RunNonQuery(con, true, "Update employergroup_nm", GetSqlForemployergroup_nm2());
            RunNonQuery(con, true, "Update group_contract_nm", GetSqlForgroup_contract_nm());
            RunNonQuery(con, true, "Update dvriderplanid", GetSqlFordvriderplanid());

            // More Filtering 
            RunNonQuery(con, true, "Update NoDVRiderPlanId", GetSqlForReductionBasedOnNulldvriderplanid());

            RunNonQuery(con, true, "Update benefit_contract_benefit_plan_id", GetSqlForbenefit_contract_benefitplan_id());

            // this needs to be fixed
            //RunNonQuery(con, true, "Update Frame_Display_Template", GetSqlForFrame_Display_Template());

            RunNonQuery(con, true, "Update NA categories", GetSqlForNaCategories());

            RunNonQuery(con, true, "Update Standard_Frame_Discount", GetSqlForStandard_Frame_Discount());
            RunNonQuery(con, true, "Update Standard_Contacts_Discount", GetSqlForStandard_Contacts_Discount());
            
            
            RunNonQuery(con, true, "Update CleanupStandard_Frame_Discount", GetSqlForCleanupStandard_Frame_Discount());
            RunNonQuery(con, true, "Update CleanupStandard_Contacts_Discount", GetSqlForCleanupStandard_Contacts_Discount());

            // just insert a break in the SQL output
            DumpSqlComment("2", "Eligibility Counts in Each Benefit Contract - Rider");

            RunNonQuery(con, true, "Update Eligibility Counts", GetSqlForEligibility_Counts());
            RunNonQuery(con, true, "Update Active Eligibility Counts", GetSqlForActiveEligibility_Counts());
            RunNonQuery(con, true, "Update Future Eligibility Counts", GetSqlForFutureEligibility_Counts());
            RunNonQuery(con, true, "Update Term Eligibility Counts", GetSqlForTermEligibility_Counts());
            RunNonQuery(con, true, "Update PPL Eligibility Counts", GetSqlForPPLEligibility_Counts());
            RunNonQuery(con, true, "Update PPL Eligibility id", GetSqlForPplFirstEligibilityId());
            RunNonQuery(con, true, "Update PPL Eligibility ud", GetSqlForPplFirstEligibilityUd());



            // Make Nulls into Zeros
            RunNonQuery(con, true, "Clean  Eligibility Counts", GetSqlForCleanEligibility_Counts());
            RunNonQuery(con, true, "Clean  Active Eligibility Counts", GetSqlForCleanActiveEligibility_Counts());
            RunNonQuery(con, true, "Clean  Future Eligibility Counts", GetSqlForCleanFutureEligibility_Counts());
            RunNonQuery(con, true, "Clean  Term Eligibility Counts", GetSqlForCleanTermEligibility_Counts());
            RunNonQuery(con, true, "Clean  PPL Eligibility Counts", GetSqlForCleanPPLEligibility_Counts());

          //  RunNonQuery(con, true, "Clear  Empty Eligibility Rows", GetSqlForReductionofZeroEligibility());
           // RunNonQuery(con, true, "Clear  Empty Active Eligibility Rows", GetSqlForReductionofZeroActiveEligibility());

            RunNonQuery(con, true, "Update Exam Accum", GetSqlForPair_Cycle_Time());


            // just insert a break in the SQL output
            DumpSqlComment("2", "Yes No Fields");

            RunNonQuery(con, true, "Update [Has VDT] No", GetSqlForVDtoNo());
            RunNonQuery(con, true, "Update [Has VDT] Yes", GetSqlForVDtoYes());
            //RunNonQuery(con, true, "Update [Has VDT] Clean", GetSqlForCleanVd());

            RunNonQuery(con, true, "Update [Has Safety] No", GetSqlForSafetytoNo());
            RunNonQuery(con, true, "Update [Has Safety] Yes", GetSqlForSafetytoYes());
            //RunNonQuery(con, true, "Update [Has Safety] Clean", GetSqlForCleanSafety());

            RunNonQuery(con, true, "Update [Has 2 Dress Pair] No", GetSqlFor2DresstoNo());
            RunNonQuery(con, true, "Update [Has 2 Dress Pair] Yes", GetSqlFor2DresstoYes());
            //RunNonQuery(con, true, "Update [Has 2 Dress Pair] Clean", GetSqlForClean2Dress());


            RunNonQuery(con, true, "Update [Has Deductable] No", GetSqlForHasDeductabletoNo());
            RunNonQuery(con, true, "Update [Has Deductable] Yes", GetSqlForHasDetuctable());


            RunNonQuery(con, true, "Update [Benefit_Splitting] No", GetSqlForBenefit_SplittingNo());
            RunNonQuery(con, true, "Update [Benefit_Splitting] Yes", GetSqlForBenefit_SplittingYes());


            RunNonQuery(con, true, "Update [Flat_Allowance]", GetSqlForFlatAllowance());



            // just insert a break in the SQL output
            DumpSqlComment("2", "Copays");

            // Uses Temp Table field and [DVServiceClasses].[ClassID]

            RunNonQuery(con, true, "Start with Zero", GetSqlForCopayToZero());


            RunNonQuery(con, true, "Update SV", GetSqlForBenefitCopay("[Single_Vision]", "1"));
            RunNonQuery(con, true, "Update SV Count", GetSqlForBenefitCopayCnt("[Single_Vision_Cnt]", "1"));

            RunNonQuery(con, true, "Update Bifocal", GetSqlForBenefitCopay("[Bifocal]", "9"));
            RunNonQuery(con, true, "Update Bifocal Count", GetSqlForBenefitCopayCnt("[Bifocal_Cnt]", "9"));

            RunNonQuery(con, true, "Update Trifocal", GetSqlForBenefitCopay("[Trifocal]", "10"));
            RunNonQuery(con, true, "Update Trifocal Count", GetSqlForBenefitCopayCnt("[Trifocal_Cnt]", "10"));

            RunNonQuery(con, true, "Update Polarized", GetSqlForBenefitCopay("[Polarized]", "27"));
            RunNonQuery(con, true, "Update Polarized Count", GetSqlForBenefitCopayCnt("[Polarized_Cnt]", "27"));

            // High Impact
            RunNonQuery(con, true, "Update Trivex", GetSqlForBenefitCopay("[Trivex]", "26"));
            RunNonQuery(con, true, "Update Trivex Count", GetSqlForBenefitCopayCnt("[Trivex_Cnt]", "26"));

            RunNonQuery(con, true, "Update Polycarbonate", GetSqlForBenefitCopay("[Polycarbonate]", "28"));
            RunNonQuery(con, true, "Update Polycarbonate Count", GetSqlForBenefitCopayCnt("[Polycarbonate_Cnt]", "28"));

            RunNonQuery(con, true, "Update Progressive", GetSqlForBenefitCopay("[Progressive]", "50"));
            RunNonQuery(con, true, "Update Progressive Count", GetSqlForBenefitCopayCnt("[Progressive_Cnt]", "50"));

            RunNonQuery(con, true, "Update Progressive Standard", GetSqlForBenefitCopay("[Progressive_Standard]", "24"));
            RunNonQuery(con, true, "Update Progressive Standard Count", GetSqlForBenefitCopayCnt("[Progressive_Standard_Cnt]", "24"));

            RunNonQuery(con, true, "Update Progressive Premium", GetSqlForBenefitCopay("[Progressive_Premium]", "25"));
            RunNonQuery(con, true, "Update Progressive Premium Count", GetSqlForBenefitCopayCnt("[Progressive_Premium_Cnt]", "25"));

            RunNonQuery(con, true, "Update Progressive Ultra", GetSqlForBenefitCopay("[Progressive_Ultra]", "75"));
            RunNonQuery(con, true, "Update Progressive Ultra Count", GetSqlForBenefitCopayCnt("[Progressive_Ultra_Cnt]", "75"));

            RunNonQuery(con, true, "Update AR Standard", GetSqlForBenefitCopay("[AR_Standard]", "21"));
            RunNonQuery(con, true, "Update AR Standard Count", GetSqlForBenefitCopayCnt("[AR_Standard_Cnt]", "21"));

            RunNonQuery(con, true, "Update AR Premium", GetSqlForBenefitCopay("[AR_Premium]", "21"));
            RunNonQuery(con, true, "Update AR Premium Count", GetSqlForBenefitCopayCnt("[AR_Premium_Cnt]", "21"));

            RunNonQuery(con, true, "Update AR Ultra", GetSqlForBenefitCopay("[AR_Ultra]", "21"));
            RunNonQuery(con, true, "Update AR Ultra Count", GetSqlForBenefitCopayCnt("[AR_Ultra_Cnt]", "21"));

            RunNonQuery(con, true, "Update AR Ultra", GetSqlForBenefitCopay("[AR_Ultra]", "21"));
            RunNonQuery(con, true, "Update AR Ultra Count", GetSqlForBenefitCopayCnt("[AR_Ultra_Cnt]", "21"));

            RunNonQuery(con, true, "Update Transitions", GetSqlForBenefitCopay("[Transitions]", "35"));
            RunNonQuery(con, true, "Update Transitions Count", GetSqlForBenefitCopayCnt("[Transitions_Cnt]", "35"));

            RunNonQuery(con, true, "Update Transitions MF", GetSqlForBenefitCopay("[Transitions_MF]", "35"));
            RunNonQuery(con, true, "Update Transitions MF Count", GetSqlForBenefitCopayCnt("[Transitions_MF_Cnt]", "35"));
            
            RunNonQuery(con, true, "Update Contact Lens", GetSqlForBenefitCopay("[CL_Copay]", "21"));

            RunNonQuery(con, true, "Update Contact Lens Disposable", GetSqlForBenefitCopay("[CL_Disc]", "101"));




            DumpsqlLine("2", "");

            // Now that all the information is in the temp table we draw from that
            var sql = GetSqlForWholeMonster1();

            OutputDebugScripts();

            var tbl1 = Get_TableofDataFromDatabase(ref con, sql);
            return tbl1;
        }




        // ========================================================================================

        private void OutputDebugScripts()
        {
            DumpsqlLine("3", "");

            Dumpsql("3", "Get Shorthand", GetSqlShortHand());

            //  DumpSqlComment("--------------------------------------------");

            // for debugging

            Dumpsql("3", "Get Copay Cnt For a Specifc Benefit Plan", GetSqlForBenefitCopayBreakdownCnt());

            Dumpsql("3", "Get Copay Amounts For a Specifc Benefit Plan (older way)", GetSqlForBenefitCopayBreakdown());


            Dumpsql("3", "Get Copay Amounts For a Specifc Benefit Plan (new way)", GetSqlForBenefitCopayBreakdown2());

            Dumpsql("3", "Misc supporting snips", GetSqlForBenefitCopayBreakdown3());


            Dumpsql("3", "Get All Part1", "SELECT * FROM #ActiveBenefitPlans");
            Dumpsql("3", "Get All Part1", "SELECT * FROM #ActiveRiders");
            Dumpsql("3", "Get All Part1", "SELECT * FROM #BenefitAndTerm");
            Dumpsql("3", "Get All Part1", "SELECT * FROM #EmployerContractRiders");
            Dumpsql("3", "Get All Part1", "SELECT * FROM #MonsterTempTable1");


            DumpsqlLine("3", "");

            Dumpsql("3", "Get All Part1", "SELECT REPLACE(CONVERT(varchar, CAST(SUM(eligibility_Cnt) AS money), 1), '.00', '') AS 'All Eligibilities' FROM [#MonsterTempTable1]");
            Dumpsql("3", "Get All Part1", "SELECT REPLACE(CONVERT(varchar, CAST(SUM(active_eligibility_Cnt) AS money), 1), '.00', '') AS 'Active Eligibilities' FROM [#MonsterTempTable1]");
            Dumpsql("3", "Get All Part1", "SELECT REPLACE(CONVERT(varchar, CAST(SUM(future_eligibility_Cnt) AS money), 1), '.00', '') AS 'Future Eligibilities' FROM [#MonsterTempTable1]");
            Dumpsql("3", "Get All Part1", "SELECT REPLACE(CONVERT(varchar, CAST(SUM(term_eligibility_Cnt) AS money), 1), '.00', '') AS 'Termed Eligibilities' FROM [#MonsterTempTable1]");
            Dumpsql("3", "Get All Part1", "SELECT REPLACE(CONVERT(varchar, CAST(SUM(PPL_eligibility_Cnt) AS money), 1), '.00', '') AS '$PPL Eligibilities' FROM [#MonsterTempTable1]");


            DumpSqlComment("2", "--------------------------------------------");
        }


        // ========================================================================================

        private void SaveAsFile(List<string> listofpipeddata2)
        {

            var fpathcr2B = Path.Combine(_relativebasepath, "CVX_Benefits_In_Excel_Format" + ".tab");

            try
            {
            
                File.WriteAllLines(fpathcr2B, listofpipeddata2.ToArray());
            }
            catch (Exception ex)
            {
                Output(ex.Message);
                    
                Output("Cant Save File (Probably Open Already In Excel)");
            }

        }

        // ========================================================================================

        private List<string> DatabaseStuff(SqlConnection con2,string subsetFilter)
        {
            var list1 = new List<string>();

            var test = new Stopwatch(); test.Reset(); test.Start();

            try
            {
                con2.InfoMessage += conn_InfoMessage;
                con2.FireInfoMessageEventOnUserErrors = true;



                var tbl1 = GetFromDatabase1(ref con2, subsetFilter);
                list1 = TableToPipedStrings(tbl1);

                AddHeaders(con2, list1, "#MonsterTempTable1");
 
                con2.Close();
            }
            catch (Exception ex)
            {
                Output("Trapped Error:");
                Output("");
                Output(ex.Message);
            }

            test.Stop();
            OutputD("DatabaseStuff" + " in Elapsed Time: " + DisplayTime6(test));

            return list1;
        }

        // ========================================================================================

        private List<string> RunFullDatabaseQuerys(string subsetFilter)
        {
            ClearOldSqlDump();

            const string domain = "ad.davisvision.com";
            const string database = "CVX_PROD_1";

            // const string Server = "NYL-CVXSQL1";
            // const string Server = "NYL-D-RPTSQL1";

            var con2 = GetOpenConnection(domain, Server, database, "MacessUser", "m@c3ss");

           // var con2 = GetOpenConnection(domain, Server, database, "MacessUser", "leR$E+1LsoRs");

            var listofpipeddata = DatabaseStuff(con2,subsetFilter);

            Output(Dash(50));

            return listofpipeddata;
        }

        // ====================================================================================

        private static string Commafystr(string ss)
        {
            var s1 = ss.Trim();

            if (s1.Contains(",")) return ss;

            var s2 = "";

            for (var i = (s1.Length - 1); i > -1; i--)
            {
                s2 = s1[i] + s2;

                if (i == 0) continue;

                // get inverse count
                var j = s1.Length - i;

                if ((j % 3) == 0) { s2 = "," + s2; }
            }

            return s2;
        }   


        // ========================================================================================

        private void AddEligTotals(ref List<string> listofpipeddata)
        {
            //Output("Add Eligibility Totals");
            var list1 = new List<string>();


            if (listofpipeddata.Count > 0)
            {

                var eligibilityCnt = 0;
                var activeEligibilityCnt = 0;
                var futureEligibilityCnt = 0;
                var termEligibilityCnt = 0;
                var pplEligibilityCnt = 0;

                for (var i = 0; i < 2; i++)
                {
                    list1.Add(listofpipeddata[i]);
                }

                for (var i = 2; i < listofpipeddata.Count; i++)
                {

                    var sa = listofpipeddata[i].Split('|');

                    var i1 = Convert.ToInt32(sa[23]);
                    var i2 = Convert.ToInt32(sa[24]);
                    var i3 = Convert.ToInt32(sa[25]);
                    var i4 = Convert.ToInt32(sa[26]);
                    var i5 = Convert.ToInt32(sa[27]);

                    eligibilityCnt = eligibilityCnt + i1;

                    activeEligibilityCnt = activeEligibilityCnt + i2;

                    futureEligibilityCnt = futureEligibilityCnt + i3;

                    termEligibilityCnt = termEligibilityCnt + i4;

                    pplEligibilityCnt = pplEligibilityCnt + i5;

                    list1.Add(listofpipeddata[i]);
                }

                const string s1 = "|||||||||||||||||||||||";

                var s2 = Commafystr(eligibilityCnt.ToString()) + "|"
                         + Commafystr(activeEligibilityCnt.ToString()) + "|"
                         + Commafystr(futureEligibilityCnt.ToString()) + "|"
                         + Commafystr(termEligibilityCnt.ToString()) + "|"
                         + Commafystr(pplEligibilityCnt.ToString());

                const string s3 = "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||";

                list1.Add(s1 + s2 + s3);
                listofpipeddata = list1;

            }

        }

        // ========================================================================================

        private List<string> RemoveorAddTextQualifiers(List<string> listofpipeddata, bool[] dec)
        {
            const char delimiter = '|';

            var list1 = new List<string>();


            foreach (var item in listofpipeddata)
            {
                var pieces = item.Split('|');
                var s1 = "";

                for (var i = 0; i < pieces.Length; i++)
                {
                    string textqualifier;

                    if (dec[i])
                    {
                        textqualifier = ""; 
                    }
                    else
                    {
                        textqualifier = "'";
                    }

                    if (i != (pieces.Length - 1))
                    {
                        s1 = s1 + textqualifier + pieces[i] + textqualifier + delimiter;
                    }
                    else
                    {
                        s1 = s1 + textqualifier + pieces[i] + textqualifier;
                    }

                }

                list1.Add(s1);
            }

            return list1;
        }


        // ========================================================================================

        private void button2_Click(object sender, EventArgs e)
        {
            PrintDocument();

        }

        // ========================================================================================

        private List<string> ConvertToPipesToTabs(List<string> listofpipeddata)
        {
            const char delimiter = '\t';

            var list1 = new List<string>();

            foreach (var item in listofpipeddata)
            {
                var pieces = item.Split('|');
                var s1 = "";

                for (var i = 0; i < pieces.Length; i++)
                {
                    if (i != (pieces.Length - 1))
                    {
                        s1 = s1 + pieces[i] + delimiter;
                    }
                    else
                    {
                        s1 = s1 + pieces[i];
                    }
                }

                list1.Add(s1);
            }

            return list1;
        }

        // ====================================================================================

        private void Remove_BlankTextQuotes(ref List<string> listofpipeddata)
        {
            var list1 = new List<string>();
            const char delimiter = '|';


            for (var i = 0; i < listofpipeddata.Count; i++)
            {
                var s1 = listofpipeddata[i];
                var pieces = s1.Split('|');
                var s2 = "";

                for (var k = 0; k < pieces.Length; k++)
                {
                    if (pieces[k] == "''") { pieces[k] = ""; }

                    if (pieces[k] == "'0.00'") { pieces[k] = ""; }

                    if (k != (pieces.Length - 1))
                    {
                        s2 = s2 + pieces[k] + delimiter;
                    }
                    else
                    {
                        s2 = s2 + pieces[k];
                    }
                }

                list1.Add(s2);
            }

            listofpipeddata = list1;

        }







        // ========================================================================================

        private void button1_Click(object sender, EventArgs e)
        {
            var sTest = new Stopwatch();
            sTest.Reset();
            sTest.Start();

            // const string subsetFilter = "'XTN'";,'FNY'
            // const string subsetFilter = "'X1Z'";
            //  const string subsetFilter = "'FNY'";

         // const string subsetFilter = "'XTN','XM8','YF3','X10','Y1B','YAG','X1Z','YBY','YB4','YD6','X1Z','FNY'";       
          
          const string subsetFilter = "";             // final output will use this one for all groups 



            var listofpipeddata = RunFullDatabaseQuerys(subsetFilter);

  AddEligTotals(ref listofpipeddata);

            // GetbArrAllTrue2()                    
            //  GetbArrAllTrueButSubgroup()            final output will use this one
            // GetbArrForQuotesToStrip()
            listofpipeddata = RemoveorAddTextQualifiers(listofpipeddata, GetbArrAllTrueButSubgroup());

            // GetbArrForSpecification()            final output will use this one
            // GetbArrDebug()
            // GetbArrAllTrue2() 
            Remove_Defined_Columns_From_List(ref listofpipeddata, GetbArrForSpecification());

           // Remove_BlankTextQuotes(ref listofpipeddata);

            var listoftabs = ConvertToPipesToTabs(listofpipeddata);

            SaveAsFile(listoftabs);

            sTest.Stop();
            Output("");
            Output("");
            Output("Done" + " in Elapsed Time: " + DisplayTime6(sTest));
        }


        // ====================================================================================


    }
}
