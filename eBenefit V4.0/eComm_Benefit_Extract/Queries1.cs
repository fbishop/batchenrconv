﻿namespace eComm_Benefit_Extract
{
    public partial class Form1
    {
        private const string RiderFilterBlock = "" 
            + "AND member_category_set_ud NOT LIKE 'do not use%' " 
            + "AND member_category_set_ud NOT LIKE 'donotuse%' " 
            + "AND member_category_set_ud NOT LIKE 'test%' " 
            + "AND member_category_set_ud NOT LIKE 'error%' " 
            + "AND member_category_set_ud NOT LIKE 'WEB USE ONLY%' " 
            + "AND member_category_set_ud != 'DE9999999999999' " 
            + "AND member_category_set_ud NOT LIKE 'We received a GLOP ticket regarding' " 
            + "AND member_category_set_nm != 'No Benefit Rider'  " 
            + "AND member_category_set_nm != 'No Benefit Rider2' " 
            + "AND member_category_set_nm != 'Dummy Rider ID'  "
            + "AND member_category_set_ud NOT LIKE 'DI%' "
            + "AND member_category_set_ud NOT LIKE 'MO%'  "
            + "AND member_category_set_ud NOT LIKE '%.Any'  "
            + "AND member_category_set_ud NOT LIKE '%.G'  "
            + "AND member_category_set_ud NOT LIKE '%.DvDG' "
            + "AND member_category_set_ud NOT LIKE '%.CvCG'  "
            + "AND member_category_set_ud NOT LIKE '%.MvMG'  "
            + "AND member_category_set_ud NOT LIKE '%.DCvDCG' "
            + "AND member_category_set_ud NOT LIKE '%.CMvCMG' "
            + "AND member_category_set_ud NOT LIKE '%.DMvDMG'  "
            + "AND member_category_set_ud NOT LIKE '%.DCMvDMCG'  " 
            + "AND COALESCE(mcs.[description], '') NOT LIKE 'DO NOT USE%' " 
            + "AND member_category_set_nm NOT LIKE '%DO NOT USE%' " + "";

        // ========================================================================================
        //

        private string GetSqlForEmployerContractRiders()
        {
            const string s1 = "INSERT INTO #EmployerContractRiders  " +
                              "( " +
                              "[EmployerContractRiders_employergroup_id]  " +
                              ",[EmployerContractRiders_employergroup_ud]  " +
                              ",[EmployerContractRiders_group_code]  " +
                              ",[EmployerContractRiders_group_contract_id] " +
                              ",[EmployerContractRiders_group_contract_ud]  " +
                              ",[EmployerContractRiders_member_category_set_id]  " +
                              ",[EmployerContractRiders_member_category_set_ud]  " +
                              ",[EmployerContractRiders_ridercode]  " +
                              ") " +
                              "( " +
                              "SELECT " + 
                              "eg.employergroup_id " +
                              ",eg.employergroup_ud " +
                              ",eg.group_code " +
                              ",gc.group_contract_id " +
                              ",gc.group_contract_ud " +
                              ",mcs.member_category_set_id " +
                              ",mcs.member_category_set_ud " +
                              ",mcs.ridercode  " +
                              "FROM [member_category_set] mcs  " +
                              "JOIN [benefit_category] bc ON bc.[member_category_set_id] = mcs.[member_category_set_id] AND (bc.[term_date] IS NULL AND bc.[eff_date] < GetDate()) OR (bc.[term_date] IS NOT NULL AND bc.[term_date] > GetDate()) " +
                              "JOIN [group_contract] gc ON bc.[benefit_contract_id] = gc.[group_contract_id] AND gc.[contract_eff_date] < GetDate() AND gc.[contract_term_date] IS NULL " +
                              "JOIN [employergroup] eg ON eg.[employergroup_id] = gc.[employergroup_id] AND [group_code] IS NOT NULL AND eg.[employergroup_id] NOT IN (1,5,63,200,456,459,514,523,557,579) " +
                              "WHERE mcs.ridercode = 1  " + RiderFilterBlock + ") ";

            Dumpsql("2", "Insert Into EmployerContractRiders (~120,922 rows)", s1);

            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlForActiveBenefitPlans()
        {
            const string s1 = "INSERT INTO #ActiveBenefitPlans ([benefitplan_id]) " +
                              "( " +
                              "SELECT DISTINCT bcbp.[benefitplan_id] " +
                              "FROM [member_category_set] mcs  " +
                              "JOIN [benefit_category] bc ON bc.[member_category_set_id] = mcs.[member_category_set_id]  " +
                              "AND (bc.[term_date] IS NULL AND bc.[eff_date] < GetDate()) OR (bc.[term_date] IS NOT NULL  " +
                              "AND bc.[term_date] > GetDate()) " +
                              "JOIN [group_contract] gc ON bc.[benefit_contract_id] = gc.[group_contract_id]  " +
                              "AND gc.[contract_eff_date] < GetDate() AND gc.[contract_term_date] IS NULL  " +
                              "JOIN [employergroup] eg ON eg.[employergroup_id] = gc.[employergroup_id] AND [group_code] IS NOT NULL  " +
                              "JOIN [benefit_contract_benefitplan] bcbp ON gc.[group_contract_id] = bcbp.[benefit_contract_id]  " +
                              "AND bcbp.[term_date] IS NULL and bcbp.[eff_date] <= GetDate()  " +
                              "JOIN [benefitplan] bp on bcbp.[benefitplan_id] = bp.[benefitplan_id]  " +
                              "WHERE mcs.ridercode = 1  " + RiderFilterBlock + ") ";

            Dumpsql("2", "Insert Into ActiveBenefitPlans (~128 rows)", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForInitialBenefit(string s2)
        {
            var s1 =
                "INSERT INTO #MonsterTempTable1 (" +
                "eg.employergroup_id, " +
                "eg.employergroup_ud, " +
                "eg.group_code, " +
                "gc.group_contract_id, " +
                "gc.group_contract_ud, " +
                "mcs.member_category_set_id, " +
                "mcs.member_category_set_ud, " +
                "mcs.ridercode) " +
                "(SELECT " +
                "eg.employergroup_id, " +
                "eg.employergroup_ud, " +
                "eg.group_code, " +
                "gc.group_contract_id, " +
                "gc.group_contract_ud, " +
                "mcs.member_category_set_id, " +
                "mcs.member_category_set_ud, " +
                "mcs.ridercode " +
                "FROM [member_category_set] mcs " +
                "JOIN [benefit_category] bc ON bc.[member_category_set_id] = mcs.[member_category_set_id] AND (bc.[term_date] IS NULL AND bc.[eff_date] < GetDate()) OR (bc.[term_date] IS NOT NULL AND bc.[term_date] > GetDate()) " +
                "JOIN [group_contract] gc ON bc.[benefit_contract_id] = gc.[group_contract_id] AND gc.[contract_eff_date] < GetDate() AND gc.[contract_term_date] IS NULL " +
                "JOIN [employergroup] eg ON eg.[employergroup_id] = gc.[employergroup_id] AND [group_code] IS NOT NULL AND eg.[employergroup_id] NOT IN (1,5,63,200,456,459,514,523,557,579) " +
                "WHERE mcs.ridercode = 1  " + RiderFilterBlock;

            if (s2 != "")
            {
                s1 = s1 + " AND eg.group_code IN (" + s2 + ")";
            }

            s1 = s1 + ")";

            // 121,012 rows why differnt from (~120,922 rows)

            Dumpsql("2", "Insert Initial Groups and Riders (" + s2 + ")", s1);
            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlForBenefitAndTerm()
        {
            const string s1 = "INSERT INTO #BenefitAndTerm  " +
                              "( " +
                              "[benefit_benefit_id]  " +
                              ",[benefit_benefit_ud]  " +

                              ",[benefit_class_id]  " +
                              ",[benefit_class]  " +

                              ",[benefit_benefit_nm]  " +
                              ",[benefit_benefit_benefit_type] " +
                              ",[benefit_benefit_node_type_id] " +
                              ",[benefit_benefitplan_id] " +
                              ",[benefitterm_benefit_term_id]  " +
                              ",[benefitterm_benefit_id]  " +
                              ",[benefitterm_benefit_tier]  " +
                              ",[benefitterm_coverage_tier]  " +
                              ",[benefitterm_eff_date]  " +
                              ",[benefitterm_term_date]  " +
                              ",[benefitmcs_member_category_set_id] " + 
                              ",[benefitmcs_member_category_set_ud]  " +
                              ",[benefitmcs_member_category_set_nm] " + 
                              ",[benefitmcs_description]  " +
                              ",[benefitmcs_ridercode]  " +

                              ",[benefitmcsd_member_category_set_id]  " +
                              ",[benefitmcsd_member_category_id]  " +
                              ",[benefitmcsd_eff_date]  " +
                              ",[benefitmcsd_term_date] " + 
                              ",[benefitmcsd_ridercode]  " +

                              ",[benefitmcs_member_category_id]  " +
                              ",[benefitmcs_member_category_ud] " +
                              ",[benefitmcs_member_category_nm] " +
                              //",[benefit_rider_id]  " +
                              //",[benefit_rider_ud]  " +
                              ") " +
                              "( " +
                              "SELECT DISTINCT " +
                              " b.[benefit_id] " + 
                              ",b.[benefit_ud]  " +
                              ",sc.ClassID " +
                              ",sc.Class " +

                              ",b.[benefit_nm]  " +
                              ",b.[benefit_type] " +
                              ",b.[benefit_node_type_id] " +
                              ",b.[benefitplan_id] " +
                              ",bt.[benefit_term_id]  " +
                              ",bt.[benefit_id]  " +
                              ",bt.[benefit_tier]  " +
                              ",bt.[coverage_tier]  " +
                              ",bt.[eff_date]  " +
                              ",bt.[term_date]  " +
                              ",mcs.[member_category_set_id]  " +
                              ",mcs.[member_category_set_ud]  " +
                              ",mcs.[member_category_set_nm]  " +
                              ",mcs.[description]  " +
                              ",mcs.[ridercode]  " +

                              ",mcsd.[member_category_set_id]  " +
                              ",mcsd.[member_category_id] " + 
                              ",mcsd.[eff_date]  " +
                              ",mcsd.[term_date]  " +
                              ",mcsd.[ridercode]  " +

                              ",mc.[member_category_id]  " +
                              ",mc.[member_category_ud]  " +
                              ",mc.[member_category_nm]  " +
                              //",Br.[rider_id]  " +
                              //",Br.[rider_ud]  " +
                              "FROM  " +
                              "[benefit] b " +
                              "JOIN [benefit_term] bt ON b.[benefit_id] = bt.[benefit_id] AND bt.[eff_date] <= GetDate() AND bt.[term_date] IS NULL  " +
                              "JOIN #ActiveBenefitPlans a ON a.[benefitplan_id] = b.[benefitplan_id] " +
                              "JOIN benefit_codegroup bcg ON b.benefit_id = bcg.benefit_id  " +
                              "JOIN codegroup cg ON cg.codegroup_id = bcg.codegroup_id " + 
                              "JOIN procedure_codegroup_detail pcd on cg.codegroup_id = pcd.codegroup_id " +  
                              "join DVProcedureCodesServiceClasses pcsc on pcsc.ProcedureCode >= pcd.begin_procedurecode_ud and pcsc.ProcedureCode <= pcd.end_procedurecode_ud  " +
                              "join DVServiceClasses sc on pcsc.ClassID = sc.ClassID " +
                              "JOIN codegroup ModifierCodeGroup ON ModifierCodeGroup.codegroup_id = bcg.modifier_codegroup_id AND ModifierCodeGroup.codegroup_ud = 'NP' " + 
                              "JOIN [member_category_set] mcs ON bt.[member_category_set_id] = mcs.[member_category_set_id] AND mcs.[ridercode] != 1 " +
                              "JOIN [member_category_set_detail] mcsd ON mcs.[member_category_set_id] = mcsd.[member_category_set_id] AND (mcsd.term_date IS NULL OR mcsd.term_date > GetDate()) AND mcsd.eff_date < GetDate() " +
                              "JOIN [member_category] mc ON mc.[member_category_id] = mcsd.[member_category_id] " +
                              //"JOIN  " +
                              //"( " +
                              //"SELECT DISTINCT mcs.[member_category_set_id] as rider_id,  mcs.[member_category_set_ud] as rider_ud " +
                              //"FROM [member_category_set_detail] mcsd " +
                              //"JOIN [member_category_set] mcs on mcs.[member_category_set_id] = mcsd.[member_category_set_id] and mcs.[ridercode] = 1 " +
                              //") Br on Br.[rider_id] = mcsd.[member_category_id] " +
                              ") ";

            Dumpsql("2", "Insert Into BenefitAndTerm (~47,288 rows now XXbizillions)", s1);

            return s1;
        }


        // ========================================================================================
        //

        private string GetSqlForBenefitAndTermAndRider()
        {
            const string s1 = "INSERT INTO #BenefitAndTermAndRider  " +
                              "( " +
                              "[benefit_benefit_id]  " +
                              ",[benefit_benefit_ud]  " +

                              ",[benefit_class_id]  " +
                              ",[benefit_class]  " +

                              ",[benefit_benefit_nm]  " +
                              ",[benefit_benefit_benefit_type] " +
                              ",[benefit_benefit_node_type_id] " +
                              ",[benefit_benefitplan_id] " +
                              ",[benefitterm_benefit_term_id]  " +
                              ",[benefitterm_benefit_id]  " +
                              ",[benefitterm_benefit_tier]  " +
                              ",[benefitterm_coverage_tier]  " +
                              ",[benefitterm_eff_date]  " +
                              ",[benefitterm_term_date]  " +
                              ",[benefitmcs_member_category_set_id] " +
                              ",[benefitmcs_member_category_set_ud]  " +
                              ",[benefitmcs_member_category_set_nm] " +
                              ",[benefitmcs_description]  " +
                              ",[benefitmcs_ridercode]  " +

                              ",[benefitmcsd_member_category_set_id]  " +
                              ",[benefitmcsd_member_category_id]  " +
                              ",[benefitmcsd_eff_date]  " +
                              ",[benefitmcsd_term_date] " +
                              ",[benefitmcsd_ridercode]  " +

                              ",[benefitmcs_member_category_id]  " +
                              ",[benefitmcs_member_category_ud] " +
                              ",[benefitmcs_member_category_nm] " +

                              ",[MCS_Rider_Key] " +
                              ",[benefit_rider_ud] " +
                              ",[benefit_To_Rider_Key] " +

                              ") " +
                              "( " +
                              "SELECT " +
                              " bandt.[benefit_benefit_id] " +
                              ",bandt.[benefit_benefit_ud]  " +
                              ",bandt.benefit_class_id " +
                              ",bandt.benefit_class " +

                              ",bandt.[benefit_benefit_nm]  " +
                              ",bandt.[benefit_benefit_benefit_type] " +
       
                              ",bandt.[benefit_benefit_node_type_id] " +
                              ",bandt.[benefit_benefitplan_id] " +
                              ",bandt.[benefitterm_benefit_term_id]  " +
                              ",bandt.[benefitterm_benefit_id]  " +
                              ",bandt.[benefitterm_benefit_tier]  " +
                              ",bandt.[benefitterm_coverage_tier]  " +
                              ",bandt.[benefitterm_eff_date]  " +
                              ",bandt.[benefitterm_term_date]  " +

                              ",bandt.[benefitmcs_member_category_set_id]  " +
                              ",bandt.[benefitmcs_member_category_set_ud]  " +
                              ",bandt.[benefitmcs_member_category_set_nm]  " +
                              ",bandt.[benefitmcs_description]  " +
                              ",bandt.[benefitmcs_ridercode]  " +

                              ",bandt.[benefitmcsd_member_category_set_id]  " +
                              ",bandt.[benefitmcsd_member_category_id] " +
                              ",bandt.[benefitmcsd_eff_date]  " +
                              ",bandt.[benefitmcsd_term_date]  " +
                              ",bandt.[benefitmcsd_ridercode]  " +

                              ",bandt.[benefitmcs_member_category_id]  " +
                              ",bandt.[benefitmcs_member_category_ud]  " +
                              ",bandt.[benefitmcs_member_category_nm]  " +

                              ",act.[MCS_Rider_Key] " +
                              ",act.[Rider_ud] " +
                              ",act.[benefit_To_Rider_Key] " +

                              "FROM #BenefitAndTerm bandt " +
                              "JOIN #ActiveRiders act ON act.Benefit_To_Rider_Key = bandt.benefitmcs_member_category_id " +
                              ") ";

            Dumpsql("2", "Insert Into BenefitAndTermAndRider (XXbizillions)", s1);

            return s1;
        }



        // ========================================================================================
        //

        private string GetSqlForActiveRiders()
        {
            const string s1 = "INSERT INTO #ActiveRiders  " +
                              "( " +
                              " [MCS_Rider_Key]  " +
                              ",[Rider_ud]  " +
                              ",[Benefit_To_Rider_Key] " +
                              ") " +
                              "( " +
                              "SELECT  " +
                              "DISTINCT  " +
                              "[EmployerContractRiders_member_category_set_id] " +
                              ",[EmployerContractRiders_member_category_set_ud] " + 
                              ",mcsd.member_category_id " + 
                              "From #Employercontractriders " +
                              "JOIN member_category_set_detail mcsd " 
            + "on mcsd.member_category_set_id = EmployerContractRiders_member_category_set_id " +
            "and mcsd.term_date is null " +
                              ") ";

            Dumpsql("2", "Insert Into ActiveRiders (~3,735 rows)", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForemployergroup_nm()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Group_Name] = eg.[employergroup_nm] " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [employergroup] eg ON eg.[employergroup_id] = T2.[employergroup_id] ";

            Dumpsql("2", "Get Employer Group Name", s1);
            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForgroup_contract_nm()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[group_contract_nm] = gc.[group_contract_nm] " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [group_contract] gc ON gc.[group_contract_id] = T2.[group_contract_id] ";

            Dumpsql("2", "Get Benefit Contract Name (Group Contract Name)", s1);
            return s1;
        }

        // ========================================================================================
        // 
        private string GetSqlForemployergroup_nm2()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[employergroup_nm] = eg.[employergroup_nm] " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [employergroup] eg ON eg.[employergroup_id] = T2.[employergroup_id] ";

            Dumpsql("2", "Get Employer Group Name2", s1);
            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForGroup_Code()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Group_Code] = eg.[group_code] " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [employergroup] eg ON eg.[employergroup_id] = T2.[employergroup_id] ";

            Dumpsql("2", "Get Group Code ([group_code] in [employergroup])", s1);
            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForSubgroup()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[SubGroup_Code] = COALESCE(gcd.[dv_subgroup], '') " +

                              "FROM [#MonsterTempTable1] T2 " +

                              "JOIN [CVX_Prod_1].[dbo].[dv_group_contract_def] gcd " +
                              "on T2.[group_contract_id] = gcd.[group_contract_id] ";

            Dumpsql("2", "Get Sub Group ([dv_subgroup] in [dv_group_contract_def])", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForEmployerGroupUd()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Employer_Group_UD] = eg.[employergroup_ud] " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [employergroup] eg ON eg.[employergroup_id] = T2.[employergroup_id] ";

            Dumpsql("2", "Get Payor ([employergroup_ud] in [employergroup])", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForGroupContractUd()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Benefit_Contract_UD] = gc.[group_contract_ud] " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [group_contract] gc ON gc.[group_contract_id] = T2.[group_contract_id] ";

            Dumpsql("2", "Get Benefit Contract", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForGroupContractEffDate()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Effective_Date] = CONVERT(CHAR(10), gc.[contract_eff_date], 120) " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [group_contract] gc ON gc.[group_contract_id] = T2.[group_contract_id] ";

            Dumpsql("2", "Get Benefit Contract EffDate", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForGroupName()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Group_Name] = COALESCE(eg.[employergroup_nm], '') " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [employergroup] eg ON eg.[employergroup_id] = T2.[employergroup_id] ";

            Dumpsql("2", "Get GroupName", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForPlanCode()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Plan_Code] = COALESCE(dvrp.[Plan_Code], '') " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [benefit_category] bc ON bc.[member_category_set_id] = T2.[member_category_set_id] " +
                              "JOIN [group_contract] gc ON  bc.[benefit_contract_id] = gc.[group_contract_id] " +
                              "JOIN [employergroup] eg ON eg.[employergroup_id] = gc.[employergroup_id] " +
                              "JOIN [dvRiderPlan] dvrp ON eg.[employergroup_id] = dvrp.[employer_id] AND dvrp.member_category_set_id = T2.member_category_set_id " +
                              "WHERE T2.[ridercode] = 1";

            Dumpsql("2", "Get PlanCode ([Plan_Code] in [dvRiderPlan])", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForRider()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Benefit_Rider] = COALESCE(mcs.[member_category_set_ud], '') " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN  [member_category_set] mcs ON mcs.[member_category_set_id] = T2.[member_category_set_id] ";

            Dumpsql("2", "Get Rider ([member_category_set_ud] in [member_category_set])", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForPrefix()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Auth_Prefix] = COALESCE(dvrp.[Prefix], '') " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [benefit_category] bc ON bc.[member_category_set_id] = T2.[member_category_set_id] " +
                              "JOIN [group_contract] gc ON  bc.[benefit_contract_id] = gc.[group_contract_id] " +
                              "JOIN [employergroup] eg ON eg.[employergroup_id] = gc.[employergroup_id] " +
                              "JOIN [dvRiderPlan] dvrp ON eg.[employergroup_id] = dvrp.[employer_id] AND dvrp.member_category_set_id = T2.member_category_set_id " +
                              "WHERE T2.[ridercode] = 1";

            Dumpsql("2", "Get Prefix ([Prefix] in [dvRiderPlan])", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForPlanName()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Plan_Name] = COALESCE(mcs.[member_category_set_nm], '') " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN  [member_category_set] mcs ON mcs.[member_category_set_id] = T2.[member_category_set_id] ";

            Dumpsql("2", "Get PlanName ([member_category_set_nm] in [member_category_set])", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForPlanNumber()
        {
            const string s1 = "UPDATE T2 " +
                  "SET " +
                  "T2.[Plan_Number] = COALESCE(mcs.[member_category_set_id], '') " +
                  "FROM [#MonsterTempTable1] T2 " +
                  "JOIN [member_category_set] mcs ON mcs.[member_category_set_id] = T2.[member_category_set_id] ";

            Dumpsql("2", "Get PlanNumber ([member_category_set_id] in [member_category_set])", s1);

            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlFordvriderplanid()
        {
            const string s1 = "UPDATE T2 " +
                  "SET T2.dvriderplanid = COALESCE(CONVERT(VARCHAR, dvrp.[employer_id]), '') + '-' + COALESCE(CONVERT(VARCHAR, dvrp.[member_category_set_id]), '') " +
                  "FROM [#MonsterTempTable1] T2 " +
                  "JOIN [dvriderplan] dvrp ON dvrp.[member_category_set_id] = T2.[member_category_set_id] AND T2.[employergroup_id] = dvrp.[employer_id] ";

            Dumpsql("2", "Get PlanID (dvrp.[employer_id]-dvrp.[member_category_set_id])", s1);

            return s1;
        }
        
        // ========================================================================================
        //

        private string GetSqlForbenefit_contract_benefitplan_id()
        {
            const string s1 = "UPDATE T2 " +
                  "SET " +
                  "T2.[benefit_contract_benefitplan_id] = bcbp.[benefit_contract_benefitplan_id]," + 
                  "T2.[benefitplan_id] = bcbp.[benefitplan_id]  " +
                  "FROM [#MonsterTempTable1] T2 " +
                  "LEFT JOIN [benefit_contract_benefitplan] bcbp " + 
                  "ON bcbp.[benefit_contract_id] = T2.[group_contract_id] " + 
                  "AND bcbp.[benefit_contract_id] = T2.[group_contract_id] ";

            Dumpsql("2", "Get benefit_contract_benefitplan_id", s1);

            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlForFrame_Display_Template()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Frame_Display_Template] = 'Frame_Display_Template' " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN  [member_category_set] mcs ON mcs.[member_category_set_id] = T2.[member_category_set_id] ";

            Dumpsql("2", "Get Frame_Display_Template", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlToRemoveNoBenefitRiders()
        {
            const string s1 = @"DELETE FROM [#MonsterTempTable1] WHERE [Plan_Name] like '%No Benefit Rider%'";

            Dumpsql("2", "Remove NoBenefitRiders (this is not really needed since the incomming rider filter takes care of this)", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlToRemovePlansWithNoPlanCode()
        {
            const string s1 = @"DELETE FROM [#MonsterTempTable1] WHERE [Plan_Code] IS NULL";

            Dumpsql("2", "Filter Rows with No Plan_Code (Join Issues  turns out these are all Oscar" + 
                " Insurance Corporation or Highmark Blue Shield Delaware  3900 of them)", s1);

            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlForCopayToZero()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET " +
                              "[Flat_Allowance] = 0, " +
                              "[Progressive_Standard] = 0, " +
                              "[Single_Vision] = 0, " +
                              "[Bifocal] = 0, " +
                              "[Trifocal] = 0, " +
                              "[Progressive] = 0, " +
                              "[Progressive_Premium] = 0, " +
                              "[Progressive_Ultra] = 0, " +
                              "[AR_Standard] = 0, " +
                              "[AR_Premium] = 0, " +
                              "[AR_Ultra] = 0, " +
                              "[Transitions] = 0, " +
                              "[Transitions_MF] = 0, " +
                              "[Polarized] = 0, " +
                              "[Polycarbonate] = 0, " +
                              "[Trivex] = 0, " +
                              "[CL_Disc] = 0, " +
                              "[CL_Copay] = 0";

            Dumpsql("2", "Start With Zeros",s1);

            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlForBenefitCopay(string a1,string a2)
        {
            var s1 = "UPDATE [#MonsterTempTable1] SET [#MonsterTempTable1]." + a1 + " = A.value " +
                              "FROM " +
                              "( " +
                              "SELECT MAX(ISNULL(bc.value, 0.0 ))*2 AS value, bandt.benefit_benefitplan_id, bandt.[MCS_Rider_Key] " +
                              "FROM #BenefitAndTermAndRider bandt " +
                              "JOIN benefit_copay bc ON bc.benefit_id = bandt.benefit_benefit_id AND bandt.benefit_class_id = " + a2 + " " +
                              "GROUP BY bandt.benefit_benefitplan_id,bandt.[MCS_Rider_Key] " +
                              ") A " +
                              "WHERE A.benefit_benefitplan_id = [#MonsterTempTable1].benefitplan_id " + 
                              "AND A.[MCS_Rider_Key] = [#MonsterTempTable1].[member_category_set_id]";

            Dumpsql("2", "Get Benefit Copay " + a1, s1);

            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlForBenefitCopayCnt(string a1, string a2)
        {
            var s1 = "UPDATE [#MonsterTempTable1] SET [#MonsterTempTable1]." + a1 + " = A.cnt " +
                              "FROM " +
                              "( " +
                              "SELECT Count(ISNULL(bc.value, 0)) AS cnt, bandt.benefit_benefitplan_id, bandt.[MCS_Rider_Key] " +
                              "FROM #BenefitAndTermAndRider bandt " +
                              "JOIN benefit_copay bc ON bc.benefit_id = bandt.benefit_benefit_id AND bandt.benefit_class_id = " + a2 + " " +
                              "GROUP BY bandt.benefit_benefitplan_id,bandt.[MCS_Rider_Key] " +
                              ") A " +
                              "WHERE A.benefit_benefitplan_id = [#MonsterTempTable1].benefitplan_id " + 
                              "AND A.[MCS_Rider_Key] = [#MonsterTempTable1].[member_category_set_id]";

            Dumpsql("2", "Get Benefit Copay Cnt " + a1, s1);

            return s1;
        }

        // ========================================================================================
        private string GetSqlForPair_Cycle_Time()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [#MonsterTempTable1].[Pair_Cycle_Time] = A.cnt " +
                              "FROM " +
                              "( " +


                              //"SELECT TOP 1 bad.accumulator_rollover_number AS cnt" +
                              //"FROM #BenefitAndTermAndRider bandt " +
                              //"JOIN benefit_accumulator ba ON ba.benefit_id = bandt.benefit_benefit_id " +
                              //"JOIN benefit_accum_def bad ON ba.benefit_accum_def_id = bad.benefit_accum_def_id " +
                              //"WHERE bandt.benefit_class_id = 3 " +
                              //"AND benefit_accum_def_ud like 'EX_%'" + 

                              "SELECT  " +
                              "bandt.benefit_benefitplan_id " +
                              ", bandt.[MCS_Rider_Key] " +
                              ",bad.accumulator_rollover_number AS cnt " +
                              "FROM #BenefitAndTermAndRider bandt " + 
                              "JOIN benefit_accumulator ba ON ba.benefit_id = bandt.benefit_benefit_id " + 
                              "JOIN benefit_accum_def bad ON ba.benefit_accum_def_id = bad.benefit_accum_def_id " + 
                              "WHERE bandt.benefit_class_id = 1 " +

                              // 1 Single Vision Spectacle Lens
                              // 2 Frame
                              // 3 Routine
                              // 4 Glasses

                              ") A " +
                              "WHERE A.benefit_benefitplan_id = [#MonsterTempTable1].benefitplan_id " +
                              "AND A.[MCS_Rider_Key] = [#MonsterTempTable1].[member_category_set_id]";


            Dumpsql("2", "Get Pair_Cycle_Time ", s1);

            return s1;






        }


        private string GetSqlForHasDetuctable()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [#MonsterTempTable1].[Has_Deductable] = A.[Has_D] " +
                              "FROM " +
                              "( " +


                              //"SELECT " +
                              //"batar.benefit_benefitplan_id, " +
                              //"batar.[MCS_Rider_Key], " +
                              //"CASE WHEN COUNT(batar.benefit_benefitplan_id) > 0 THEN 'Y' ELSE 'N' END AS [Has_D] " + 
                              //"FROM #BenefitAndTermAndRider batar " + 
                              //"JOIN benefit_accumulator ba on batar.benefit_benefit_id = ba.benefit_id " + 
                              //"JOIN benefit_accum_def bad ON ba.benefit_accum_def_id = bad.benefit_accum_def_id AND bad.benefit_accum_type_id = 1 " +
                              //"GROUP BY batar.benefit_benefitplan_id, batar.benefit_rider_u " +
                              //") A " +
                              //"WHERE A.benefitplan_id = [#MonsterTempTable1].benefitplan_id " +
                              //"AND A.[MCS_Rider_Key] = [#MonsterTempTable1].[member_category_set_id]";




            "SELECT " +
            "batar.[benefit_benefitplan_id] " +
            ", batar.[MCS_Rider_Key] " +
//--, benefit_rider_ud 
//--,COUNT(batar.benefit_benefitplan_id) 
//--,
            ",CASE WHEN COUNT(batar.benefit_benefitplan_id) > 0 THEN 'Y' ELSE 'N' END AS Has_D " +
            "FROM #BenefitAndTermAndRider batar " +
            "JOIN benefit_accumulator ba on batar.benefit_benefit_id = ba.benefit_id " +
            "JOIN benefit_accum_def bad ON ba.benefit_accum_def_id = bad.benefit_accum_def_id " +
            "AND bad.benefit_accum_type_id = 1 " +
            "GROUP BY batar.benefit_benefitplan_id, batar.MCS_Rider_Key " +  // ,benefit_rider_ud " +

            ") A  " +
            "WHERE A.[benefit_benefitplan_id] = [#MonsterTempTable1].benefitplan_id "; 

    //--AND A.[MCS_Rider_Key] = [#MonsterTempTable1].[member_category_set_id]







            Dumpsql("2", "Get Has Deductable ", s1);

            return s1;


        }


        // ========================================================================================

        private string GetSqlForFlatAllowance()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [#MonsterTempTable1].[Flat_Allowance] = A.[maximum] " +
                              "FROM " +
                              "( " +

                              "SELECT " +
                              "batar.[benefit_benefitplan_id] " +
                              ", batar.[MCS_Rider_Key] " +
                              ",bad.maximum " +
                              "FROM #BenefitAndTermAndRider batar " +
                              "JOIN benefit_accumulator ba on batar.benefit_benefit_id = ba.benefit_id " +
                              "JOIN benefit_accum_def bad ON ba.benefit_accum_def_id = bad.benefit_accum_def_id " +
                              "AND bad.benefit_accum_type_id = 5 " +
                              "GROUP BY batar.benefit_benefitplan_id, batar.MCS_Rider_Key,[maximum] " +

                              ") A  " +
                              "WHERE A.[benefit_benefitplan_id] = [#MonsterTempTable1].benefitplan_id " +

                              "AND A.[MCS_Rider_Key] = [#MonsterTempTable1].[member_category_set_id] ";







            Dumpsql("2", "Get FlatAllowance ", s1);

            return s1;


        }


        // ========================================================================================

        private string GetSqlForReductionofZeroEligibility()
        {
            const string s1 = @"DELETE FROM [#MonsterTempTable1] WHERE [eligibility_cnt] = 0";

            Dumpsql("2", "Remove NoEligibility Rows", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForReductionBasedOnNulldvriderplanid()
        {
            const string s1 = @"DELETE FROM [#MonsterTempTable1] WHERE [dvriderplanid] IS NULL";

            Dumpsql("2", "Remove NoDvRiderRef Rows (~101,294 rows out of the ~120,953 rows All from Oscar Insurance Corporation or Highmark Blue Shield Delaware ", s1);

            return s1;
        }

        // ========================================================================================
        // 

        private string GetSqlForWholeMonster1()
        {
            const string s1 = "SELECT * FROM #MonsterTempTable1 " 
                + "ORDER BY [Group_Code],[SubGroup_Code],[Plan_Code]";

            Dumpsql("2", "Get All Part1", s1);
            return s1;
        }

        // ========================================================================================
        // 
    }
}