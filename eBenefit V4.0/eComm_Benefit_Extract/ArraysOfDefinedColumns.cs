﻿namespace eComm_Benefit_Extract
{
    public partial class Form1
    {

        // ========================================================================================

        private bool[] GetbArrForSpecification()
        {
            var bArray = new bool[92];

            // If true then remove 
            bArray[00] = true; // "GROUP_NAME"
            bArray[01] = true; //"GROUP_CODE"
            bArray[02] = true; //"SUBGROUP_CODE"
            bArray[03] = true; //"PLAN_CODE"
            bArray[04] = true; //"EMPLOYER_GROUP_UD"
            bArray[05] = true; //"BENEFIT_CONTRACT_UD"
            bArray[06] = true; //"BENEFIT_RIDER"
            bArray[07] = true; //"AUTH_PREFIX"
            bArray[08] = true; //"PLAN_NAME"
            bArray[09] = true; //"PLAN_NUMBER"

            bArray[10] = true; //"EFFECTIVE_DATE"
            bArray[11] = false; //"EMPLOYERGROUP_ID"
            bArray[12] = false; //"EMPLOYERGROUP_UD"
            bArray[13] = false; //"EMPLOYERGROUP_NM"
            bArray[14] = false; //"GROUP_CONTRACT_ID"
            bArray[15] = false; //"GROUP_CONTRACT_UD"
            bArray[16] = false; //"GROUP_CONTRACT_NM"
            bArray[17] = false; //"MEMBER_CATEGORY_SET_ID"
            bArray[18] = false; //"MEMBER_CATEGORY_SET_UD"
            bArray[19] = false; //"RIDERCODE"

            bArray[20] = false; //"DVRIDERPLANID"
            bArray[21] = false; //"BENEFIT_CONTRACT_BENEFITPLAN_ID"
            bArray[22] = false; //"BENEFITPLAN_ID"
            bArray[23] = false; //"ELIGIBILITY_CNT"
            bArray[24] = false; //"ACTIVE_ELIGIBILITY_CNT"
            bArray[25] = false; //"FUTURE_ELIGIBILITY_CNT"
            bArray[26] = false; //"TERM_ELIGIBILITY_CNT"
            bArray[27] = false; //"PPL_ELIGIBILITY_CNT"
            bArray[28] = false; //"PPL_ELIGIBILITY_ID"
            bArray[29] = false; //"PPL_ELIGIBILITY_UD"

            bArray[30] = true; //"MAX_CHILD"
            bArray[31] = true; //"POLY_ELIGIBLE_POWER"
            bArray[32] = true; //"SAME_DAY_PURCHASE"
            bArray[33] = true; //"BENEFIT_SPLITTING"
            bArray[34] = true; //"50%_OFF_ALLOWED"
            bArray[35] = true; //"PRICE_PROGRESSIVE_AS"
            bArray[36] = true; //"PAIR_CYCLE_TIME"
            bArray[37] = true; //"PAIR_CONFIGURATION"
            bArray[38] = true; //"FRAME_DISPLAY_TEMPLATE"
            bArray[39] = true; //"FRAME_ALLOWANCE_TYPE"

            bArray[40] = true; //"HAS_SAFETY"
            bArray[41] = true; //"HAS_DRESS"
            bArray[42] = true; //"HAS_2_DRESS_PAIR"
            bArray[43] = true; //"HAS_TWO_TIERED_CONTACTS"
            bArray[44] = true; //"HAS_VDT"
            bArray[45] = true; //"HAS_VOUCHER"
            bArray[46] = true; //"PAYER_TERM"
            bArray[47] = true; //"FLAT_ALLOWANCE"
            bArray[48] = true; //"FRAME_COPAY"
            bArray[49] = true; //"MATERIAL_COPAY"

            bArray[50] = true; //"STANDARD_FRAME"
            bArray[51] = true; //"STANDARD_FRAME_DISCOUNT"
            bArray[52] = false; //"PROGRESSIVE_STANDARD_CNT"
            bArray[53] = true; //"PROGRESSIVE_STANDARD"
            bArray[54] = false; //"SINGLE_VISION_CNT"
            bArray[55] = true; //"SINGLE_VISION"
            bArray[56] = false; //"BIFOCAL_CNT"
            bArray[57] = true; //"BIFOCAL"
            bArray[58] = false;//"TRIFOCAL_CNT"
            bArray[59] = true;//"TRIFOCAL"

            bArray[60] = false; //"PROGRESSIVE_CNT"
            bArray[61] = true; //"PROGRESSIVE"
            bArray[62] = false; //"PROGRESSIVE_PREMIUM_CNT"
            bArray[63] = true; //"PROGRESSIVE_PREMIUM"
            bArray[64] = false; //"PROGRESSIVE_ULTRA_CNT"
            bArray[65] = true; //"PROGRESSIVE_ULTRA"
            bArray[66] = false; //"AR_STANDARD_CNT"
            bArray[67] = true; //"AR_STANDARD"
            bArray[68] = false; //"AR_PREMIUM_Cnt"
            bArray[69] = true; //"AR_PREMIUM"

            bArray[70] = false; //"AR_ULTRA_CNT"
            bArray[71] = true; //"AR_ULTRA"
            bArray[72] = false; //"TRANSITIONS_CNT"
            bArray[73] = true; //"TRANSITIONS"
            bArray[74] = false; //"TRANSITIONS_MF_CNT"
            bArray[75] = true; //"TRANSITIONS_MF"
            bArray[76] = false; //"POLARIZED_CNT"
            bArray[77] = true; //"POLARIZED"
            bArray[78] = false; //"POLYCARBONATE_CNT"
            bArray[79] = true; //"POLYCARBONATE"

            bArray[80] = false; //"TRIVEX_CNT"
            bArray[81] = true; //"TRIVEX"
            bArray[82] = true; //"CONTACT_LENS_TIER_1"
            bArray[83] = true; //"CONTACT_LENS_TIER_2"
            bArray[84] = true; //"STANDARD_CONTACTS_DISCOUNT"
            bArray[85] = true; //"CL_DISC"
            bArray[86] = true; //"CL_COPAY"
            bArray[87] = true; //"CL_BENEFIT_BANKING"
            bArray[88] = true; //"PLAN_GROUP"
            bArray[89] = true; //"HAS_DEDUCTABLE"

            bArray[90] = true; //"CONTACT_LENS_TIER_3"
            bArray[91] = true; //"CONTACT_LENS_TIER_4"

            return bArray;
        }

        // ========================================================================================

        private bool[] GetbArrForQuotesToStrip()
        {
            var bArray = new bool[92];

            // If true then remove 
            bArray[00] = true; // "GROUP_NAME"
            bArray[01] = true; //"GROUP_CODE"
            bArray[02] = true; //"SUBGROUP_CODE"
            bArray[03] = true; //"PLAN_CODE"
            bArray[04] = true; //"EMPLOYER_GROUP_UD"
            bArray[05] = true; //"BENEFIT_CONTRACT_UD"
            bArray[06] = true; //"BENEFIT_RIDER"
            bArray[07] = true; //"AUTH_PREFIX"
            bArray[08] = true; //"PLAN_NAME"
            bArray[09] = true; //"PLAN_NUMBER"

            bArray[10] = true; //"EFFECTIVE_DATE"
            bArray[11] = true; //"EMPLOYERGROUP_ID"
            bArray[12] = true; //"EMPLOYERGROUP_UD"
            bArray[13] = true; //"EMPLOYERGROUP_NM"
            bArray[14] = true; //"GROUP_CONTRACT_ID"
            bArray[15] = true; //"GROUP_CONTRACT_UD"
            bArray[16] = true; //"GROUP_CONTRACT_NM"
            bArray[17] = true; //"MEMBER_CATEGORY_SET_ID"
            bArray[18] = true; //"MEMBER_CATEGORY_SET_UD"
            bArray[19] = true; //"RIDERCODE"

            bArray[20] = true; //"DVRIDERPLANID"
            bArray[21] = true; //"BENEFIT_CONTRACT_BENEFITPLAN_ID"
            bArray[22] = true; //"BENEFITPLAN_ID"
            bArray[23] = true; //"ELIGIBILITY_CNT"
            bArray[24] = true; //"ACTIVE_ELIGIBILITY_CNT"
            bArray[25] = true; //"FUTURE_ELIGIBILITY_CNT"
            bArray[26] = true; //"TERM_ELIGIBILITY_CNT"
            bArray[27] = true; //"PPL_ELIGIBILITY_CNT"
            bArray[28] = true; //"PPL_ELIGIBILITY_ID"
            bArray[29] = true; //"PPL_ELIGIBILITY_UD"

            bArray[30] = true; //"MAX_CHILD"
            bArray[31] = true; //"POLY_ELIGIBLE_POWER"
            bArray[32] = true; //"SAME_DAY_PURCHASE"
            bArray[33] = true; //"BENEFIT_SPLITTING"
            bArray[34] = true; //"50%_OFF_ALLOWED"
            bArray[35] = true; //"PRICE_PROGRESSIVE_AS"
            bArray[36] = true; //"PAIR_CYCLE_TIME"
            bArray[37] = true; //"PAIR_CONFIGURATION"
            bArray[38] = true; //"FRAME_DISPLAY_TEMPLATE"
            bArray[39] = true; //"FRAME_ALLOWANCE_TYPE"

            bArray[40] = true; //"HAS_SAFETY"
            bArray[41] = true; //"HAS_DRESS"
            bArray[42] = true; //"HAS_2_DRESS_PAIR"
            bArray[43] = true; //"HAS_TWO_TIERED_CONTACTS"
            bArray[44] = true; //"HAS_VDT"
            bArray[45] = true; //"HAS_VOUCHER"
            bArray[46] = true; //"PAYER_TERM"
            bArray[47] = true; //"FLAT_ALLOWANCE"
            bArray[48] = true; //"FRAME_COPAY"
            bArray[49] = true; //"MATERIAL_COPAY"

            bArray[50] = true; //"STANDARD_FRAME"
            bArray[51] = true; //"STANDARD_FRAME_DISCOUNT"
            bArray[52] = true; //"PROGRESSIVE_STANDARD_CNT"
            bArray[53] = false; //"PROGRESSIVE_STANDARD"
            bArray[54] = true; //"SINGLE_VISION_CNT"
            bArray[55] = false; //"SINGLE_VISION"
            bArray[56] = true; //"BIFOCAL_CNT"
            bArray[57] = false; //"BIFOCAL"
            bArray[58] = true;//"TRIFOCAL_CNT"
            bArray[59] = false;//"TRIFOCAL"

            bArray[60] = true; //"PROGRESSIVE_CNT"
            bArray[61] = false; //"PROGRESSIVE"
            bArray[62] = true; //"PROGRESSIVE_PREMIUM_CNT"
            bArray[63] = false; //"PROGRESSIVE_PREMIUM"
            bArray[64] = true; //"PROGRESSIVE_ULTRA_CNT"
            bArray[65] = false; //"PROGRESSIVE_ULTRA"
            bArray[66] = true; //"AR_STANDARD_CNT"
            bArray[67] = false; //"AR_STANDARD"
            bArray[68] = true; //"AR_PREMIUM_Cnt"
            bArray[69] = false; //"AR_PREMIUM"

            bArray[70] = true; //"AR_ULTRA_CNT"
            bArray[71] = false; //"AR_ULTRA"
            bArray[72] = true; //"TRANSITIONS_CNT"
            bArray[73] = false; //"TRANSITIONS"
            bArray[74] = true; //"TRANSITIONS_MF_CNT"
            bArray[75] = false; //"TRANSITIONS_MF"
            bArray[76] = true; //"POLARIZED_CNT"
            bArray[77] = false; //"POLARIZED"
            bArray[78] = true; //"POLYCARBONATE_CNT"
            bArray[79] = false; //"POLYCARBONATE"

            bArray[80] = true; //"TRIVEX_CNT"
            bArray[81] = false; //"TRIVEX"
            bArray[82] = true; //"CONTACT_LENS_TIER_1"
            bArray[83] = true; //"CONTACT_LENS_TIER_2"
            bArray[84] = true; //"STANDARD_CONTACTS_DISCOUNT"
            bArray[85] = true; //"CL_DISC"
            bArray[86] = false; //"CL_COPAY"
            bArray[87] = true; //"CL_BENEFIT_BANKING"
            bArray[88] = true; //"PLAN_GROUP"
            bArray[89] = true; //"HAS_DEDUCTABLE"

            bArray[90] = true; //"CONTACT_LENS_TIER_3"
            bArray[91] = true; //"CONTACT_LENS_TIER_4"

            return bArray;
        }

        // ========================================================================================

        private bool[] GetbArrDebug()
        {
            var bArray = new bool[92];

            // If true then remove 
            bArray[00] = true; // "GROUP_NAME"
            bArray[01] = true; //"GROUP_CODE"
            bArray[02] = false; //"SUBGROUP_CODE"
            bArray[03] = false; //"PLAN_CODE"
            bArray[04] = false; //"EMPLOYER_GROUP_UD"
            bArray[05] = false; //"BENEFIT_CONTRACT_UD"
            bArray[06] = false; //"BENEFIT_RIDER"
            bArray[07] = false; //"AUTH_PREFIX"
            bArray[08] = false; //"PLAN_NAME"
            bArray[09] = false; //"PLAN_NUMBER"

            bArray[10] = false; //"EFFECTIVE_DATE"
            bArray[11] = false; //"EMPLOYERGROUP_ID"
            bArray[12] = false; //"EMPLOYERGROUP_UD"
            bArray[13] = false; //"EMPLOYERGROUP_NM"
            bArray[14] = false; //"GROUP_CONTRACT_ID"
            bArray[15] = true; //"GROUP_CONTRACT_UD"
            bArray[16] = false; //"GROUP_CONTRACT_NM"
            bArray[17] = false; //"MEMBER_CATEGORY_SET_ID"
            bArray[18] = true; //"MEMBER_CATEGORY_SET_UD"
            bArray[19] = false; //"RIDERCODE"

            bArray[20] = false; //"DVRIDERPLANID"
            bArray[21] = false; //"BENEFIT_CONTRACT_BENEFITPLAN_ID"
            bArray[22] = true; //"BENEFITPLAN_ID"
            bArray[23] = true; //"ELIGIBILITY_CNT"
            bArray[24] = true; //"ACTIVE_ELIGIBILITY_CNT"
            bArray[25] = true; //"FUTURE_ELIGIBILITY_CNT"
            bArray[26] = true; //"TERM_ELIGIBILITY_CNT"
            bArray[27] = true; //"PPL_ELIGIBILITY_CNT"
            bArray[28] = true; //"PPL_ELIGIBILITY_ID"
            bArray[29] = true; //"PPL_ELIGIBILITY_UD"

            bArray[30] = false; //"MAX_CHILD"
            bArray[31] = false; //"POLY_ELIGIBLE_POWER"
            bArray[32] = false; //"SAME_DAY_PURCHASE"
            bArray[33] = false; //"BENEFIT_SPLITTING"
            bArray[34] = false; //"50%_OFF_ALLOWED"
            bArray[35] = false; //"PRICE_PROGRESSIVE_AS"
            bArray[36] = false; //"PAIR_CYCLE_TIME"
            bArray[37] = false; //"PAIR_CONFIGURATION"
            bArray[38] = false; //"FRAME_DISPLAY_TEMPLATE"
            bArray[39] = false; //"FRAME_ALLOWANCE_TYPE"

            bArray[40] = false; //"HAS_SAFETY"
            bArray[41] = false; //"HAS_DRESS"
            bArray[42] = false; //"HAS_2_DRESS_PAIR"
            bArray[43] = false; //"HAS_TWO_TIERED_CONTACTS"
            bArray[44] = false; //"HAS_VDT"
            bArray[45] = false; //"HAS_VOUCHER"
            bArray[46] = false; //"PAYER_TERM"
            bArray[47] = false; //"FLAT_ALLOWANCE"
            bArray[48] = false; //"FRAME_COPAY"
            bArray[49] = false; //"MATERIAL_COPAY"

            bArray[50] = false; //"STANDARD_FRAME"
            bArray[51] = false; //"STANDARD_FRAME_DISCOUNT"
            bArray[52] = false; //"PROGRESSIVE_STANDARD_CNT"
            bArray[53] = false; //"PROGRESSIVE_STANDARD"


            bArray[54] = true; //"SINGLE_VISION_CNT"
            bArray[55] = true; //"SINGLE_VISION"
            bArray[56] = true; //"BIFOCAL_CNT"
            bArray[57] = true; //"BIFOCAL"
            bArray[58] = true;//"TRIFOCAL_CNT"
            bArray[59] = true;//"TRIFOCAL"

            bArray[60] = true; //"PROGRESSIVE_CNT"
            bArray[61] = true; //"PROGRESSIVE"
            bArray[62] = true; //"PROGRESSIVE_PREMIUM_CNT"
            bArray[63] = true; //"PROGRESSIVE_PREMIUM"
            bArray[64] = true; //"PROGRESSIVE_ULTRA_CNT"
            bArray[65] = true; //"PROGRESSIVE_ULTRA"
            bArray[66] = true; //"AR_STANDARD_CNT"
            bArray[67] = true; //"AR_STANDARD"
            bArray[68] = true; //"AR_PREMIUM_Cnt"
            bArray[69] = true; //"AR_PREMIUM"

            bArray[70] = true; //"AR_ULTRA_CNT"
            bArray[71] = true; //"AR_ULTRA"
            bArray[72] = true; //"TRANSITIONS_CNT"
            bArray[73] = true; //"TRANSITIONS"
            bArray[74] = true; //"TRANSITIONS_MF_CNT"
            bArray[75] = true; //"TRANSITIONS_MF"
            bArray[76] = true; //"POLARIZED_CNT"
            bArray[77] = true; //"POLARIZED"
            bArray[78] = true; //"POLYCARBONATE_CNT"
            bArray[79] = true; //"POLYCARBONATE"

            bArray[80] = true; //"TRIVEX_CNT"
            bArray[81] = true; //"TRIVEX"


            bArray[82] = false; //"CONTACT_LENS_TIER_1"
            bArray[83] = false; //"CONTACT_LENS_TIER_2"
            bArray[84] = false; //"STANDARD_CONTACTS_DISCOUNT"
            bArray[85] = false; //"CL_DISC"
            bArray[86] = true; //"CL_COPAY"
            bArray[87] = false; //"CL_BENEFIT_BANKING"
            bArray[88] = false; //"PLAN_GROUP"
            bArray[89] = false; //"HAS_DEDUCTABLE"

            bArray[90] = false; //"CONTACT_LENS_TIER_3"
            bArray[91] = false; //"CONTACT_LENS_TIER_4"

            return bArray;
        }

        // ========================================================================================

        private bool[] GetbArrAllTrue2()
        {
            var bArray = new bool[92];

            for (var i = 0; i < bArray.Length; i++)
            {
                bArray[i] = true;
            }

            return bArray;
        }


        // ========================================================================================

        private bool[] GetbArrAllTrueButSubgroup()
        {
            var bArray = new bool[92];

            for (var i = 0; i < bArray.Length; i++)
            {
                bArray[i] = true;
            }

            bArray[02] = false;
            
            return bArray;
        }

        // ========================================================================================



    }
}
