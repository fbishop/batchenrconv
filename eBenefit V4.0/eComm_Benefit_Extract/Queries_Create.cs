﻿namespace eComm_Benefit_Extract
{
    public partial class Form1
    {

        // ========================================================================================
        //

        private string GetSqlCreateActiveBenefitPlans()
        {
            const string s1 = "CREATE TABLE #ActiveBenefitPlans ([benefitplan_id] int)";
            Dumpsql("2", "Create ActiveBenefitPlans Temp Table (~128 rows)", s1);
            return s1;
        }
        // ========================================================================================
        //

        private string GetSqlCreateActiveRiders()
        {
            const string s1 = "CREATE TABLE #ActiveRiders ([MCS_Rider_Key] int,[Rider_ud] varchar(35),[Benefit_To_Rider_Key] int)";
            Dumpsql("2", "Create ActiveRiders Temp Table  (~218,113 rows)", s1);
            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlCreateBenefitAndTerm()
        {
            const string s1 = "CREATE TABLE #BenefitAndTerm ( " +
                              "[benefit_benefit_id] int " +
                              ",[benefit_benefit_ud] varchar(35) " +
                              ",[benefit_class_id] int " +
                              ",[benefit_class] varchar(50) " +
                              ",[benefit_benefit_nm] varchar(50) " +
                              ",[benefit_benefit_benefit_type] int " +
                              ",[benefit_benefit_node_type_id] int " +
                              ",[benefit_benefitplan_id] int " +
                              ",[benefitterm_benefit_term_id] int " +
                              ",[benefitterm_benefit_id] int " +
                              ",[benefitterm_benefit_tier] int " +
                              ",[benefitterm_coverage_tier] int " +
                              ",[benefitterm_eff_date] datetime " +
                              ",[benefitterm_term_date] datetime " +
                              ",[benefitmcs_member_category_set_id] int " +
                              ",[benefitmcs_member_category_set_ud] varchar(35) " +
                              ",[benefitmcs_member_category_set_nm] varchar(35) " +
                              ",[benefitmcs_description] varchar(50) " +
                              ",[benefitmcs_ridercode] bit " +

                              ",[benefitmcsd_member_category_set_id]  int " +
                              ",[benefitmcsd_member_category_id] int " +
                              ",[benefitmcsd_eff_date] datetime " +
                              ",[benefitmcsd_term_date] datetime  " +
                              ",[benefitmcsd_ridercode] bit  " +

                              ",[benefitmcs_member_category_id] int " +
                              ",[benefitmcs_member_category_ud] varchar(35) " +
                              ",[benefitmcs_member_category_nm] varchar(50) " +

                              ",[benefit_rider_id] int " +
                              ",[benefit_rider_ud] varchar(35) " +

                              ") ";
            Dumpsql("2", "Create BenefitAndTerm Temp Table (~47,288 rows)", s1);
            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlCreateBenefitAndTermAndRider()
        {
            const string s1 = "CREATE TABLE #BenefitAndTermAndRider ( " +
                              "[benefit_benefit_id] int " +
                              ",[benefit_benefit_ud] varchar(35) " +
                              ",[benefit_class_id] int " +
                              ",[benefit_class] varchar(50) " +
                              ",[benefit_benefit_nm] varchar(50) " +
                              ",[benefit_benefit_benefit_type] int " +
                              ",[benefit_benefit_node_type_id] int " +
                              ",[benefit_benefitplan_id] int " +
                              ",[benefitterm_benefit_term_id] int " +
                              ",[benefitterm_benefit_id] int " +
                              ",[benefitterm_benefit_tier] int " +
                              ",[benefitterm_coverage_tier] int " +
                              ",[benefitterm_eff_date] datetime " +
                              ",[benefitterm_term_date] datetime " +
                              ",[benefitmcs_member_category_set_id] int " +
                              ",[benefitmcs_member_category_set_ud] varchar(35) " +
                              ",[benefitmcs_member_category_set_nm] varchar(35) " +
                              ",[benefitmcs_description] varchar(50) " +
                              ",[benefitmcs_ridercode] bit " +

                              ",[benefitmcsd_member_category_set_id]  int " +
                              ",[benefitmcsd_member_category_id] int " +
                              ",[benefitmcsd_eff_date] datetime " +
                              ",[benefitmcsd_term_date] datetime  " +
                              ",[benefitmcsd_ridercode] bit  " +

                              ",[benefitmcs_member_category_id] int " +
                              ",[benefitmcs_member_category_ud] varchar(35) " +
                              ",[benefitmcs_member_category_nm] varchar(50) " +

                              ",[MCS_Rider_Key] int " +
                              ",[benefit_rider_ud] varchar(35) " +
                              ",[benefit_To_Rider_Key] int " +

                              ") ";
            Dumpsql("2", "Create BenefitAndTermAndRider Temp Table (~5,142,622 rows)", s1);
            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlCreateEmployerContractRiders()
        {
            const string s1 = "CREATE TABLE #EmployerContractRiders ( " +
                              " [EmployerContractRiders_employergroup_id] int " +
                              ",[EmployerContractRiders_employergroup_ud] varchar(35) " +
                              ",[EmployerContractRiders_group_code] varchar(3) " +
                              ",[EmployerContractRiders_group_contract_id] int " +
                              ",[EmployerContractRiders_group_contract_ud] varchar(35) " +
                              ",[EmployerContractRiders_member_category_set_id] int " +
                              ",[EmployerContractRiders_member_category_set_ud] varchar(35) " +
                              ",[EmployerContractRiders_ridercode] bit " +
                              ",[EmployerContractRiders_icnt] int " +
                              ",[EmployerContractRiders_str] varchar(35) " +
                              ")	";
            Dumpsql("2", "Create EmployerContractRiders Temp Table (~120,953 rows)", s1);
            return s1;
        }

        // ========================================================================================
        // by employer group and/or specific benefit contracts 

        private string GetSqlForBenefitPlans()
        {
            const string s1 = "CREATE TABLE #MonsterTempTable1 " +
                              "( " +
                // One based numbering of fields                 // Full      Final
                              "[Group_Name] varchar(80), " +                   // 01        01
                              "[Group_Code] varchar(3), " +                    // 02        02
                              "[SubGroup_Code] varchar(3), " +                 // 03        03
                              "[Plan_Code] varchar(3), " +                     // 04        04
                              "[Employer_Group_UD] varchar(35), " +            // 05        05
                              "[Benefit_Contract_UD] varchar(35), " +          // 06        06
                              "[Benefit_Rider] varchar(35), " +                // 07        07
                              "[Auth_Prefix] varchar(35), " +                  // 08        08
                              "[Plan_Name] varchar(35), " +                    // 09        09
                              "[Plan_Number] varchar(35), " +                  // 10        10

                              "[Effective_Date] varchar(35), " +               // 11        11

                              // Identifies Groups
                // From [employergroup] eg  
                              "[employergroup_id] int, " +                     // 12        *
                              "[employergroup_ud] varchar(35), " +             // 13        *
                              "[employergroup_nm] varchar(50), " +             // 14        *

                              // (Identifies Benefit Contracts)
                // From [group_contract] gc
                              "[group_contract_id] int, " +                    // 15        *
                              "[group_contract_ud] varchar(35), " +            // 16        *
                              "[group_contract_nm] varchar(50), " +            // 17        *

                              // (Identifies Riders)
                // From [member_category_set] mcs  
                              "[member_category_set_id] int, " +               // 18        *
                              "[member_category_set_ud] varchar(35), " +       // 19        *
                              "[ridercode] int, " +                            // 20        *

                              "[dvriderplanid] varchar(35), " +                // 21        *
                              "[benefit_contract_benefitplan_id] int, " +      // 22        *
                              "[benefitplan_id] int, " +                       // 23        *


                              "[eligibility_Cnt] int, " +                      // 24        *
                              "[active_eligibility_Cnt] int, " +               // 25        *
                              "[future_eligibility_Cnt] int, " +               // 26        *
                              "[term_eligibility_Cnt] int, " +                 // 27        *
                              "[PPL_eligibility_Cnt] int, " +                  // 28        *

                              "[PPL_eligibility_id] int, " +                   // 29        *

                              "[PPL_eligibility_ud] varchar(35), " +           // 30        *

                              "[Max_Child] varchar(35), " +                    // 31        12
                              "[Poly_Eligible_Power] varchar(35), " +          // 32        13
                              "[Same_Day_Purchase] varchar(35), " +            // 33        14
                              "[Benefit_Splitting] varchar(35), " +            // 34        15
                              "[50%_Off_Allowed] varchar(35), " +              // 35        16
                              "[Price_Progressive_As] varchar(35), " +         // 36        17
                              "[Pair_Cycle_Time] varchar(35), " +              // 37        18

                              "[Pair_Configuration] varchar(35), " +           // 38        19
                              "[Frame_Display_Template] varchar(35), " +       // 39        20
                              "[Frame_Allowance_Type] varchar(35), " +         // 40        21
                              "[Has_Safety] varchar(35), " +                   // 41        22
                              "[Has_Dress] varchar(35), " +                    // 42        23
                              "[Has_2_Dress_Pair] varchar(35), " +             // 43        24
                              "[Has_Two_Tiered_Contacts] varchar(35), " +      // 44        25
                              "[Has_VDT] varchar(35), " +                      // 45        26
                              "[Has_Voucher] varchar(35), " +                  // 46        27
                              "[Payer_Term] varchar(35), " +                   // 47        28

                              "[Flat_Allowance] varchar(35), " +               // 48        29
                              "[Frame_Copay] varchar(35), " +                // 49        30
                              "[Material_Copay] varchar(35), " +             // 50        31
                              "[Standard_Frame] varchar(35), " +               // 51        32
                              "[Standard_Frame_Discount] varchar(35), " +      // 52        33

                              "[Progressive_Standard_Cnt] varchar(35), " +     // 53        *        
                              "[Progressive_Standard] varchar(35), " +         // 54        34



                              "[Single_Vision_Cnt] int, " +                    // 55        *             
                              "[Single_Vision] varchar(35), " +                // 56        35             
                             
                              "[Bifocal_Cnt] int, " +                          // 57        *
                              "[Bifocal] varchar(35), " +                      // 58        36

                              "[Trifocal_Cnt] int, " +                         // 59        *
                              "[Trifocal] varchar(35), " +                     // 60        37


                              "[Progressive_Cnt] int, " +                      // 61        *
                              "[Progressive] varchar(35), " +                  // 62        38
                              "[Progressive_Premium_Cnt] int, " +              // 63        *
                              "[Progressive_Premium] varchar(35), " +          // 64        39
                              "[Progressive_Ultra_Cnt] int, " +                // 65        *
                              "[Progressive_Ultra] varchar(35), " +            // 66        40


                              "[AR_Standard_Cnt] int, " +                      // 67        *                              
                              "[AR_Standard] varchar(35), " +                  // 68        41
                              "[AR_Premium_Cnt] int, " +                       // 69        *
                              "[AR_Premium] varchar(35), " +                   // 70        42
                              "[AR_Ultra_Cnt] int, " +                         // 71        *
                              "[AR_Ultra] varchar(35), " +                     // 72        43

                              "[Transitions_Cnt] int, " +                      // 73        *
                              "[Transitions] varchar(35), " +                  // 74        44
                              "[Transitions_MF_Cnt] int, " +                   // 75        *
                              "[Transitions_MF] varchar(35), " +               // 76        45
                              "[Polarized_Cnt] int, " +                        // 77        *
                              "[Polarized] varchar(35), " +                    // 78        46

                              "[Polycarbonate_Cnt] int, " +                    // 79        *
                              "[Polycarbonate] varchar(35), " +                // 80        47
                              "[Trivex_Cnt] int, " +                           // 81        *
                              "[Trivex] varchar(35), " +                       // 82        48

                              "[Contact_Lens_Tier_1] varchar(35), " +          // 83        49
                              "[Contact_Lens_Tier_2] varchar(35), " +          // 84        50
                              "[Standard_Contacts_Discount] varchar(35), " +   // 85        51
                              "[CL_Disc] varchar(35), " +                      // 86        52
                              "[CL_Copay] varchar(35), " +                     // 87        53
                              "[CL_Benefit_Banking] varchar(35), " +           // 88        54
                              "[Plan_Group] varchar(35), " +                   // 89        55
                              "[Has_Deductable] varchar(35), " +               // 90        56
                              "[Contact_Lens_Tier_3] varchar(35), " +          // 91        57
                              "[Contact_Lens_Tier_4] varchar(35) " +           // 92        58
                              ")";

            Dumpsql("2", "Create Final Results Temp Table (Initially ~120,953 rows)", s1);

            return s1;
        }

        // Remove_Columns_From_list1 is at line 67 of Form1.cs




    }
}
