﻿namespace eComm_Benefit_Extract
{
    public partial class Form1
    {


        // ========================================================================================
        // 

        private string GetSqlShortHand()
        {
            const string s1 = "SELECT "
                + "  [Group_Name] "
                + ", [Group_Code]  "
                + ", [employergroup_id]  "
                + ", [employergroup_ud]  "
                + ", [group_contract_id]  "
                + ", [group_contract_ud]  "
                + ", [member_category_set_id]  "
                + ", [member_category_set_ud] "
                + ", [Pair_Cycle_Time] "
                + ", [Single_Vision_Cnt]  "
                + ", [Single_Vision]  "
                + ", [Bifocal_Cnt]  "
                + ", [Bifocal] "
                + ", [Trifocal_Cnt] "
                + ", [Trifocal]  "
                + ", [Progressive_Cnt] "
                + ", [Progressive] "
                + "FROM #MonsterTempTable1  "
                + "ORDER BY [Group_Code],[SubGroup_Code],[Plan_Code] ";

            return s1;
        }

        // ========================================================================================
        //
        private string GetSqlForBenefitCopayBreakdownCnt()
        {
            const string s1 = "SELECT * FROM #BenefitAndTerm bandt";
            return s1;
        }


        // ========================================================================================
        //
        private string GetSqlForBenefitCopayBreakdown2()
        {
            // [Progressive] 50

            const string s1 =
                "SELECT bandt.benefit_benefitplan_id  ,benefit_benefit_id,bc.benefit_copay_id ,ISNULL(bc.value, 0.0 )*2 AS value , " +
                "benefit_rider_ud, bandt.[MCS_Rider_Key] " +
                "FROM #BenefitAndTermAndRider bandt " +
                "JOIN benefit_copay bc ON bc.benefit_id = bandt.benefit_benefit_id " +
                "AND bandt.benefit_class_id = 50 " +
                "AND benefit_benefitplan_id = 286 " +  // fidelis
                "AND benefit_rider_ud = 'DE00000511' " +   // this lines narrows most to 1 entry
                "ORDER BY benefit_benefit_id  ";



            return s1;
        }



                // ========================================================================================
        //
        private string GetSqlForBenefitCopayBreakdown3()
        {

            const string s1 =
                "-- riders for a employer group " +
                "SELECT * FROM employergroup where group_code = 'FNY' " +


                "SELECT * FROM employergroup eg " +
                "JOIN DVRiderPlan dvrp ON dvrp.employer_id = eg.employerGroup_id " +
                "WHERE group_code = 'FNY' " +
                "-- 30 rows " +


                "SELECT mcs.* FROM employergroup eg " +
                "JOIN DVRiderPlan dvrp ON dvrp.employer_id = eg.employerGroup_id " +
                "JOIN member_category_set mcs on mcs.member_category_set_id = dvrp.member_category_set_id " +
                "WHERE group_code = 'FNY' " +
                "-- 30 rows " +


                "SELECT mcs.* FROM employergroup eg " +
                "JOIN DVRiderPlan dvrp ON dvrp.employer_id = eg.employerGroup_id " +
                "JOIN member_category_set mcs on mcs.member_category_set_id = dvrp.member_category_set_id " +
                "WHERE group_code = 'FNY' --and member_category_set_ud = 'DE00000511' " +
                "-- 30 rows " +




                "-- riders for a group contract " +
                "SELECT * FROM group_contract where group_contract_ud = '40F900000014' " +


                "SELECT category,* FROM group_contract " +
                "JOIN Benefit_category on group_contract_id = benefit_contract_id " +
                "WHERE group_contract_ud = '40F900000014' " +
                "-- 12 ";


            return s1;
        }

        







        // ========================================================================================
        //
        private string GetSqlForBenefitCopayBreakdown()
        {
            // [Progressive_Ultra] 75

            const string s1 = "SELECT " +
                              "bandt.benefit_benefitplan_id  " +
                              ",benefit_benefit_id,bc.benefit_copay_id " +
                              ",ISNULL(bc.value, 0.0 )*2 AS value " +
                              ",benefit_rider_id " +
                              ",benefit_rider_ud " +
                              "FROM #BenefitAndTerm bandt  " +
                              "JOIN benefit_copay bc ON bc.benefit_id = bandt.benefit_benefit_id  " +
                              "AND bandt.benefit_class_id = 75 " +
                              "AND benefit_benefitplan_id = 37 " +
                              "ORDER BY benefit_benefit_id --[Progressive_Ultra] 75";

            return s1;
        }

//SELECT * FROM employergroup WHERE group_code in ('XTN','XM8','YF3','X10','Y1B','YAG','X1Z','YBY','YB4','YD6')

//SELECT * FROM employergroup WHERE employergroup_id in (29,29,410,236,525,536,393,435,453,464)

//SELECT Count(*) FROM eligibility WHERE employergroup_id in (29,29,410,236,525,536,393,435,453,464)






    }
}
