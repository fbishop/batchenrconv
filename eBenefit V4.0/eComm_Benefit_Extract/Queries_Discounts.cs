﻿namespace eComm_Benefit_Extract
{
    public partial class Form1
    {
        // ========================================================================================
        //

        private string GetSqlForNaCategories()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Max_Child] = 'NA', " +
                              "T2.[Poly_Eligible_Power] = 'NA', " +
                              "T2.[50%_Off_Allowed] = 'NA', " +
                              "T2.[Price_Progressive_As] = 'NA', " +
                              "T2.[Frame_Allowance_Type] = 'NA', " +
                              "T2.[Has_Voucher] = 'NA', " +
                              "T2.[Payer_Term] = 'NA', " +
                              "T2.[Plan_Group] = 'NA', " +
                              "T2.[Material_Copay] = 'NA' " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN  [member_category_set] mcs ON mcs.[member_category_set_id] = T2.[member_category_set_id] ";

            Dumpsql("2", "Get Frame_Display_Template", s1);

            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlForStandard_Frame_Discount()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Standard_Frame_Discount] = COALESCE(" +
                              "CONVERT(VARCHAR, convert(decimal(10, 2), prudf.value)), '') + ' %' " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "LEFT JOIN [payor_rider_user_defined_field] prudf ON " +
                              "prudf.user_defined_field_id = 76 AND prudf.[member_category_set_id] = T2.[member_category_set_id] AND prudf.[employergroup_id] = T2.[employergroup_id] ";

            Dumpsql("2", "Get Standard_Frame_Discount", s1);

            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlForStandard_Contacts_Discount()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Standard_Contacts_Discount] = COALESCE(" +
                              "CONVERT(VARCHAR, convert(decimal(10, 2), prudf2.value)), '') + ' %' " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "LEFT JOIN [payor_rider_user_defined_field] prudf2 ON " +
                              "prudf2.[employergroup_id] = T2.[employergroup_id] AND prudf2.[member_category_set_id] = T2.[member_category_set_id] AND prudf2.user_defined_field_id = 77 ";

            Dumpsql("2", "Get Standard_Contacts_Discount", s1);

            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlForCleanupStandard_Contacts_Discount()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Standard_Contacts_Discount] = '' " +
                              "FROM [#MonsterTempTable1] T2 WHERE T2.[Standard_Contacts_Discount] = ' %'";

            Dumpsql("2", "Cleanup Standard_Contacts_Discount", s1);

            return s1;
        }

        // ========================================================================================
        //

        private string GetSqlForCleanupStandard_Frame_Discount()
        {
            const string s1 = "UPDATE T2 " +
                              "SET " +
                              "T2.[Standard_Frame_Discount] = '' " +
                              "FROM [#MonsterTempTable1] T2 WHERE T2.[Standard_Frame_Discount] = ' %'";

            Dumpsql("2", "Cleanup Standard_Frame_Discount", s1);

            return s1;
        }



    }
}
