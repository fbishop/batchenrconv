﻿namespace eComm_Benefit_Extract
{
    public partial class Form1
    {



        // ========================================================================================

        private string GetSqlForVDtoYes()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[Has_VDT] = b.[Has_VDT] " +
                              "FROM " +
                              "( " +
                              "SELECT CASE WHEN COUNT(a.cnt) > 0 THEN 'Y' ELSE 'N' END AS [Has_VDT] , a.[benefitplan_id] " +
                              "FROM " +
                              "( " +
                              "SELECT COUNT(*) as 'cnt',[benefitplan_id] " +
                              "FROM [benefit] b " +
                              "WHERE [benefit_ud] like 'VDT[_]%' " +
                              "GROUP BY [benefitplan_id] " +
                              ") a " +
                              "GROUP BY [benefitplan_id] " +
                              ") b WHERE b.[benefitplan_id] = [#MonsterTempTable1].[benefitplan_id]";

            Dumpsql("2", "GetSqlForVDtoYes", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForSafetytoYes()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[Has_Safety] = b.[Has_Safety] " +
                              "FROM " +
                              "( " +
                              "SELECT CASE WHEN COUNT(a.cnt) > 0 THEN 'Y' ELSE 'N' END AS [Has_Safety] , a.[benefitplan_id] " +
                              "FROM " +
                              "( " +
                              "SELECT COUNT(*) as 'cnt',[benefitplan_id] " +
                              "FROM [benefit] b " +
                              "WHERE [benefit_ud] like 'Safety[_]%' " +
                              "GROUP BY [benefitplan_id] " +
                              ") a " +
                              "GROUP BY [benefitplan_id] " +
                              ") b WHERE b.[benefitplan_id] = [#MonsterTempTable1].[benefitplan_id]";

            Dumpsql("2", "GetSqlForHasSafetytoYes", s1);

            return s1;
        }



        // ========================================================================================

        private string GetSqlFor2DresstoYes()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[Has_2_Dress_Pair] = b.[Has_2_Dress_Pair] " +
                              "FROM " +
                              "( " +
                              "SELECT CASE WHEN COUNT(a.cnt) > 0 THEN 'Y' ELSE 'N' END AS [Has_2_Dress_Pair] , a.[benefitplan_id] " +
                              "FROM " +
                              "( " +
                              "SELECT COUNT(*) as 'cnt',[benefitplan_id] " +
                              "FROM [benefit] b " +
                              "WHERE [benefit_ud] like '2[_]%' " +
                              "GROUP BY [benefitplan_id] " +
                              ") a " +
                              "GROUP BY [benefitplan_id] " +
                              ") b WHERE b.[benefitplan_id] = [#MonsterTempTable1].[benefitplan_id]";

            Dumpsql("2", "GetSqlForHasSafetytoYes", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForBenefit_SplittingYes()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] " +
                              "SET [#MonsterTempTable1].[Benefit_Splitting] = 'Y' " +
                              "FROM [#MonsterTempTable1] T2 " +
                              "JOIN [dv_group_contract_def] gcd ON gcd.[group_contract_id] = T2.[group_contract_id] ";

            Dumpsql("2", "GetSqlForHasSafetytoYes", s1);

            return s1;
        }

        // ========================================================================================

        // ========================================================================================

        private string GetSqlForSafetytoNo()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [Has_Safety] = 'N'";

            Dumpsql("2", "GetSqlForHasSafetytoNo", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlFor2DresstoNo()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [Has_2_Dress_Pair] = 'N'";

            Dumpsql("2", "GetSqlForHasSafetytoNo", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForHasDeductabletoNo()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [Has_Deductable] = 'N'";

            Dumpsql("2", "GetSqlForHas_DeductabletoNo", s1);

            return s1;
        }

        // ========================================================================================

        private string GetSqlForVDtoNo()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [Has_VDT] = 'N'";

            Dumpsql("2", "GetSqlForVDtoNo", s1);

            return s1;
        }

        // ========================================================================================


        private string GetSqlForBenefit_SplittingNo()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [Benefit_Splitting] = 'N'";

            Dumpsql("2", "GetSqlForBenefit_SplittingNo", s1);

            return s1;
        }


        // ========================================================================================

        // ========================================================================================

        private string GetSqlForCleanSafety()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [Has_Safety] = '' WHERE [Has_Safety] = 'N'";

            Dumpsql("2", "GetSqlForCleanSafety", s1);

            return s1;
        }


        // ========================================================================================

        private string GetSqlForClean2Dress()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [Has_2_Dress_Pair] = '' WHERE [Has_2_Dress_Pair] = 'N'";

            Dumpsql("2", "GetSqlForClean2Dress", s1);

            return s1;
        }


        // ========================================================================================

        private string GetSqlForCleanVd()
        {
            const string s1 = "UPDATE [#MonsterTempTable1] SET [Has_VDT] = '' WHERE [Has_VDT] = 'N'";

            Dumpsql("2", "GetSqlForCleanVD", s1);

            return s1;
        }
        // ========================================================================================



    }
}
