------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--Get Shorthand
SELECT   [Group_Name] 
, [Group_Code]  
, [employergroup_id]  
, [employergroup_ud]  
, [group_contract_id]  
, [group_contract_ud]  
, [member_category_set_id]  
, [member_category_set_ud] 
, [Pair_Cycle_Time] 
, [Single_Vision_Cnt]  
, [Single_Vision]  
, [Bifocal_Cnt]  
, [Bifocal] 
, [Trifocal_Cnt] 
, [Trifocal]  
, [Progressive_Cnt] 
, [Progressive] 
FROM #MonsterTempTable1  ORDER BY [Group_Code],[SubGroup_Code],[Plan_Code]

--Get Copay Cnt For a Specifc Benefit Plan
SELECT * 
FROM #BenefitAndTerm bandt

--Get Copay Amounts For a Specifc Benefit Plan (older way)
SELECT bandt.benefit_benefitplan_id  ,benefit_benefit_id,bc.benefit_copay_id ,ISNULL(bc.value, 0.0 )*2 AS value ,benefit_rider_id ,benefit_rider_ud 
FROM #BenefitAndTerm bandt  
JOIN benefit_copay bc ON bc.benefit_id = bandt.benefit_benefit_id  
AND bandt.benefit_class_id = 75 
AND benefit_benefitplan_id = 37 ORDER BY benefit_benefit_id --[Progressive_Ultra] 75

--Get Copay Amounts For a Specifc Benefit Plan (new way)
SELECT bandt.benefit_benefitplan_id  ,benefit_benefit_id,bc.benefit_copay_id ,ISNULL(bc.value, 0.0 )*2 AS value , benefit_rider_ud, bandt.[MCS_Rider_Key] 
FROM #BenefitAndTermAndRider bandt 
JOIN benefit_copay bc ON bc.benefit_id = bandt.benefit_benefit_id 
AND bandt.benefit_class_id = 50 
AND benefit_benefitplan_id = 286 
AND benefit_rider_ud = 'DE00000511' ORDER BY benefit_benefit_id

--Misc supporting snips
-- riders for a employer group SELECT * 
FROM employergroup where group_code = 'FNY' SELECT * 
FROM employergroup eg 
JOIN DVRiderPlan dvrp ON dvrp.employer_id = eg.employerGroup_id 
WHERE group_code = 'FNY' -- 30 rows SELECT mcs.* 
FROM employergroup eg 
JOIN DVRiderPlan dvrp ON dvrp.employer_id = eg.employerGroup_id 
JOIN member_category_set mcs on mcs.member_category_set_id = dvrp.member_category_set_id 
WHERE group_code = 'FNY' -- 30 rows SELECT mcs.* 
FROM employergroup eg 
JOIN DVRiderPlan dvrp ON dvrp.employer_id = eg.employerGroup_id 
JOIN member_category_set mcs on mcs.member_category_set_id = dvrp.member_category_set_id 
WHERE group_code = 'FNY' --and member_category_set_ud = 'DE00000511' -- 30 rows -- riders for a group contract SELECT * 
FROM group_contract where group_contract_ud = '40F900000014' SELECT category,* 
FROM group_contract 
JOIN Benefit_category on group_contract_id = benefit_contract_id 
WHERE group_contract_ud = '40F900000014' -- 12

--Get All Part1
SELECT * 
FROM #ActiveBenefitPlans

--Get All Part1
SELECT * 
FROM #ActiveRiders

--Get All Part1
SELECT * 
FROM #BenefitAndTerm

--Get All Part1
SELECT * 
FROM #EmployerContractRiders

--Get All Part1
SELECT * 
FROM #MonsterTempTable1
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--Get All Part1
SELECT REPLACE(CONVERT(varchar, CAST(SUM(eligibility_Cnt) AS money), 1), '.00', '') AS 'All Eligibilities' 
FROM [#MonsterTempTable1]

--Get All Part1
SELECT REPLACE(CONVERT(varchar, CAST(SUM(active_eligibility_Cnt) AS money), 1), '.00', '') AS 'Active Eligibilities' 
FROM [#MonsterTempTable1]

--Get All Part1
SELECT REPLACE(CONVERT(varchar, CAST(SUM(future_eligibility_Cnt) AS money), 1), '.00', '') AS 'Future Eligibilities' 
FROM [#MonsterTempTable1]

--Get All Part1
SELECT REPLACE(CONVERT(varchar, CAST(SUM(term_eligibility_Cnt) AS money), 1), '.00', '') AS 'Termed Eligibilities' 
FROM [#MonsterTempTable1]

--Get All Part1
SELECT REPLACE(CONVERT(varchar, CAST(SUM(PPL_eligibility_Cnt) AS money), 1), '.00', '') AS '$PPL Eligibilities' 
FROM [#MonsterTempTable1]
