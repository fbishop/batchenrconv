﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using CodeDebuggingNamespace;

namespace AccumulatorConversion
{
    public partial class Form1 : Form
    {
        // ========================================================================================

        private string _pathToInputFiles = "";

        private string _pathToArchiveFilesInputs = "";
        private string _pathToArchiveFilesOutputs = "";
        private string _pathToArchiveFilesLogs = "";

        private string _pathToFinalOutputFiles = "";

        private string _globalOutputFileName = "";
        private string _globalInputFileName = "";

        private string _fullpathfilename = "";

        private static string _emailbody;

        private Dictionary<string, string> _lookupDictionary;

        private List<string> _log;

        private bool _ranonce;

        private const string Domain   = "ad.davisvision.com";
       // private const string Server   = "NYL-D-RPTSQL1";

        private const string Server = "NYL-CVXSQL";

        private const string Database = "CVX_PROD_1";

        //private const string Username = "";
        //private const string Password = "";

        private const string Username = "MacessUser";
        private const string Password = "m@c3ss"; // m@c3ss   //leR$E+1LsoRs

        // =============================================================================================

        private void Outputtodebug(string s1)
        {
            UDP_Debugging.LogFlat(s1);

            _log.Add(s1);
        }

        // ========================================================================================

        private void Output(string p)
        {
            textBox1.AppendText(p + Environment.NewLine);

            Outputtodebug(p);

            _emailbody = _emailbody + p + Environment.NewLine;

        }

        // ========================================================================================

        public Form1()
        {
            InitializeComponent();
        }

        // ==============================================================================================

        //private string ConvertFooter(string item)
        //{
        //    //9AR0000004523    
        //    var existingcnt = Get_FieldOneBased(item, 7, 15);

        //    var cnt = Convert.ToInt32(existingcnt);

        //    return "9AR" + cnt.ToString().PadLeft(10, '0');
        //}

        // ==============================================================================================

        //private string ConvertHeader(string item)
        //{
        //    // 1H   BCBSAZ ACCUM   201707170219PROD
        //    // 1ARNSLIJ               DAVIS               2017120705130000

        //    var customerkey = Get_FieldOneBased(item, 6, 11);
        //    var filedate = Get_FieldOneBased(item, 21, 28);
        //    var filetime = Get_FieldOneBased(item, 29, 32);

        //    if (customerkey != "BCBSAZ")
        //    {
        //        Output("Warning Header Issue Found");
        //    }


        //    return "1AR" + customerkey + Blanks(14) + "DAVIS" + Blanks(15) + filedate + filetime + Blanks(295);
        //}

        // ==============================================================================================

        private string Blanks(int cnt)
        {
            var s2 = "";
            for (var i = 0; i < cnt; i++)
            {
                s2 = s2 + " ";
            }
            return s2;
        }

        // ========================================================================================

        private bool Valid_Footer(string footer)
        {
            var key = Get_FieldOneBased(footer, 1, 2);

            if (key.Trim().ToUpper() == "9T")
            {
                return true;
            }
            else
            {
                Output("Footer does not start with 9T");
                return false;
            }

        }

        // ========================================================================================

        private bool Valid_Header(string header)
        {
            var key = Get_FieldOneBased(header, 1, 2);

            if (key.Trim().ToUpper() == "1H")
            {
                return true;
            }
            else
            {
                Output("Header does not start with 1H");
                return false;
            }

        }

        // ========================================================================================

        private string Dash(int p)
        {
            var s = "";
            for (var i = 0; i < p; i++) { s = s + "-"; }
            return s;
        }

        // ========================================================================================

        private string SetInitialFile(string path)
        {
            var files = Directory.GetFiles(path);

            if (files.Length > 0)
            {
                var list1 = new List<string>();

                foreach (var line in files)
                {
                    var s1 = Path.GetExtension(line);

                    if (s1.ToUpper() != ".EXE")
                    {
                        //if (s1.ToUpper() != ".TXT")
                        {
                            //if (s1.ToUpper() != ".INPUT")
                            {
                                list1.Add(line);
                            }
                        }
                    }
                }

                if (list1.Count > 0)
                {
                    return list1[0];
                }

                return "";
            }
            else
            {
                return "";
            }
        }


        private static string GetVersion()
        {
            var major = Assembly.GetExecutingAssembly().GetName().Version.Major.ToString();
            var minor = Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString();
            var build = Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();

            return major + @"." + minor + @" (Build " + build + @")";
        }



        // ========================================================================================

        private void SetTitle()
        {
            const string baseFormTitle = "Accumulator Convert V";
            Text = baseFormTitle + GetVersion();
        }




        // ====================================================================================

        private static string Commafystr(string ss)
        {
            var s1 = ss.Trim();

            if (s1.Contains(",")) return ss;

            var s2 = "";

            for (var i = (s1.Length - 1); i > -1; i--)
            {
                s2 = s1[i] + s2;

                if (i == 0) continue;

                // get inverse count
                var j = s1.Length - i;

                if ((j % 3) == 0) { s2 = "," + s2; }
            }

            return s2;
        }

        // ========================================================================================

        private static string Get_FieldOneBased(string singleLine, int startingPos, int endingPos)
        {
            var s1 = "";
            if (singleLine.Length >= endingPos)
            {
                if (startingPos <= endingPos)
                {
                    if (singleLine != "")
                    {
                        s1 = singleLine.Substring(startingPos - 1, endingPos - startingPos + 1);
                    }
                }
            }
            return s1;
        }

        // ========================================================================================

        private bool Validate_Lines_All(int p, List<string> list1)
        {
            var answer = true;

            for (var i = 1; i < list1.Count - 1; i++)
            {
                var length = list1[1].Length;

                if (length != p)
                {
                    answer = false;
                    Output("Incorrect line length found of " + length + " for line number " + i);
                    break;
                }
            }
            return answer;
        }

        // ========================================================================================

        //private static string DefineAndCreateIfNeeded(string s1, string s2)
        //{
        //    var s0 = Path.Combine(s1, s2);
        //    s0 = Path.GetFullPath(s0);
        //    if (!Directory.Exists(s0))
        //    {
        //        Directory.CreateDirectory(s0);
        //    }

        //    return s0;
        //}


        // ========================================================================================

        private string AddTrailingBackSlash(string dir)
        {
            // Path.GetDirectoryName(dir); will work incorrectly without a trailing backslash

            Outputtodebug("AddTrailingBackSlash");
            if (dir.EndsWith(@"\"))
            {
                return dir;
            }
            else
            {
                return dir + @"\";
            }
        }



        // ========================================================================================

        private string GenerateOutputFileName(string dir, string p, string c, string d)
        {
            dir = AddTrailingBackSlash(dir);

            var a = Path.GetDirectoryName(dir);
            const string b = "BCBSAZ_ACCUM_";

            if (a != null)
            {
                return Path.Combine(a, b + c + "_" + d + ".txt");
            }

            return "";
        }

        // ========================================================================================

        private string GenerateOutputLogFileName(string dir)
        {
            dir = AddTrailingBackSlash(dir);

            return Path.Combine(dir,"LogFile_" + DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss") + ".txt");

       }

        // ========================================================================================

        private string Getheader(string datetimestamp)
        {
            var s1 = "1AR";
            s1 = s1 + "BCBSAZ".PadRight(20, ' ');
            s1 = s1 + "DAVIS".PadRight(20, ' ');
            //s1 = s1 + DateTime.Now.ToString("yyyyMMddHHmmss");    
            s1 = s1 + datetimestamp;
            s1 = s1 + "00";                             
            s1 = s1 + " ".PadRight(291, ' ');             // Filler

            return s1;
        }


        // ========================================================================================

        private string Gettrailer(int recordCount)
        {
            var s1 = "9AR";
            s1 = s1 + recordCount.ToString().PadLeft(10, '0');
            s1 = s1 + " ".PadRight(337, ' ');                     // Filler

            return s1;
        }

        // ========================================================================================

        private List<string> AddHeaderTrailer(List<string> list1A,string dts)
        {
            var list2A = new List<string>();

            list2A.Add(Getheader(dts));

            list2A.AddRange(list1A);

            list2A.Add(Gettrailer(list1A.Count));

            return list2A;
        }
        

        // ========================================================================================

        private void DumpLogFile(string p)
        {
            var logfilename = GenerateOutputLogFileName(p);
            File.WriteAllLines(logfilename, _log.ToArray());
        }

        // ========================================================================================

        private void MainProcessing(List<string> list1A, Dictionary<string, string> dataDictionary, List<string> list2B)
        {
            dataDictionary = SetDataDictionary(list1A, dataDictionary);


            // Dump the Dictionary
            //var items = from pair in dataDictionary orderby pair.Value ascending select pair;
            var items = from pair in dataDictionary orderby pair.Key ascending select pair;

            var cnt = 1;

            foreach (var pair in items)
            {
                //   Output("[" + pair.Key + "] = [" + pair.Value + "]");

                var pieces1 = pair.Key.Split('|');

                var subscriberid = pieces1[0];
                var lastname = pieces1[1];
                var firstname = pieces1[2];
                var middleinitial = pieces1[3];

                var dependentid = pieces1[4];
                var gender = pieces1[5];
                var dateofbirth = pieces1[6];
                var planyear = pieces1[7];


                var pieces2 = pair.Value.Split('|');

                var individualdet = pieces2[0];
                var familydeductable = pieces2[1];
                var individualoop = pieces2[2];
                var familyoop = pieces2[3];

                const string fourthindividualdet = "0.00";
                const string fourthfamilydeductable = "0.00";
                const string fourthindividualoop = "0.00";
                const string fourthfamilyoop = "0.00";

                const string recordtype = "2";

                var recordnumber = cnt.ToString().PadLeft(6, '0');
                cnt = cnt + 1;

                var benefitcontractid = "";

                if (_lookupDictionary.ContainsKey(subscriberid.Trim()))
                {
                    benefitcontractid = _lookupDictionary[subscriberid.Trim()];
                }


                var groupnumber = "";

                var InorOut = "I";
                var filler = Blanks(122);

                var p1 = recordtype +
                         recordnumber +
                         benefitcontractid.PadRight(30, ' ') +
                         subscriberid.PadRight(20, ' ') +
                         dependentid.PadRight(20, ' ') +
                         groupnumber.PadRight(20, ' ');

                p1 = p1.PadRight(97, ' ');

                var p2 = firstname.PadRight(15, ' ') +
                         middleinitial.PadRight(1, ' ') +
                         lastname.PadRight(25, ' ') +
                         dateofbirth.PadRight(8, ' ') +
                         gender.PadRight(1, ' ') + planyear.PadRight(8, ' ') +
                         InorOut.PadRight(1, ' ');

                var p3 = individualdet.PadLeft(9, ' ') +
                         familydeductable.PadLeft(9, ' ') +
                         individualoop.PadLeft(9, ' ') +
                         familyoop.PadLeft(9, ' ') +
                         fourthindividualdet.PadLeft(9, ' ') +
                         fourthfamilydeductable.PadLeft(9, ' ') +
                         fourthindividualoop.PadLeft(9, ' ') +
                         fourthfamilyoop.PadLeft(9, ' ');

                list2B.Add(p1 + p2 + p3 + filler);
            }
        }

        // ========================================================================================

        private Dictionary<string, string> SetDataDictionary(List<string> list1A, Dictionary<string, string> dataDictionary)
        {
            for (var i = 0; i < list1A.Count; i++)
            {
                var item = list1A[i];

                if ((i > 0) && (i < (list1A.Count - 1)))
                {
                    AddLineToDictionary(ref dataDictionary, item);
                }
            }
            return dataDictionary;
        }

        // ========================================================================================

        private List<string> ProcessList(List<string> list1A)
        {
            var list2B = new List<string>();

            // Define and Initialize
            var dataDictionary = new Dictionary<string, string>();


            Outputtodebug("ProcessList");

            // check header
            var header = list1A[0];
            //Output(header);

            // check footer
            var footer = list1A[list1A.Count - 1];
            //Output(footer);


            if (Valid_Header(header))
            {
                if (Valid_Footer(footer))
                {

                    if (File_Contains_Data(footer))
                    {

                        MainProcessing(list1A, dataDictionary, list2B);
                    }
                    else
                    {
                        Output("File Is Empty (So Don't Process)");
                    }

                }
                else
                {
                    Output("Invalid Footer");
                }

            }
            else
            {
                Output("Invalid Header");
            }

            // return converted file
            return list2B;

        }

        // ==============================================================================================

        private bool File_Contains_Data(string footer)
        {
            var key = Get_FieldOneBased(footer, 1, 2);

            if (key.Trim().ToUpper() == "9T")
            {
                return true;
            }
            else
            {
                Output("Footer does not start with 9T");
                return false;
            }
        }

        // ==============================================================================================

        private void AddLineToDictionary(ref Dictionary<string, string> dataDictionary, string item)
        {
            var idandrange = GetIDandRange(item);

            var field = GetFieldandValue(item);

            if (dataDictionary.ContainsKey(idandrange.Trim()))
            {
                // increment it
                var value = dataDictionary[idandrange.Trim()];
                var newvalue = AddValue(value, field);

                dataDictionary[idandrange.Trim()] = newvalue;
            }
            else
            {
                const string value = "0.00|0.00|0.00|0.00|0.00|0.00|0.00|0.00";
                var newvalue = AddValue(value, field);

                // add it and set the count to one
                dataDictionary.Add(idandrange.Trim(), newvalue);
            }
        }

        // ==============================================================================================

        private string GetIDandRange(string item)
        {
            var memberid = Get_FieldOneBased(item, 14, 38);

            var one = Get_FieldOneBased(item, 39, 40);
            var gender = Get_FieldOneBased(item, 41, 41);
            var dob = Get_FieldOneBased(item, 42, 49);

            var lastname = Get_FieldOneBased(item, 50, 84);
            var firstname = Get_FieldOneBased(item, 85, 109);
            var middleinitial = Get_FieldOneBased(item, 110, 110);

            var counterstart = Get_FieldOneBased(item, 133, 140);
            var counterend = Get_FieldOneBased(item, 141, 148);

            return memberid.Trim() + "|" 
                + lastname.Trim() + "|" 
                + firstname.Trim() + "|" 
                + middleinitial.Trim() + "|"
                + one.Trim() + "|"
                + gender.Trim() + "|"
                + dob.Trim() + "|" 
                + counterstart.Trim() + "|" 
                + counterend.Trim();
        }

        // ==============================================================================================

        private string GetFieldandValue(string item)
        {
            var counterid = Get_FieldOneBased(item, 111, 120);

            var counteramount = Get_FieldOneBased(item, 122, 132);

            return counterid + "|" + counteramount;
        }

        // ==============================================================================================

        private string AddValue(string value, string field)
        {
            // parse current value into 8 pieces

            var pieces1 = value.Split('|');

            var individualdeductable = pieces1[0];
            var familydeductable = pieces1[1];
            var individualoop = pieces1[2];
            var familyoop = pieces1[3];

            var quarterindividualdeductable = pieces1[4];
            var quarterfamilytable = pieces1[5];
            var quarterindividualoop = pieces1[6];
            var quarterfamilyoop = pieces1[7];

            // determine which piece changes
            var pieces2 = field.Split('|');
            var fieldkey = pieces2[0];
            var fieldamount = pieces2[1];
            var dedoroop = fieldkey.Substring(0, 3);
            var indorfam = fieldkey.Substring(7, 3);

            if (dedoroop == "DED")
            {
                if (indorfam == "IND")
                {
                    individualdeductable = Convertnum(fieldamount);
                }
                else
                {
                    familydeductable = Convertnum(fieldamount);
                }
            }
            else
            {
                if (indorfam == "IND")
                {
                    individualoop = Convertnum(fieldamount);
                }
                else
                {
                    familyoop = Convertnum(fieldamount);
                }
            }

            // put back together
            return individualdeductable + "|" +
                   familydeductable + "|" +
                   individualoop + "|" +
                   familyoop + "|" +
                   quarterindividualdeductable + "|" +
                   quarterfamilytable + "|" +
                   quarterindividualoop + "|" +
                   quarterfamilyoop;
        }

        // ==============================================================================================

        private string Convertnum(string numberwithinmplieddecimal)
        {
            if (numberwithinmplieddecimal.Length == 11)
            {
                var part1 = numberwithinmplieddecimal.Substring(0, 9);
                var i = Convert.ToInt32(part1);
                part1 = i.ToString();
                var part2 = numberwithinmplieddecimal.Substring(9, 2);

                return part1 + "." + part2;       
            }
            else
            {
                return numberwithinmplieddecimal.Length.ToString();
            }
        }

        // ========================================================================================

        private string GetFriendlyVersion(string p)
        {
            var answer = "";

            if (p.StartsWith("13.")) { answer = "SQL Server 2016"; }

            if (p.StartsWith("12."))
            {
                answer = "SQL Server 2014";
                if (p.StartsWith("12.00.5")) { answer = "SQL Server 2014 (SP2)"; }
                if (p.StartsWith("12.00.4")) { answer = "SQL Server 2014 (SP1)"; }
            }

            if (p.StartsWith("11.")) { answer = "SQL Server 2012"; }

            if (p.StartsWith("10."))
            {
                answer = "SQL Server 2008";
                if (p.StartsWith("10.5")) { answer = "SQL Server 2008 R2"; }
                if (p.StartsWith("10.0")) { answer = "SQL Server 2008"; }
            }

            if (p.StartsWith("09."))
            {
                answer = "SQL Server 2005";
                if (p.StartsWith("09.00.2")) { answer = "SQL Server 2005 (SP1)"; }
                if (p.StartsWith("09.00.4")) { answer = "SQL Server 2005 (SP2)"; }
                if (p.StartsWith("09.00.5")) { answer = "SQL Server 2005 (SP3)"; }
                if (p.StartsWith("09.00.6")) { answer = "SQL Server 2005 (SP4)"; }
            }

            if (p.StartsWith("08.")) { answer = "SQL Server 2000"; }
            if (p.StartsWith("07.")) { answer = "SQL Server 7.0"; }

            return answer;
        }

        // ========================================================================================

        private string GetUserPasswordString1(string sn, string dn, string us, string ps)
        {
            const string extra = "tcp:"; // "tcp:"      "np:"

            var s1 = "Server=" + extra + sn.Trim() + ";";
            var s2 = "Database=" + dn.Trim() + ";";

            var s3 = "UID=" + us.Trim() + ";";
            var s4 = "PWD=" + ps.Trim() + ";";

            const string s5 = "Pooling=True;";
            const string s6 = "Min Pool Size=5;";
            const string s7 = "Connection Timeout=5;";
            const string s8 = "Application Name=BZConversion;";

            return s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8;
        }

        // ========================================================================================

        private string GetTrustedString1(string serverName, string databaseName)
        {
            var s1 = "Server=" + serverName.Trim() + ";";
            var s2 = "Database=" + databaseName.Trim() + ";";
            const string s3 = "Integrated Security=SSPI;";

            const string s4 = "Pooling=True;";
            const string s5 = "Min Pool Size=5;";
            const string s6 = "Connection Timeout=5;";
            const string s7 = "Application Name=GetQueryInfo;";

            return s1 + s2 + s3 + s4 + s5 + s6 + s7;
        }

        // ========================================================================================

        private SqlConnection GetConnectionGeneric(string domain, string server, string database, string username, string password)
        {
            string connectionString;

            if (domain != "") { server = server + "." + domain; }

            if (username == "")
            {
                connectionString = GetTrustedString1(server, database);
            }
            else
            {
                connectionString = GetUserPasswordString1(server, database, username, password);
            }

            try
            {
                var con = new SqlConnection(connectionString);

                return con;
            }
            catch (Exception ex2)
            {
                Output(@"Connection Exception: " + ex2.Message);

                return null;
            }
        }

        // ========================================================================================

        private static string GetSqlForListOfGroups()
        {
            return
                "SELECT DISTINCT " +
                "e.[eligibility_ud] + '|' + c.[group_contract_ud] " + 
                "from [CVX_Prod_1].[dbo].[Member] m " +
                "join [CVX_Prod_1].[dbo].[Eligibility] e on m.member_id = e.member_id " +
                "join [CVX_Prod_1].[dbo].[Group_Contract] c on c.group_contract_id = e.group_contract_id " +
                "join [CVX_Prod_1].[dbo].[EmployerGroup] eg on eg.employergroup_id = c.employergroup_id " +
                "where eg.employergroup_id = 544 and e.[eligibility_ud] not like '$PPL%' ";
        }

        // ========================================================================================

        private List<string> Get_List(ref SqlConnection con, string s1)
        {
            var list1 = new List<string>();

            try
            {
                using (var cmd1 = con.CreateCommand())
                {
                    cmd1.CommandText = s1;

                    try
                    {
                        // Blocking command
                        using (var reader = cmd1.ExecuteReader())
                        {
                            // Cast to get HasRows functionality
                            if (reader.HasRows)
                            {
                                // Loop through all the rows in the DataTableReader 
                                while (reader.Read())
                                {
                                    list1.Add(reader[0].ToString());
                                }
                            }
                            else
                            {
                                list1.Add("Empty ReturnSet");
                            }
                        } // using
                    }
                    catch (Exception ex1)
                    {
                        Output("Exception: " + ex1);
                        Output("Exception: " + ex1.Message);
                    }
                }
            }
            catch (Exception ex1)
            {
                Output("Exception: " + ex1);
                Output("Exception: " + ex1.Message);
            }

            return list1;
        }

        // ========================================================================================

        private void PareseOut2(string s1, out string ss2, out string ss1)
        {
            var location = s1.IndexOf("|", StringComparison.Ordinal);

            ss2 = s1.Substring(0, location);

            var sizeOfPartTwo = s1.Length - ss2.Length - 1;

            ss1 = s1.Substring(location + 1, sizeOfPartTwo);
        }

        // ====================================================================================
        //

        private static string DisplayTime6(Stopwatch sTime)
        {
            var frequency = Stopwatch.Frequency;
            var nanosecPerTick = (1000L * 1000L * 1000L) / frequency;
            var absoluteNanoseconds = sTime.ElapsedTicks * nanosecPerTick;
            absoluteNanoseconds = absoluteNanoseconds / 1000;
            absoluteNanoseconds = absoluteNanoseconds / 1000;
            var msec = (int)(absoluteNanoseconds % 1000);
            absoluteNanoseconds = absoluteNanoseconds / 1000;
            var sec = (int)(absoluteNanoseconds);

            var mins = 0; var hrs = 0; var day = 0; var newSec = sec;

            if (newSec >= 60)
            { mins = (newSec / 60); newSec = newSec % 60; }

            var newMin = mins;

            if (newMin >= 60)
            { hrs = (newMin / 60); newMin = newMin % 60; }

            var newHr = hrs;

            if (newHr >= 24)
            { day = (newHr / 24); newHr = newHr % 24; }

            var newDay = day;

            if (newDay > 0)
            { return "[Days = " + newDay + "] " + "[Hours = " + newHr + "]"; }

            if (newHr > 0)
            { return "[Hours = " + newHr + "] " + "[Minutes = " + newMin + "]"; }

            if (newMin > 0)
            { return "[Minutes = " + newMin + "] " + "[Seconds = " + newSec + "]"; }

            return "[Seconds = " + newSec + "] " + "[MilliSeconds = " + msec + "]";
        }

        // ========================================================================================
        //
        private static string FormatStringForEmail(string myString, string htmlBody)
        {
            using (var sr = new StringReader(myString))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    htmlBody = htmlBody + line + "</br>";
                } //while
            } //using

            htmlBody = htmlBody +
                       "</br>" +
                       "</br>" +
                       "</BODY>" +
                       "</HTML>";
            return htmlBody;
        }

        // ========================================================================================
        //

        private static void SetupEmailTo(string myEmailToString, MailMessage objMailMessage)
        {
            var inputs2 = myEmailToString.Split(';');

            var cnt = 1;

            foreach (var s2 in inputs2)
            {
                if (s2.Trim() != "")
                {
                    if (cnt == 1)
                    {
                        objMailMessage.To.Add(new MailAddress(s2.Trim()));
                    }
                    else
                    {
                        objMailMessage.CC.Add(new MailAddress(s2.Trim()));
                    }

                    cnt = cnt + 1;
                }
            }
        }

        // ========================================================================================

        private static string GetHtmlBegin()
        {
            const string htmlBody = "<!DOCTYPE html>" +
                                    "<HTML>" +
                                    "<HEAD>" +
                                    "<TITLE>" +
                                    "</TITLE>" +
                                    "<style>" +
                                    "body" +
                                    "{" +
                                    "background-color:#fef3e6;" + //6C8EB4
                                    "}" +
                                    "</style>" +
                                    "</HEAD>" +
                                    "<BODY>" +
                                    //  "<h3>" +
                                    "</br>";
            return htmlBody;
        }

        // ========================================================================================
        // Sends out an HTML Based Email via our SMTP internal Email Server

        private static void SendAnEmail(string fromString, string toString, string subject, string body, string server)
        {
            // Dont email anyone if no email address is specified
            if (toString.Trim() != "")
            {
                // wrapped in a using statement a class that implements Dispose.
                using (var objMailMessage = new MailMessage())
                {
                    // wrapped in a using statement a class that implements Dispose.
                    using (var objSmtpClient = new SmtpClient(server))
                    {
                        objMailMessage.From = new MailAddress(fromString);

                        SetupEmailTo(toString, objMailMessage);

                        objMailMessage.Subject = subject;

                        objMailMessage.IsBodyHtml = true;

                        var myString = body;

                        var htmlBody = GetHtmlBegin();

                        htmlBody = FormatStringForEmail(myString, htmlBody);

                        objMailMessage.Body = htmlBody;

                        objSmtpClient.UseDefaultCredentials = false;

                        objSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                        objSmtpClient.Send(objMailMessage);

                    } //  using (SmtpClient...

                } // using (MailMessage... 
            }
        }

        // ========================================================================================

        private bool TestConnection()
        {
            var ans = false;

            Output("Testing Database Connection");
            var con = GetConnectionGeneric(Domain, Server, Database, Username, Password);

            try
            {
                con.Open();
                Thread.Sleep(1000);

                // IDbConnection and SqlConnection
                Output("  Connection State = " + con.State);
                //Output("  Connection String = " + con.ConnectionString);
                Output("  Database = " + con.Database);
                Output("  Timeout = " + con.ConnectionTimeout);

                // SqlConnection Only
                Output("  Database Source = " + con.DataSource);
                Output("  Server Version = " + con.ServerVersion);
                Output("  Server Version = " + GetFriendlyVersion(con.ServerVersion));
                Output("  Workstation Id = " + con.WorkstationId);
                Output("  Packet Size = " + con.PacketSize);

                Output("Connected to Database");

                ans = true;

                con.Close();

                Thread.Sleep(1000);

                Output("");
            }
            catch (Exception ex)
            {
                Output("1 FAILED TO CONNECT To Server: " + Server);
                Output("");
                Output(ex.Message);
            }

            return ans;
        }

        // ========================================================================================

        private void ProcessFileLinesAreGood(List<string> list1)
        {
            if (TestConnection())
            {
                var sin = ConnectAndGenerateNewDataList(list1);

                SaveTheOutputFile(list1, sin);

                // Now Refresh For processing the next file
                textBox2.Clear();
                _fullpathfilename = SetInitialFile(_pathToInputFiles);
                textBox2.Text = Path.GetFileName(_fullpathfilename);

                Refresh();            
            }
        }

        // ========================================================================================

        private void SaveTheOutputFile(List<string> list1, List<string> sin)
        {
            Output("");

            var hdr = list1[0];

            var filedts = Get_FieldOneBased(hdr, 21, 28);

            var filedts2 = Get_FieldOneBased(hdr, 29, 32);

            var seconds = DateTime.Now.Second.ToString().PadLeft(2, '0');

            var newdts = filedts + filedts2 + seconds;

            var fin = AddHeaderTrailer(sin, newdts);

            var outputFilename = GenerateOutputFileName(_pathToArchiveFilesOutputs, _fullpathfilename,
                filedts, filedts2 + seconds);

            File.WriteAllLines(outputFilename, fin.ToArray());

            _globalOutputFileName = outputFilename;

            Output("Done Converting File");
        }

        // ========================================================================================

        private List<string> ConnectAndGenerateNewDataList(List<string> list1)
        {
            // Get table of benefit contracts for employees

            Output("Connecting to Database");
            var con = GetConnectionGeneric(Domain, Server, Database, Username, Password);

            try
            {
                con.Open();

                Output("Connected to Database");

                var sql = GetSqlForListOfGroups();

                _lookupDictionary = new Dictionary<string, string>();

                var listofgroups = Get_List(ref con, sql);

                foreach (var item in listofgroups)
                {
                    string eligUd;

                    string benifitUd;

                    PareseOut2(item, out eligUd, out benifitUd);      
                    
                    try
                    {
                        _lookupDictionary.Add(eligUd, benifitUd);
                    }
                    catch (Exception ex)
                    {
                        //Output("eligUd    [" + eligUd + "]");
                        //Output("benifitUd [" + benifitUd + "]");
                        //Output("Already Exists");
                        //Output("");
                        //Output(ex.Message);
                    }
                }

                con.Close();

                Output("");
            }
            catch (Exception ex)
            {
                Output("2 FAILED TO CONNECT to Server: " + Server);
                Output("");
                Output(ex.Message);
            }

            Output("Benefit Contract Lookup Dictionary Contains " +
                   Commafystr(_lookupDictionary.Count.ToString()) + " Items");

            var sin = ProcessList(list1);
            return sin;
        }

        // ========================================================================================

        private void ProcessFileHasEnoughLines(List<string> list1)
        {
            //  const int correctlength = 175;   DAVIS_ACCUM_1215.txt
            const int correctlength = 154; //BCBSAZ_ACCUM_20180103065623.TXT

            if (Validate_Lines_All(correctlength, list1))
            {
                Output("All Lines are of the correct length (" + correctlength + ")");
                ProcessFileLinesAreGood(list1);
            }
        }

        // ========================================================================================

        private void PerformFileManipulations()
        {
          
            
            
            if (File.Exists(_globalOutputFileName))
            {
                Output("Copy Output To Final Destination");

                var filenameonly1 = Path.GetFileName(_globalOutputFileName);
                if (filenameonly1 != null)
                {
                    var fulltopath = Path.Combine(_pathToFinalOutputFiles, filenameonly1);
                    fulltopath = Path.GetFullPath(fulltopath);
                    Output("Copy From: " + _globalOutputFileName);
                    Output("Copy To:   " + fulltopath);

                    File.Copy(_globalOutputFileName, fulltopath);
                    File.SetLastWriteTime(fulltopath, DateTime.Now);
                }
            }





            if (File.Exists(_globalInputFileName))
            {
                Output("Move Input To Archive Input");

                var filenameonly = Path.GetFileName(_globalInputFileName);
                if (filenameonly != null)
                {
                    var fulltopath = Path.Combine(_pathToArchiveFilesInputs, filenameonly);
                    fulltopath = Path.GetFullPath(fulltopath);
                    Output("Move From: " + _globalInputFileName);
                    Output("Move To:   " + fulltopath);

                    File.Move(_globalInputFileName, fulltopath);
                    File.SetLastWriteTime(fulltopath, DateTime.Now);
                }
            }


        }


        // ========================================================================================

        private void SetInitialPathOperations()
        {
            const string pathToBase = "..\\..\\..\\zSource\\";

            // First set to current if not in debug mode
            if (!Directory.Exists(pathToBase))
            {

               // pathToBase = @".";
               //var year = DateTime.Now.ToString("yyyy");

                // Production mode
                _pathToInputFiles          = @"\\ad.davisvision.com\Files\Apps\CVXFTP\Prod\ACA\inbound\AR\BCBSAZ\";

                _pathToArchiveFilesInputs  = @"\\ad.davisvision.com\Files\Apps\CVXFTP\Prod\ACA\inbound\AR\BCBSAZ\Archive\01Inputs\";
                _pathToArchiveFilesOutputs = @"\\ad.davisvision.com\Files\Apps\CVXFTP\Prod\ACA\inbound\AR\BCBSAZ\Archive\02Outputs\";
                _pathToArchiveFilesLogs    = @"\\ad.davisvision.com\Files\Apps\CVXFTP\Prod\ACA\inbound\AR\BCBSAZ\Archive\03Logs\";

                _pathToFinalOutputFiles    = @"\\ad.davisvision.com\Files\Apps\CVXFTP\Prod\ACA\inbound\AR\";
            }
            else
            {
                // Debug mode
                _pathToInputFiles          = @"..\..\..\zSource\01Input\";

                _pathToArchiveFilesInputs  = @"..\..\..\zSource\02Archive\01Inputs\";
                _pathToArchiveFilesOutputs = @"..\..\..\zSource\02Archive\02Outputs\";
                _pathToArchiveFilesLogs    = @"..\..\..\zSource\02Archive\03Logs\";

                _pathToFinalOutputFiles    = @"..\..\..\zSource\03Output\";

            }


            _pathToInputFiles = Path.GetFullPath(_pathToInputFiles);

            _pathToArchiveFilesInputs = Path.GetFullPath(_pathToArchiveFilesInputs);
            _pathToArchiveFilesOutputs = Path.GetFullPath(_pathToArchiveFilesOutputs);
            _pathToArchiveFilesLogs = Path.GetFullPath(_pathToArchiveFilesLogs);

            _pathToFinalOutputFiles = Path.GetFullPath(_pathToFinalOutputFiles);




            Output("_pathToInputFiles          = " + _pathToInputFiles);
            Output("");
            Output("_pathToArchiveFilesInputs  = " + _pathToArchiveFilesInputs);
            Output("_pathToArchiveFilesOutputs = " + _pathToArchiveFilesOutputs);
            Output("_pathToArchiveFilesLogs    = " + _pathToArchiveFilesLogs);
            Output("");
            Output("_pathToFinalOutputFiles    = " + _pathToFinalOutputFiles);


            Output("Ready To Convert");

        }

        // ========================================================================================

        private void Form1_Load(object sender, EventArgs e)
        {
            //Outputtodebug("Form1_Load");

            _log = new List<string>();

            SetTitle();

            SetInitialPathOperations();



            // Set initial file
            textBox2.Clear();
            _fullpathfilename = SetInitialFile(_pathToInputFiles);
            textBox2.Text = Path.GetFileName(_fullpathfilename);

            Refresh();
        }

        // ========================================================================================

        private void button2_Click(object sender, EventArgs e)
        {
            Outputtodebug("button2_Click");

            textBox2.Clear();
            textBox1.Clear();

            var openFileDialog1 = new OpenFileDialog();

            _pathToInputFiles = Path.GetFullPath(_pathToInputFiles);

            // Set default directory
            //openFileDialog1.RestoreDirectory = true;
            openFileDialog1.InitialDirectory = AddTrailingBackSlash(Path.Combine(_pathToInputFiles));
            openFileDialog1.Title = @"Select Incomming File To Process";
            openFileDialog1.Filter = @"All files (*.*)|*.*|Text files (*.txt)|*.txt|Enrollment files (*.acc)|*.acc";

            var result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                var filename = openFileDialog1.FileName;
                textBox2.Text = Path.GetFileName(filename);
                _fullpathfilename = Path.GetFullPath(filename);
            }
        }

        // ========================================================================================

        private void button1_Click(object sender, EventArgs e)
        {
            Outputtodebug("button1_Click");

            if (File.Exists(_fullpathfilename))
            {
                var sTest = new Stopwatch();
                sTest.Reset();
                sTest.Start();

                Output(Dash(70));

                Output("Processing File " + _fullpathfilename);

                _globalInputFileName = _fullpathfilename;


                // check for exclusive lock goes here

                var list1 = File.ReadAllLines(_fullpathfilename).ToList();

                Output("Total Line Count = " + Commafystr(list1.Count.ToString()));

                if (list1.Count > 2)
                {
                    ProcessFileHasEnoughLines(list1);
                }
                else
                {
                    Output("File does not contain enough lines to process");
                }

                sTest.Stop();
                Output("Done in " + DisplayTime6(sTest));
            }
            else
            {
                Output("Defined Input File Does Not Exist");
            }

            PerformFileManipulations();

            DumpLogFile(_pathToArchiveFilesLogs);

            const string eto = "fbishop@davisvision.com";
            const string esub = "BCBS of AZ Accumulator Conversion";
            const string esrv = "smtp.davisvision.com";
            const string efrom = "BCBSofAZ@davisvision.com";

            SendAnEmail(efrom, eto, esub, _emailbody, esrv);
        }

        // ========================================================================================

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.A))
            {
                textBox1.SelectAll();
                e.Handled = true;
            }
        }
        // ========================================================================================
        //

        private static string Getuser()
        {
            // System.Security.Principal.WindowsIdentity.GetCurrent().Name  FQDN
            return Environment.UserName;
        }


        // ========================================================================================
        //

        private static string Getmachine()
        {
            // System.Environment.MachineName from a console or WinForms app.
            // HttpContext.Current.Server.MachineName from a web app
            // System.Net.Dns.GetHostName() to get the FQDN


            return Environment.MachineName;
        }
        // ========================================================================================

        private void Form1_Activated(object sender, EventArgs e)
        {
            Outputtodebug("Form1_Activated");

            if (!(_ranonce))
            {

                Output("Version: " + GetVersion());


                Output("Run as: " + Getuser());

                Output("Run on: " + Getmachine());

                _ranonce = true;

                var args = Environment.GetCommandLineArgs();

                if (args.Length > 1)
                {
                    var arg1 = args[1].ToUpper();

                    if (arg1 == "AUTO")
                    {

                        Output(DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " Auto Run");

                        // this will block till complete
                        button1_Click(new object(), new EventArgs());
                    }
                    else
                    {
                        //  do nothing incorrect command line arguments

                        Output(DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " Manual Run");
                    }

                    // Close the windows form
                    BeginInvoke(new MethodInvoker(Close));

                }
            }
        }

        // ========================================================================================

    }
}
