﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;              // Encoding
using System.Net.Sockets;
using System.Net;               // UdpClient            

namespace CodeDebuggingNamespace
{

    static class UDP_Debugging
    {


        private static string serverip = "127.0.0.1";
        private static int servPort = 7;


        // ========================================================================================


         public static void LogFlat(string s1)
         {


            // Convert input String to an array of bytes
            byte[] sendPacket = Encoding.ASCII.GetBytes(s1);

            // Create a UdpClient instance
            UdpClient client = new UdpClient();

            
            // Protection on failing to connect 
            try 
            {
                // Send the echo string to the specified host and port
                client.Send(sendPacket, sendPacket.Length, serverip, servPort);

            } 
            catch// (SocketException se) 
            {
		// If a problem with the file exists just keep quiet

            }

            client.Close();


        } // public static void Log(string s1)


        // ========================================================================================
	    // History
	    //
	    // 2014-06-05 Strripped down to bare basics and labeled Debugging_UDP_3.cs



        // ========================================================================================
	    // Usage
	    //
        // #1 Add existing item (Debugging_UDP_3.cs) to project 
        //
        // #2 Add output tracer code to each file to be checked (This allows us a fine degree of control)
        //
        // static private void outputtodebug(string s1)
        // {
        //                  UDP_Debugging.LogFlat("FrankSearch \\SearchAnswerFrm.cs " + s1);
        // }
        //
        // #3 Add the using statement to allow us to access CodeDebugging.LogFlat
        // using CodeDebuggingNamespace;
        //
        // #4 Add a line like outputtodebug("SearchAnswerFrm (Constructor)"); at the top of every method
        //
        // ========================================================================================


    } // class UDP_Debugging





} // namespace UDP_DebuggingNamespace
